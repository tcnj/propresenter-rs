mod uuid;

use ::uuid::Uuid;
pub use prost;
pub use uuid::RVUuid;

use std::time::{SystemTime, UNIX_EPOCH};

include!("../generated/rv.data.rs");

impl Presentation {
    pub fn arrangement(&self, uuid: impl Into<Uuid>) -> Option<&presentation::Arrangement> {
        let uuid = RVUuid(uuid.into());
        self.arrangements
            .iter()
            .find(|arrangement| arrangement.uuid == Some(uuid))
    }

    pub fn arrangement_mut(
        &mut self,
        uuid: impl Into<Uuid>,
    ) -> Option<&mut presentation::Arrangement> {
        let uuid = RVUuid(uuid.into());
        self.arrangements
            .iter_mut()
            .find(|arrangement| arrangement.uuid == Some(uuid))
    }

    pub fn cue_group(&self, uuid: impl Into<Uuid>) -> Option<&presentation::CueGroup> {
        let uuid = RVUuid(uuid.into());
        self.cue_groups.iter().find(|cue_group| {
            let Some(group) = &cue_group.group else {
                return false;
            };
            group.uuid == Some(uuid)
        })
    }

    pub fn cue_group_mut(&mut self, uuid: impl Into<Uuid>) -> Option<&mut presentation::CueGroup> {
        let uuid = RVUuid(uuid.into());
        self.cue_groups.iter_mut().find(|cue_group| {
            let Some(group) = &cue_group.group else {
                return false;
            };
            group.uuid == Some(uuid)
        })
    }

    pub fn cue(&self, uuid: impl Into<Uuid>) -> Option<&Cue> {
        let uuid = RVUuid(uuid.into());
        self.cues.iter().find(|cue| cue.uuid == Some(uuid))
    }

    pub fn cue_mut(&mut self, uuid: impl Into<Uuid>) -> Option<&mut Cue> {
        let uuid = RVUuid(uuid.into());
        self.cues.iter_mut().find(|cue| cue.uuid == Some(uuid))
    }
}

impl Cue {
    pub fn slide(&self) -> Option<&PresentationSlide> {
        let slide_action = self
            .actions
            .iter()
            .find(|a| a.r#type() == action::ActionType::PresentationSlide)?;

        let Some(action::ActionTypeData::Slide(action::SlideType {
            slide: Some(action::slide_type::Slide::Presentation(ref presentation_slide)),
        })) = slide_action.action_type_data
        else {
            return None;
        };

        Some(presentation_slide)
    }
    pub fn slide_mut(&mut self) -> Option<&mut PresentationSlide> {
        let slide_action = self
            .actions
            .iter_mut()
            .find(|a| a.r#type() == action::ActionType::PresentationSlide)?;

        let Some(action::ActionTypeData::Slide(action::SlideType {
            slide: Some(action::slide_type::Slide::Presentation(ref mut presentation_slide)),
        })) = slide_action.action_type_data
        else {
            return None;
        };

        Some(presentation_slide)
    }

    pub fn rtf_mut(&mut self) -> Option<&mut Vec<u8>> {
        let PresentationSlide {
            base_slide: Some(slide),
            ..
        } = self.slide_mut()?
        else {
            return None;
        };

        slide.rtf_mut()
    }

    pub fn rtf_str_mut(&mut self) -> Option<&mut String> {
        self.rtf_mut()
            .and_then(|bytes| mut_vec_u8_to_mut_string_checked(bytes).ok())
    }

    pub fn rtf_str(&self) -> Option<&str> {
        let PresentationSlide {
            base_slide: Some(slide),
            ..
        } = self.slide()?
        else {
            return None;
        };

        slide.rtf_str()
    }
}

impl Slide {
    pub fn rtf_mut(&mut self) -> Option<&mut Vec<u8>> {
        let Some(text_element) = self.elements.iter_mut().find(|e| {
            let Some(ref inner) = e.element else {
                return false;
            };
            inner.name == "TextElement"
        }) else {
            return None;
        };

        let Some(ref mut inner) = text_element.element else {
            return None;
        };
        let Some(ref mut text) = inner.text else {
            return None;
        };

        Some(&mut text.rtf_data)
    }

    pub fn rtf_str_mut(&mut self) -> Option<&mut String> {
        self.rtf_mut()
            .and_then(|bytes| mut_vec_u8_to_mut_string_checked(bytes).ok())
    }

    pub fn rtf(&self) -> Option<&[u8]> {
        let Some(text_element) = self.elements.iter().find(|e| {
            let Some(ref inner) = e.element else {
                return false;
            };
            inner.name == "TextElement"
        }) else {
            return None;
        };

        let Some(ref inner) = text_element.element else {
            return None;
        };
        let Some(ref text) = inner.text else {
            return None;
        };

        Some(&text.rtf_data)
    }

    pub fn rtf_str(&self) -> Option<&str> {
        self.rtf().and_then(|bytes| std::str::from_utf8(bytes).ok())
    }
}

fn mut_vec_u8_to_mut_string_checked(
    bytes: &mut Vec<u8>,
) -> Result<&mut String, std::str::Utf8Error> {
    // Ensure that `bytes` holds valid UTF8
    std::str::from_utf8(&bytes)?;

    // Might not technically be safe as `String` isn't `#[repr(transparent)]`, but it's probably fine...
    let transmuted = unsafe { &mut *(bytes as *mut Vec<u8> as *mut String) };

    Ok(transmuted)
}

impl Timestamp {
    pub fn now() -> Self {
        let duration = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();

        Self {
            seconds: duration.as_secs() as i64,
            nanos: duration.subsec_nanos() as i32,
        }
    }
}
