use core::fmt;

use prost::Message;

#[derive(Message)]
struct UuidInternal {
    #[prost(string, tag = "1")]
    string: String,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
#[repr(transparent)]
pub struct RVUuid(pub uuid::Uuid);

impl Default for RVUuid {
    fn default() -> Self {
        Self(uuid::Uuid::nil())
    }
}

impl From<uuid::Uuid> for RVUuid {
    #[inline(always)]
    fn from(value: uuid::Uuid) -> Self {
        RVUuid(value)
    }
}

impl From<RVUuid> for uuid::Uuid {
    fn from(value: RVUuid) -> Self {
        value.0
    }
}

impl std::ops::DerefMut for RVUuid {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl std::ops::Deref for RVUuid {
    type Target = uuid::Uuid;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl fmt::Display for RVUuid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_hyphenated())
    }
}

impl RVUuid {
    fn encode(&self) -> UuidInternal {
        UuidInternal {
            string: self.0.to_string().to_uppercase(),
        }
    }

    fn decode(uuid: UuidInternal) -> Self {
        RVUuid(uuid.string.parse().expect("Invalid UUID"))
    }
}

impl Message for RVUuid {
    fn encode_raw<B>(&self, buf: &mut B)
    where
        B: prost::bytes::BufMut,
        Self: Sized,
    {
        self.encode().encode_raw(buf)
    }

    fn merge_field<B>(
        &mut self,
        tag: u32,
        wire_type: prost::encoding::WireType,
        buf: &mut B,
        ctx: prost::encoding::DecodeContext,
    ) -> Result<(), prost::DecodeError>
    where
        B: prost::bytes::Buf,
        Self: Sized,
    {
        let mut encoded = self.encode();
        let res = encoded.merge_field(tag, wire_type, buf, ctx);
        *self = Self::decode(encoded);
        res
    }

    fn encoded_len(&self) -> usize {
        self.encode().encoded_len()
    }

    fn clear(&mut self) {
        self.0 = Default::default();
    }
}
