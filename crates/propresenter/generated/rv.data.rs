#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Url {
    #[prost(enumeration = "url::Platform", tag = "3")]
    pub platform: i32,
    #[prost(oneof = "url::Storage", tags = "1, 2")]
    pub storage: ::core::option::Option<url::Storage>,
    #[prost(oneof = "url::RelativeFilePath", tags = "4, 5")]
    pub relative_file_path: ::core::option::Option<url::RelativeFilePath>,
}
/// Nested message and enum types in `URL`.
pub mod url {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct LocalRelativePath {
        #[prost(enumeration = "local_relative_path::Root", tag = "1")]
        pub root: i32,
        #[prost(string, tag = "2")]
        pub path: ::prost::alloc::string::String,
    }
    /// Nested message and enum types in `LocalRelativePath`.
    pub mod local_relative_path {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Root {
            Unknown = 0,
            BootVolume = 1,
            UserHome = 2,
            UserDocuments = 3,
            UserDownloads = 4,
            UserMusic = 5,
            UserPictures = 6,
            UserVideos = 7,
            UserDesktop = 11,
            UserAppSupport = 8,
            Shared = 9,
            Show = 10,
            CurrentResource = 12,
        }
        impl Root {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Root::Unknown => "ROOT_UNKNOWN",
                    Root::BootVolume => "ROOT_BOOT_VOLUME",
                    Root::UserHome => "ROOT_USER_HOME",
                    Root::UserDocuments => "ROOT_USER_DOCUMENTS",
                    Root::UserDownloads => "ROOT_USER_DOWNLOADS",
                    Root::UserMusic => "ROOT_USER_MUSIC",
                    Root::UserPictures => "ROOT_USER_PICTURES",
                    Root::UserVideos => "ROOT_USER_VIDEOS",
                    Root::UserDesktop => "ROOT_USER_DESKTOP",
                    Root::UserAppSupport => "ROOT_USER_APP_SUPPORT",
                    Root::Shared => "ROOT_SHARED",
                    Root::Show => "ROOT_SHOW",
                    Root::CurrentResource => "ROOT_CURRENT_RESOURCE",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "ROOT_UNKNOWN" => Some(Self::Unknown),
                    "ROOT_BOOT_VOLUME" => Some(Self::BootVolume),
                    "ROOT_USER_HOME" => Some(Self::UserHome),
                    "ROOT_USER_DOCUMENTS" => Some(Self::UserDocuments),
                    "ROOT_USER_DOWNLOADS" => Some(Self::UserDownloads),
                    "ROOT_USER_MUSIC" => Some(Self::UserMusic),
                    "ROOT_USER_PICTURES" => Some(Self::UserPictures),
                    "ROOT_USER_VIDEOS" => Some(Self::UserVideos),
                    "ROOT_USER_DESKTOP" => Some(Self::UserDesktop),
                    "ROOT_USER_APP_SUPPORT" => Some(Self::UserAppSupport),
                    "ROOT_SHARED" => Some(Self::Shared),
                    "ROOT_SHOW" => Some(Self::Show),
                    "ROOT_CURRENT_RESOURCE" => Some(Self::CurrentResource),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ExternalRelativePath {
        #[prost(message, optional, tag = "1")]
        pub macos: ::core::option::Option<external_relative_path::MacOsExternalVolume>,
        #[prost(message, optional, tag = "2")]
        pub win32: ::core::option::Option<external_relative_path::Win32ExternalVolume>,
        #[prost(string, tag = "3")]
        pub path: ::prost::alloc::string::String,
    }
    /// Nested message and enum types in `ExternalRelativePath`.
    pub mod external_relative_path {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct MacOsExternalVolume {
            #[prost(string, tag = "1")]
            pub volume_name: ::prost::alloc::string::String,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Win32ExternalVolume {
            #[prost(string, tag = "1")]
            pub drive_letter: ::prost::alloc::string::String,
            #[prost(string, tag = "2")]
            pub volume_name: ::prost::alloc::string::String,
            #[prost(bool, tag = "3")]
            pub network_share: bool,
        }
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Platform {
        Unknown = 0,
        Macos = 1,
        Win32 = 2,
        Web = 3,
    }
    impl Platform {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Platform::Unknown => "PLATFORM_UNKNOWN",
                Platform::Macos => "PLATFORM_MACOS",
                Platform::Win32 => "PLATFORM_WIN32",
                Platform::Web => "PLATFORM_WEB",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "PLATFORM_UNKNOWN" => Some(Self::Unknown),
                "PLATFORM_MACOS" => Some(Self::Macos),
                "PLATFORM_WIN32" => Some(Self::Win32),
                "PLATFORM_WEB" => Some(Self::Web),
                _ => None,
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Storage {
        #[prost(string, tag = "1")]
        AbsoluteString(::prost::alloc::string::String),
        #[prost(string, tag = "2")]
        RelativePath(::prost::alloc::string::String),
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum RelativeFilePath {
        #[prost(message, tag = "4")]
        Local(LocalRelativePath),
        #[prost(message, tag = "5")]
        External(ExternalRelativePath),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UrLs {
    #[prost(message, repeated, tag = "1")]
    pub urls: ::prost::alloc::vec::Vec<Url>,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct IntRange {
    #[prost(int32, tag = "1")]
    pub start: i32,
    #[prost(int32, tag = "2")]
    pub end: i32,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Color {
    #[prost(float, tag = "1")]
    pub red: f32,
    #[prost(float, tag = "2")]
    pub green: f32,
    #[prost(float, tag = "3")]
    pub blue: f32,
    #[prost(float, tag = "4")]
    pub alpha: f32,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Version {
    #[prost(uint32, tag = "1")]
    pub major_version: u32,
    #[prost(uint32, tag = "2")]
    pub minor_version: u32,
    #[prost(uint32, tag = "3")]
    pub patch_version: u32,
    #[prost(string, tag = "4")]
    pub build: ::prost::alloc::string::String,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ApplicationInfo {
    #[prost(enumeration = "application_info::Platform", tag = "1")]
    pub platform: i32,
    #[prost(message, optional, tag = "2")]
    pub platform_version: ::core::option::Option<Version>,
    #[prost(enumeration = "application_info::Application", tag = "3")]
    pub application: i32,
    #[prost(message, optional, tag = "4")]
    pub application_version: ::core::option::Option<Version>,
}
/// Nested message and enum types in `ApplicationInfo`.
pub mod application_info {
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Platform {
        Undefined = 0,
        Macos = 1,
        Windows = 2,
    }
    impl Platform {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Platform::Undefined => "PLATFORM_UNDEFINED",
                Platform::Macos => "PLATFORM_MACOS",
                Platform::Windows => "PLATFORM_WINDOWS",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "PLATFORM_UNDEFINED" => Some(Self::Undefined),
                "PLATFORM_MACOS" => Some(Self::Macos),
                "PLATFORM_WINDOWS" => Some(Self::Windows),
                _ => None,
            }
        }
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Application {
        Undefined = 0,
        Propresenter = 1,
        Pvp = 2,
        Provideoserver = 3,
        Scoreboard = 4,
    }
    impl Application {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Application::Undefined => "APPLICATION_UNDEFINED",
                Application::Propresenter => "APPLICATION_PROPRESENTER",
                Application::Pvp => "APPLICATION_PVP",
                Application::Provideoserver => "APPLICATION_PROVIDEOSERVER",
                Application::Scoreboard => "APPLICATION_SCOREBOARD",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "APPLICATION_UNDEFINED" => Some(Self::Undefined),
                "APPLICATION_PROPRESENTER" => Some(Self::Propresenter),
                "APPLICATION_PVP" => Some(Self::Pvp),
                "APPLICATION_PROVIDEOSERVER" => Some(Self::Provideoserver),
                "APPLICATION_SCOREBOARD" => Some(Self::Scoreboard),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CollectionElementType {
    #[prost(message, optional, tag = "1")]
    pub parameter_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub parameter_name: ::prost::alloc::string::String,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MusicKeyScale {
    #[prost(enumeration = "music_key_scale::MusicKey", tag = "1")]
    pub music_key: i32,
    #[prost(enumeration = "music_key_scale::MusicScale", tag = "2")]
    pub music_scale: i32,
}
/// Nested message and enum types in `MusicKeyScale`.
pub mod music_key_scale {
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum MusicKey {
        AFlat = 0,
        A = 1,
        ASharp = 2,
        BFlat = 3,
        B = 4,
        BSharp = 5,
        CFlat = 6,
        C = 7,
        CSharp = 8,
        DFlat = 9,
        D = 10,
        DSharp = 11,
        EFlat = 12,
        E = 13,
        ESharp = 14,
        FFlat = 15,
        F = 16,
        FSharp = 17,
        GFlat = 18,
        G = 19,
        GSharp = 20,
    }
    impl MusicKey {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                MusicKey::AFlat => "MUSIC_KEY_A_FLAT",
                MusicKey::A => "MUSIC_KEY_A",
                MusicKey::ASharp => "MUSIC_KEY_A_SHARP",
                MusicKey::BFlat => "MUSIC_KEY_B_FLAT",
                MusicKey::B => "MUSIC_KEY_B",
                MusicKey::BSharp => "MUSIC_KEY_B_SHARP",
                MusicKey::CFlat => "MUSIC_KEY_C_FLAT",
                MusicKey::C => "MUSIC_KEY_C",
                MusicKey::CSharp => "MUSIC_KEY_C_SHARP",
                MusicKey::DFlat => "MUSIC_KEY_D_FLAT",
                MusicKey::D => "MUSIC_KEY_D",
                MusicKey::DSharp => "MUSIC_KEY_D_SHARP",
                MusicKey::EFlat => "MUSIC_KEY_E_FLAT",
                MusicKey::E => "MUSIC_KEY_E",
                MusicKey::ESharp => "MUSIC_KEY_E_SHARP",
                MusicKey::FFlat => "MUSIC_KEY_F_FLAT",
                MusicKey::F => "MUSIC_KEY_F",
                MusicKey::FSharp => "MUSIC_KEY_F_SHARP",
                MusicKey::GFlat => "MUSIC_KEY_G_FLAT",
                MusicKey::G => "MUSIC_KEY_G",
                MusicKey::GSharp => "MUSIC_KEY_G_SHARP",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "MUSIC_KEY_A_FLAT" => Some(Self::AFlat),
                "MUSIC_KEY_A" => Some(Self::A),
                "MUSIC_KEY_A_SHARP" => Some(Self::ASharp),
                "MUSIC_KEY_B_FLAT" => Some(Self::BFlat),
                "MUSIC_KEY_B" => Some(Self::B),
                "MUSIC_KEY_B_SHARP" => Some(Self::BSharp),
                "MUSIC_KEY_C_FLAT" => Some(Self::CFlat),
                "MUSIC_KEY_C" => Some(Self::C),
                "MUSIC_KEY_C_SHARP" => Some(Self::CSharp),
                "MUSIC_KEY_D_FLAT" => Some(Self::DFlat),
                "MUSIC_KEY_D" => Some(Self::D),
                "MUSIC_KEY_D_SHARP" => Some(Self::DSharp),
                "MUSIC_KEY_E_FLAT" => Some(Self::EFlat),
                "MUSIC_KEY_E" => Some(Self::E),
                "MUSIC_KEY_E_SHARP" => Some(Self::ESharp),
                "MUSIC_KEY_F_FLAT" => Some(Self::FFlat),
                "MUSIC_KEY_F" => Some(Self::F),
                "MUSIC_KEY_F_SHARP" => Some(Self::FSharp),
                "MUSIC_KEY_G_FLAT" => Some(Self::GFlat),
                "MUSIC_KEY_G" => Some(Self::G),
                "MUSIC_KEY_G_SHARP" => Some(Self::GSharp),
                _ => None,
            }
        }
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum MusicScale {
        Major = 0,
        Minor = 1,
    }
    impl MusicScale {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                MusicScale::Major => "MUSIC_SCALE_MAJOR",
                MusicScale::Minor => "MUSIC_SCALE_MINOR",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "MUSIC_SCALE_MAJOR" => Some(Self::Major),
                "MUSIC_SCALE_MINOR" => Some(Self::Minor),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct HotKey {
    #[prost(enumeration = "hot_key::KeyCode", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub control_identifier: ::prost::alloc::string::String,
}
/// Nested message and enum types in `HotKey`.
pub mod hot_key {
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum KeyCode {
        Unknown = 0,
        AnsiA = 1,
        AnsiB = 2,
        AnsiC = 3,
        AnsiD = 4,
        AnsiE = 5,
        AnsiF = 6,
        AnsiG = 7,
        AnsiH = 8,
        AnsiI = 9,
        AnsiJ = 10,
        AnsiK = 11,
        AnsiL = 12,
        AnsiM = 13,
        AnsiN = 14,
        AnsiO = 15,
        AnsiP = 16,
        AnsiQ = 17,
        AnsiR = 18,
        AnsiS = 19,
        AnsiT = 20,
        AnsiU = 21,
        AnsiV = 22,
        AnsiW = 23,
        AnsiX = 24,
        AnsiY = 25,
        AnsiZ = 26,
        Ansi0 = 27,
        Ansi1 = 28,
        Ansi2 = 29,
        Ansi3 = 30,
        Ansi4 = 31,
        Ansi5 = 32,
        Ansi6 = 33,
        Ansi7 = 34,
        Ansi8 = 35,
        Ansi9 = 36,
        AnsiEqual = 37,
        AnsiMinus = 38,
        AnsiRightBracket = 39,
        AnsiLeftBracket = 40,
        AnsiQuote = 41,
        AnsiSemicolon = 42,
        AnsiBackslash = 43,
        AnsiComma = 44,
        AnsiSlash = 45,
        AnsiPeriod = 46,
        AnsiGrave = 47,
        AnsiKeypadDecimal = 48,
        AnsiKeypadPlus = 49,
        AnsiKeypadClear = 50,
        AnsiKeypadDivide = 51,
        AnsiKeypadEnter = 52,
        AnsiKeypadMinus = 53,
        AnsiKeypadEquals = 54,
        AnsiKeypad0 = 55,
        AnsiKeypad1 = 56,
        AnsiKeypad2 = 57,
        AnsiKeypad3 = 58,
        AnsiKeypad4 = 59,
        AnsiKeypad5 = 60,
        AnsiKeypad6 = 61,
        AnsiKeypad7 = 62,
        AnsiKeypad8 = 63,
        AnsiKeypad9 = 64,
        F1 = 65,
        F2 = 66,
        F3 = 67,
        F4 = 68,
        F5 = 69,
        F6 = 70,
        F7 = 71,
        F8 = 72,
        F9 = 73,
        F10 = 74,
        F11 = 75,
        F12 = 76,
        F13 = 77,
        F14 = 78,
        F15 = 79,
        F16 = 80,
        F17 = 81,
        F18 = 82,
        F19 = 83,
        F20 = 84,
        Function = 85,
        Return = 86,
        Tab = 87,
        Space = 88,
        Delete = 89,
        Escape = 90,
        Command = 91,
        Shift = 92,
        CapsLock = 93,
        Option = 94,
        Control = 95,
        RightShift = 96,
        RightOption = 97,
        RightControl = 98,
        VolumeUp = 99,
        VolumeDown = 100,
        Mute = 101,
        Help = 102,
        Home = 103,
        PageUp = 104,
        ForwardDelete = 105,
        End = 106,
        PageDown = 107,
        LeftArrow = 108,
        RightArrow = 109,
        DownArrow = 110,
        UpArrow = 111,
        IsoSelection = 112,
        JisYen = 113,
        JisUnderscore = 114,
        JisKeypadComma = 115,
        JisEisu = 116,
        JisKana = 117,
    }
    impl KeyCode {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                KeyCode::Unknown => "KEY_CODE_UNKNOWN",
                KeyCode::AnsiA => "KEY_CODE_ANSI_A",
                KeyCode::AnsiB => "KEY_CODE_ANSI_B",
                KeyCode::AnsiC => "KEY_CODE_ANSI_C",
                KeyCode::AnsiD => "KEY_CODE_ANSI_D",
                KeyCode::AnsiE => "KEY_CODE_ANSI_E",
                KeyCode::AnsiF => "KEY_CODE_ANSI_F",
                KeyCode::AnsiG => "KEY_CODE_ANSI_G",
                KeyCode::AnsiH => "KEY_CODE_ANSI_H",
                KeyCode::AnsiI => "KEY_CODE_ANSI_I",
                KeyCode::AnsiJ => "KEY_CODE_ANSI_J",
                KeyCode::AnsiK => "KEY_CODE_ANSI_K",
                KeyCode::AnsiL => "KEY_CODE_ANSI_L",
                KeyCode::AnsiM => "KEY_CODE_ANSI_M",
                KeyCode::AnsiN => "KEY_CODE_ANSI_N",
                KeyCode::AnsiO => "KEY_CODE_ANSI_O",
                KeyCode::AnsiP => "KEY_CODE_ANSI_P",
                KeyCode::AnsiQ => "KEY_CODE_ANSI_Q",
                KeyCode::AnsiR => "KEY_CODE_ANSI_R",
                KeyCode::AnsiS => "KEY_CODE_ANSI_S",
                KeyCode::AnsiT => "KEY_CODE_ANSI_T",
                KeyCode::AnsiU => "KEY_CODE_ANSI_U",
                KeyCode::AnsiV => "KEY_CODE_ANSI_V",
                KeyCode::AnsiW => "KEY_CODE_ANSI_W",
                KeyCode::AnsiX => "KEY_CODE_ANSI_X",
                KeyCode::AnsiY => "KEY_CODE_ANSI_Y",
                KeyCode::AnsiZ => "KEY_CODE_ANSI_Z",
                KeyCode::Ansi0 => "KEY_CODE_ANSI_0",
                KeyCode::Ansi1 => "KEY_CODE_ANSI_1",
                KeyCode::Ansi2 => "KEY_CODE_ANSI_2",
                KeyCode::Ansi3 => "KEY_CODE_ANSI_3",
                KeyCode::Ansi4 => "KEY_CODE_ANSI_4",
                KeyCode::Ansi5 => "KEY_CODE_ANSI_5",
                KeyCode::Ansi6 => "KEY_CODE_ANSI_6",
                KeyCode::Ansi7 => "KEY_CODE_ANSI_7",
                KeyCode::Ansi8 => "KEY_CODE_ANSI_8",
                KeyCode::Ansi9 => "KEY_CODE_ANSI_9",
                KeyCode::AnsiEqual => "KEY_CODE_ANSI_EQUAL",
                KeyCode::AnsiMinus => "KEY_CODE_ANSI_MINUS",
                KeyCode::AnsiRightBracket => "KEY_CODE_ANSI_RIGHT_BRACKET",
                KeyCode::AnsiLeftBracket => "KEY_CODE_ANSI_LEFT_BRACKET",
                KeyCode::AnsiQuote => "KEY_CODE_ANSI_QUOTE",
                KeyCode::AnsiSemicolon => "KEY_CODE_ANSI_SEMICOLON",
                KeyCode::AnsiBackslash => "KEY_CODE_ANSI_BACKSLASH",
                KeyCode::AnsiComma => "KEY_CODE_ANSI_COMMA",
                KeyCode::AnsiSlash => "KEY_CODE_ANSI_SLASH",
                KeyCode::AnsiPeriod => "KEY_CODE_ANSI_PERIOD",
                KeyCode::AnsiGrave => "KEY_CODE_ANSI_GRAVE",
                KeyCode::AnsiKeypadDecimal => "KEY_CODE_ANSI_KEYPAD_DECIMAL",
                KeyCode::AnsiKeypadPlus => "KEY_CODE_ANSI_KEYPAD_PLUS",
                KeyCode::AnsiKeypadClear => "KEY_CODE_ANSI_KEYPAD_CLEAR",
                KeyCode::AnsiKeypadDivide => "KEY_CODE_ANSI_KEYPAD_DIVIDE",
                KeyCode::AnsiKeypadEnter => "KEY_CODE_ANSI_KEYPAD_ENTER",
                KeyCode::AnsiKeypadMinus => "KEY_CODE_ANSI_KEYPAD_MINUS",
                KeyCode::AnsiKeypadEquals => "KEY_CODE_ANSI_KEYPAD_EQUALS",
                KeyCode::AnsiKeypad0 => "KEY_CODE_ANSI_KEYPAD_0",
                KeyCode::AnsiKeypad1 => "KEY_CODE_ANSI_KEYPAD_1",
                KeyCode::AnsiKeypad2 => "KEY_CODE_ANSI_KEYPAD_2",
                KeyCode::AnsiKeypad3 => "KEY_CODE_ANSI_KEYPAD_3",
                KeyCode::AnsiKeypad4 => "KEY_CODE_ANSI_KEYPAD_4",
                KeyCode::AnsiKeypad5 => "KEY_CODE_ANSI_KEYPAD_5",
                KeyCode::AnsiKeypad6 => "KEY_CODE_ANSI_KEYPAD_6",
                KeyCode::AnsiKeypad7 => "KEY_CODE_ANSI_KEYPAD_7",
                KeyCode::AnsiKeypad8 => "KEY_CODE_ANSI_KEYPAD_8",
                KeyCode::AnsiKeypad9 => "KEY_CODE_ANSI_KEYPAD_9",
                KeyCode::F1 => "KEY_CODE_F1",
                KeyCode::F2 => "KEY_CODE_F2",
                KeyCode::F3 => "KEY_CODE_F3",
                KeyCode::F4 => "KEY_CODE_F4",
                KeyCode::F5 => "KEY_CODE_F5",
                KeyCode::F6 => "KEY_CODE_F6",
                KeyCode::F7 => "KEY_CODE_F7",
                KeyCode::F8 => "KEY_CODE_F8",
                KeyCode::F9 => "KEY_CODE_F9",
                KeyCode::F10 => "KEY_CODE_F10",
                KeyCode::F11 => "KEY_CODE_F11",
                KeyCode::F12 => "KEY_CODE_F12",
                KeyCode::F13 => "KEY_CODE_F13",
                KeyCode::F14 => "KEY_CODE_F14",
                KeyCode::F15 => "KEY_CODE_F15",
                KeyCode::F16 => "KEY_CODE_F16",
                KeyCode::F17 => "KEY_CODE_F17",
                KeyCode::F18 => "KEY_CODE_F18",
                KeyCode::F19 => "KEY_CODE_F19",
                KeyCode::F20 => "KEY_CODE_F20",
                KeyCode::Function => "KEY_CODE_FUNCTION",
                KeyCode::Return => "KEY_CODE_RETURN",
                KeyCode::Tab => "KEY_CODE_TAB",
                KeyCode::Space => "KEY_CODE_SPACE",
                KeyCode::Delete => "KEY_CODE_DELETE",
                KeyCode::Escape => "KEY_CODE_ESCAPE",
                KeyCode::Command => "KEY_CODE_COMMAND",
                KeyCode::Shift => "KEY_CODE_SHIFT",
                KeyCode::CapsLock => "KEY_CODE_CAPS_LOCK",
                KeyCode::Option => "KEY_CODE_OPTION",
                KeyCode::Control => "KEY_CODE_CONTROL",
                KeyCode::RightShift => "KEY_CODE_RIGHT_SHIFT",
                KeyCode::RightOption => "KEY_CODE_RIGHT_OPTION",
                KeyCode::RightControl => "KEY_CODE_RIGHT_CONTROL",
                KeyCode::VolumeUp => "KEY_CODE_VOLUME_UP",
                KeyCode::VolumeDown => "KEY_CODE_VOLUME_DOWN",
                KeyCode::Mute => "KEY_CODE_MUTE",
                KeyCode::Help => "KEY_CODE_HELP",
                KeyCode::Home => "KEY_CODE_HOME",
                KeyCode::PageUp => "KEY_CODE_PAGE_UP",
                KeyCode::ForwardDelete => "KEY_CODE_FORWARD_DELETE",
                KeyCode::End => "KEY_CODE_END",
                KeyCode::PageDown => "KEY_CODE_PAGE_DOWN",
                KeyCode::LeftArrow => "KEY_CODE_LEFT_ARROW",
                KeyCode::RightArrow => "KEY_CODE_RIGHT_ARROW",
                KeyCode::DownArrow => "KEY_CODE_DOWN_ARROW",
                KeyCode::UpArrow => "KEY_CODE_UP_ARROW",
                KeyCode::IsoSelection => "KEY_CODE_ISO_SELECTION",
                KeyCode::JisYen => "KEY_CODE_JIS_YEN",
                KeyCode::JisUnderscore => "KEY_CODE_JIS_UNDERSCORE",
                KeyCode::JisKeypadComma => "KEY_CODE_JIS_KEYPAD_COMMA",
                KeyCode::JisEisu => "KEY_CODE_JIS_EISU",
                KeyCode::JisKana => "KEY_CODE_JIS_KANA",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "KEY_CODE_UNKNOWN" => Some(Self::Unknown),
                "KEY_CODE_ANSI_A" => Some(Self::AnsiA),
                "KEY_CODE_ANSI_B" => Some(Self::AnsiB),
                "KEY_CODE_ANSI_C" => Some(Self::AnsiC),
                "KEY_CODE_ANSI_D" => Some(Self::AnsiD),
                "KEY_CODE_ANSI_E" => Some(Self::AnsiE),
                "KEY_CODE_ANSI_F" => Some(Self::AnsiF),
                "KEY_CODE_ANSI_G" => Some(Self::AnsiG),
                "KEY_CODE_ANSI_H" => Some(Self::AnsiH),
                "KEY_CODE_ANSI_I" => Some(Self::AnsiI),
                "KEY_CODE_ANSI_J" => Some(Self::AnsiJ),
                "KEY_CODE_ANSI_K" => Some(Self::AnsiK),
                "KEY_CODE_ANSI_L" => Some(Self::AnsiL),
                "KEY_CODE_ANSI_M" => Some(Self::AnsiM),
                "KEY_CODE_ANSI_N" => Some(Self::AnsiN),
                "KEY_CODE_ANSI_O" => Some(Self::AnsiO),
                "KEY_CODE_ANSI_P" => Some(Self::AnsiP),
                "KEY_CODE_ANSI_Q" => Some(Self::AnsiQ),
                "KEY_CODE_ANSI_R" => Some(Self::AnsiR),
                "KEY_CODE_ANSI_S" => Some(Self::AnsiS),
                "KEY_CODE_ANSI_T" => Some(Self::AnsiT),
                "KEY_CODE_ANSI_U" => Some(Self::AnsiU),
                "KEY_CODE_ANSI_V" => Some(Self::AnsiV),
                "KEY_CODE_ANSI_W" => Some(Self::AnsiW),
                "KEY_CODE_ANSI_X" => Some(Self::AnsiX),
                "KEY_CODE_ANSI_Y" => Some(Self::AnsiY),
                "KEY_CODE_ANSI_Z" => Some(Self::AnsiZ),
                "KEY_CODE_ANSI_0" => Some(Self::Ansi0),
                "KEY_CODE_ANSI_1" => Some(Self::Ansi1),
                "KEY_CODE_ANSI_2" => Some(Self::Ansi2),
                "KEY_CODE_ANSI_3" => Some(Self::Ansi3),
                "KEY_CODE_ANSI_4" => Some(Self::Ansi4),
                "KEY_CODE_ANSI_5" => Some(Self::Ansi5),
                "KEY_CODE_ANSI_6" => Some(Self::Ansi6),
                "KEY_CODE_ANSI_7" => Some(Self::Ansi7),
                "KEY_CODE_ANSI_8" => Some(Self::Ansi8),
                "KEY_CODE_ANSI_9" => Some(Self::Ansi9),
                "KEY_CODE_ANSI_EQUAL" => Some(Self::AnsiEqual),
                "KEY_CODE_ANSI_MINUS" => Some(Self::AnsiMinus),
                "KEY_CODE_ANSI_RIGHT_BRACKET" => Some(Self::AnsiRightBracket),
                "KEY_CODE_ANSI_LEFT_BRACKET" => Some(Self::AnsiLeftBracket),
                "KEY_CODE_ANSI_QUOTE" => Some(Self::AnsiQuote),
                "KEY_CODE_ANSI_SEMICOLON" => Some(Self::AnsiSemicolon),
                "KEY_CODE_ANSI_BACKSLASH" => Some(Self::AnsiBackslash),
                "KEY_CODE_ANSI_COMMA" => Some(Self::AnsiComma),
                "KEY_CODE_ANSI_SLASH" => Some(Self::AnsiSlash),
                "KEY_CODE_ANSI_PERIOD" => Some(Self::AnsiPeriod),
                "KEY_CODE_ANSI_GRAVE" => Some(Self::AnsiGrave),
                "KEY_CODE_ANSI_KEYPAD_DECIMAL" => Some(Self::AnsiKeypadDecimal),
                "KEY_CODE_ANSI_KEYPAD_PLUS" => Some(Self::AnsiKeypadPlus),
                "KEY_CODE_ANSI_KEYPAD_CLEAR" => Some(Self::AnsiKeypadClear),
                "KEY_CODE_ANSI_KEYPAD_DIVIDE" => Some(Self::AnsiKeypadDivide),
                "KEY_CODE_ANSI_KEYPAD_ENTER" => Some(Self::AnsiKeypadEnter),
                "KEY_CODE_ANSI_KEYPAD_MINUS" => Some(Self::AnsiKeypadMinus),
                "KEY_CODE_ANSI_KEYPAD_EQUALS" => Some(Self::AnsiKeypadEquals),
                "KEY_CODE_ANSI_KEYPAD_0" => Some(Self::AnsiKeypad0),
                "KEY_CODE_ANSI_KEYPAD_1" => Some(Self::AnsiKeypad1),
                "KEY_CODE_ANSI_KEYPAD_2" => Some(Self::AnsiKeypad2),
                "KEY_CODE_ANSI_KEYPAD_3" => Some(Self::AnsiKeypad3),
                "KEY_CODE_ANSI_KEYPAD_4" => Some(Self::AnsiKeypad4),
                "KEY_CODE_ANSI_KEYPAD_5" => Some(Self::AnsiKeypad5),
                "KEY_CODE_ANSI_KEYPAD_6" => Some(Self::AnsiKeypad6),
                "KEY_CODE_ANSI_KEYPAD_7" => Some(Self::AnsiKeypad7),
                "KEY_CODE_ANSI_KEYPAD_8" => Some(Self::AnsiKeypad8),
                "KEY_CODE_ANSI_KEYPAD_9" => Some(Self::AnsiKeypad9),
                "KEY_CODE_F1" => Some(Self::F1),
                "KEY_CODE_F2" => Some(Self::F2),
                "KEY_CODE_F3" => Some(Self::F3),
                "KEY_CODE_F4" => Some(Self::F4),
                "KEY_CODE_F5" => Some(Self::F5),
                "KEY_CODE_F6" => Some(Self::F6),
                "KEY_CODE_F7" => Some(Self::F7),
                "KEY_CODE_F8" => Some(Self::F8),
                "KEY_CODE_F9" => Some(Self::F9),
                "KEY_CODE_F10" => Some(Self::F10),
                "KEY_CODE_F11" => Some(Self::F11),
                "KEY_CODE_F12" => Some(Self::F12),
                "KEY_CODE_F13" => Some(Self::F13),
                "KEY_CODE_F14" => Some(Self::F14),
                "KEY_CODE_F15" => Some(Self::F15),
                "KEY_CODE_F16" => Some(Self::F16),
                "KEY_CODE_F17" => Some(Self::F17),
                "KEY_CODE_F18" => Some(Self::F18),
                "KEY_CODE_F19" => Some(Self::F19),
                "KEY_CODE_F20" => Some(Self::F20),
                "KEY_CODE_FUNCTION" => Some(Self::Function),
                "KEY_CODE_RETURN" => Some(Self::Return),
                "KEY_CODE_TAB" => Some(Self::Tab),
                "KEY_CODE_SPACE" => Some(Self::Space),
                "KEY_CODE_DELETE" => Some(Self::Delete),
                "KEY_CODE_ESCAPE" => Some(Self::Escape),
                "KEY_CODE_COMMAND" => Some(Self::Command),
                "KEY_CODE_SHIFT" => Some(Self::Shift),
                "KEY_CODE_CAPS_LOCK" => Some(Self::CapsLock),
                "KEY_CODE_OPTION" => Some(Self::Option),
                "KEY_CODE_CONTROL" => Some(Self::Control),
                "KEY_CODE_RIGHT_SHIFT" => Some(Self::RightShift),
                "KEY_CODE_RIGHT_OPTION" => Some(Self::RightOption),
                "KEY_CODE_RIGHT_CONTROL" => Some(Self::RightControl),
                "KEY_CODE_VOLUME_UP" => Some(Self::VolumeUp),
                "KEY_CODE_VOLUME_DOWN" => Some(Self::VolumeDown),
                "KEY_CODE_MUTE" => Some(Self::Mute),
                "KEY_CODE_HELP" => Some(Self::Help),
                "KEY_CODE_HOME" => Some(Self::Home),
                "KEY_CODE_PAGE_UP" => Some(Self::PageUp),
                "KEY_CODE_FORWARD_DELETE" => Some(Self::ForwardDelete),
                "KEY_CODE_END" => Some(Self::End),
                "KEY_CODE_PAGE_DOWN" => Some(Self::PageDown),
                "KEY_CODE_LEFT_ARROW" => Some(Self::LeftArrow),
                "KEY_CODE_RIGHT_ARROW" => Some(Self::RightArrow),
                "KEY_CODE_DOWN_ARROW" => Some(Self::DownArrow),
                "KEY_CODE_UP_ARROW" => Some(Self::UpArrow),
                "KEY_CODE_ISO_SELECTION" => Some(Self::IsoSelection),
                "KEY_CODE_JIS_YEN" => Some(Self::JisYen),
                "KEY_CODE_JIS_UNDERSCORE" => Some(Self::JisUnderscore),
                "KEY_CODE_JIS_KEYPAD_COMMA" => Some(Self::JisKeypadComma),
                "KEY_CODE_JIS_EISU" => Some(Self::JisEisu),
                "KEY_CODE_JIS_KANA" => Some(Self::JisKana),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Effect {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(bool, tag = "2")]
    pub enabled: bool,
    #[prost(string, tag = "3")]
    pub name: ::prost::alloc::string::String,
    #[prost(string, tag = "4")]
    pub render_id: ::prost::alloc::string::String,
    #[prost(string, tag = "5")]
    pub behavior_description: ::prost::alloc::string::String,
    #[prost(string, tag = "6")]
    pub category: ::prost::alloc::string::String,
    #[prost(message, repeated, tag = "7")]
    pub variables: ::prost::alloc::vec::Vec<effect::EffectVariable>,
}
/// Nested message and enum types in `Effect`.
pub mod effect {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct EffectVariable {
        #[prost(string, tag = "1")]
        pub name: ::prost::alloc::string::String,
        #[prost(string, tag = "2")]
        pub description: ::prost::alloc::string::String,
        #[prost(oneof = "effect_variable::Type", tags = "3, 4, 5, 6, 7")]
        pub r#type: ::core::option::Option<effect_variable::Type>,
    }
    /// Nested message and enum types in `EffectVariable`.
    pub mod effect_variable {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct EffectInt {
            #[prost(int32, tag = "1")]
            pub value: i32,
            #[prost(int32, tag = "2")]
            pub default_value: i32,
            #[prost(int32, tag = "3")]
            pub min: i32,
            #[prost(int32, tag = "4")]
            pub max: i32,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct EffectFloat {
            #[prost(float, tag = "1")]
            pub value: f32,
            #[prost(float, tag = "2")]
            pub default_value: f32,
            #[prost(float, tag = "3")]
            pub min: f32,
            #[prost(float, tag = "4")]
            pub max: f32,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct EffectDouble {
            #[prost(double, tag = "1")]
            pub value: f64,
            #[prost(double, tag = "2")]
            pub default_value: f64,
            #[prost(double, tag = "3")]
            pub min: f64,
            #[prost(double, tag = "4")]
            pub max: f64,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct EffectColor {
            #[prost(message, optional, tag = "1")]
            pub color: ::core::option::Option<super::super::Color>,
            #[prost(message, optional, tag = "2")]
            pub default_color: ::core::option::Option<super::super::Color>,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct EffectDirection {
            #[prost(enumeration = "effect_direction::EffectDirection", tag = "1")]
            pub direction: i32,
            #[prost(enumeration = "effect_direction::EffectDirection", tag = "2")]
            pub default_direction: i32,
            #[prost(uint32, tag = "3")]
            pub available_directions: u32,
        }
        /// Nested message and enum types in `EffectDirection`.
        pub mod effect_direction {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum EffectDirection {
                None = 0,
                TopLeft = 1,
                Top = 2,
                TopRight = 4,
                Left = 8,
                Center = 16,
                Right = 32,
                BottomLeft = 64,
                Bottom = 128,
                BottomRight = 256,
            }
            impl EffectDirection {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        EffectDirection::None => "EFFECT_DIRECTION_NONE",
                        EffectDirection::TopLeft => "EFFECT_DIRECTION_TOP_LEFT",
                        EffectDirection::Top => "EFFECT_DIRECTION_TOP",
                        EffectDirection::TopRight => "EFFECT_DIRECTION_TOP_RIGHT",
                        EffectDirection::Left => "EFFECT_DIRECTION_LEFT",
                        EffectDirection::Center => "EFFECT_DIRECTION_CENTER",
                        EffectDirection::Right => "EFFECT_DIRECTION_RIGHT",
                        EffectDirection::BottomLeft => "EFFECT_DIRECTION_BOTTOM_LEFT",
                        EffectDirection::Bottom => "EFFECT_DIRECTION_BOTTOM",
                        EffectDirection::BottomRight => "EFFECT_DIRECTION_BOTTOM_RIGHT",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "EFFECT_DIRECTION_NONE" => Some(Self::None),
                        "EFFECT_DIRECTION_TOP_LEFT" => Some(Self::TopLeft),
                        "EFFECT_DIRECTION_TOP" => Some(Self::Top),
                        "EFFECT_DIRECTION_TOP_RIGHT" => Some(Self::TopRight),
                        "EFFECT_DIRECTION_LEFT" => Some(Self::Left),
                        "EFFECT_DIRECTION_CENTER" => Some(Self::Center),
                        "EFFECT_DIRECTION_RIGHT" => Some(Self::Right),
                        "EFFECT_DIRECTION_BOTTOM_LEFT" => Some(Self::BottomLeft),
                        "EFFECT_DIRECTION_BOTTOM" => Some(Self::Bottom),
                        "EFFECT_DIRECTION_BOTTOM_RIGHT" => Some(Self::BottomRight),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Type {
            #[prost(message, tag = "3")]
            Int(EffectInt),
            #[prost(message, tag = "4")]
            Float(EffectFloat),
            #[prost(message, tag = "5")]
            Color(EffectColor),
            #[prost(message, tag = "6")]
            Direction(EffectDirection),
            #[prost(message, tag = "7")]
            Double(EffectDouble),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Preset {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, repeated, tag = "3")]
        pub effects: ::prost::alloc::vec::Vec<super::Effect>,
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Transition {
    #[prost(double, tag = "1")]
    pub duration: f64,
    #[prost(message, optional, tag = "2")]
    pub favorite_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(message, optional, tag = "3")]
    pub effect: ::core::option::Option<Effect>,
}
/// Nested message and enum types in `Transition`.
pub mod transition {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Preset {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub transition: ::core::option::Option<super::Transition>,
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Layer {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub name: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub color: ::core::option::Option<Color>,
    #[prost(bool, tag = "4")]
    pub muted: bool,
    #[prost(bool, tag = "5")]
    pub hidden: bool,
    #[prost(enumeration = "layer::BlendMode", tag = "6")]
    pub blend_mode: i32,
    #[prost(double, tag = "7")]
    pub opacity: f64,
    #[prost(message, optional, tag = "8")]
    pub selected_target_set_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(message, optional, tag = "9")]
    pub effects_preset_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(double, tag = "10")]
    pub effects_build_duration: f64,
    #[prost(message, optional, tag = "11")]
    pub layer_preset_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(message, optional, tag = "12")]
    pub hot_key: ::core::option::Option<HotKey>,
    #[prost(message, optional, tag = "13")]
    pub transition: ::core::option::Option<Transition>,
    #[prost(message, repeated, tag = "14")]
    pub effects: ::prost::alloc::vec::Vec<Effect>,
    #[prost(message, optional, tag = "15")]
    pub blend: ::core::option::Option<layer::Blending>,
}
/// Nested message and enum types in `Layer`.
pub mod layer {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Preset {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub layer: ::core::option::Option<super::Layer>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Blending {
        #[prost(oneof = "blending::BlendingType", tags = "1, 2")]
        pub blending_type: ::core::option::Option<blending::BlendingType>,
    }
    /// Nested message and enum types in `Blending`.
    pub mod blending {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Standard {
            #[prost(enumeration = "standard::BlendMode", tag = "1")]
            pub mode: i32,
            #[prost(double, tag = "2")]
            pub opacity: f64,
        }
        /// Nested message and enum types in `Standard`.
        pub mod standard {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum BlendMode {
                Normal = 0,
                Dissolve = 1,
                Darken = 2,
                Multiply = 3,
                ColorBurn = 4,
                LinearBurn = 5,
                DarkerColor = 6,
                Lighten = 7,
                Screen = 8,
                ColorDodge = 9,
                LinearDodge = 10,
                LighterColor = 11,
                Overlay = 12,
                SoftLight = 13,
                HardLight = 14,
                VividLight = 15,
                LinearLight = 16,
                PinLight = 17,
                HardMix = 18,
                Difference = 19,
                Exclusion = 20,
                Subtract = 21,
                Divide = 22,
                Hue = 23,
                Saturation = 24,
                Color = 25,
                Luminosity = 26,
            }
            impl BlendMode {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        BlendMode::Normal => "BLEND_MODE_NORMAL",
                        BlendMode::Dissolve => "BLEND_MODE_DISSOLVE",
                        BlendMode::Darken => "BLEND_MODE_DARKEN",
                        BlendMode::Multiply => "BLEND_MODE_MULTIPLY",
                        BlendMode::ColorBurn => "BLEND_MODE_COLOR_BURN",
                        BlendMode::LinearBurn => "BLEND_MODE_LINEAR_BURN",
                        BlendMode::DarkerColor => "BLEND_MODE_DARKER_COLOR",
                        BlendMode::Lighten => "BLEND_MODE_LIGHTEN",
                        BlendMode::Screen => "BLEND_MODE_SCREEN",
                        BlendMode::ColorDodge => "BLEND_MODE_COLOR_DODGE",
                        BlendMode::LinearDodge => "BLEND_MODE_LINEAR_DODGE",
                        BlendMode::LighterColor => "BLEND_MODE_LIGHTER_COLOR",
                        BlendMode::Overlay => "BLEND_MODE_OVERLAY",
                        BlendMode::SoftLight => "BLEND_MODE_SOFT_LIGHT",
                        BlendMode::HardLight => "BLEND_MODE_HARD_LIGHT",
                        BlendMode::VividLight => "BLEND_MODE_VIVID_LIGHT",
                        BlendMode::LinearLight => "BLEND_MODE_LINEAR_LIGHT",
                        BlendMode::PinLight => "BLEND_MODE_PIN_LIGHT",
                        BlendMode::HardMix => "BLEND_MODE_HARD_MIX",
                        BlendMode::Difference => "BLEND_MODE_DIFFERENCE",
                        BlendMode::Exclusion => "BLEND_MODE_EXCLUSION",
                        BlendMode::Subtract => "BLEND_MODE_SUBTRACT",
                        BlendMode::Divide => "BLEND_MODE_DIVIDE",
                        BlendMode::Hue => "BLEND_MODE_HUE",
                        BlendMode::Saturation => "BLEND_MODE_SATURATION",
                        BlendMode::Color => "BLEND_MODE_COLOR",
                        BlendMode::Luminosity => "BLEND_MODE_LUMINOSITY",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "BLEND_MODE_NORMAL" => Some(Self::Normal),
                        "BLEND_MODE_DISSOLVE" => Some(Self::Dissolve),
                        "BLEND_MODE_DARKEN" => Some(Self::Darken),
                        "BLEND_MODE_MULTIPLY" => Some(Self::Multiply),
                        "BLEND_MODE_COLOR_BURN" => Some(Self::ColorBurn),
                        "BLEND_MODE_LINEAR_BURN" => Some(Self::LinearBurn),
                        "BLEND_MODE_DARKER_COLOR" => Some(Self::DarkerColor),
                        "BLEND_MODE_LIGHTEN" => Some(Self::Lighten),
                        "BLEND_MODE_SCREEN" => Some(Self::Screen),
                        "BLEND_MODE_COLOR_DODGE" => Some(Self::ColorDodge),
                        "BLEND_MODE_LINEAR_DODGE" => Some(Self::LinearDodge),
                        "BLEND_MODE_LIGHTER_COLOR" => Some(Self::LighterColor),
                        "BLEND_MODE_OVERLAY" => Some(Self::Overlay),
                        "BLEND_MODE_SOFT_LIGHT" => Some(Self::SoftLight),
                        "BLEND_MODE_HARD_LIGHT" => Some(Self::HardLight),
                        "BLEND_MODE_VIVID_LIGHT" => Some(Self::VividLight),
                        "BLEND_MODE_LINEAR_LIGHT" => Some(Self::LinearLight),
                        "BLEND_MODE_PIN_LIGHT" => Some(Self::PinLight),
                        "BLEND_MODE_HARD_MIX" => Some(Self::HardMix),
                        "BLEND_MODE_DIFFERENCE" => Some(Self::Difference),
                        "BLEND_MODE_EXCLUSION" => Some(Self::Exclusion),
                        "BLEND_MODE_SUBTRACT" => Some(Self::Subtract),
                        "BLEND_MODE_DIVIDE" => Some(Self::Divide),
                        "BLEND_MODE_HUE" => Some(Self::Hue),
                        "BLEND_MODE_SATURATION" => Some(Self::Saturation),
                        "BLEND_MODE_COLOR" => Some(Self::Color),
                        "BLEND_MODE_LUMINOSITY" => Some(Self::Luminosity),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Matte {
            #[prost(oneof = "matte::MatteType", tags = "1, 2, 4")]
            pub matte_type: ::core::option::Option<matte::MatteType>,
        }
        /// Nested message and enum types in `Matte`.
        pub mod matte {
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Alpha {
                #[prost(bool, tag = "1")]
                pub inverted: bool,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Luma {
                #[prost(bool, tag = "1")]
                pub inverted: bool,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct White {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Oneof)]
            pub enum MatteType {
                #[prost(message, tag = "1")]
                Alpha(Alpha),
                #[prost(message, tag = "2")]
                Luma(Luma),
                #[prost(message, tag = "4")]
                White(White),
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum BlendingType {
            #[prost(message, tag = "1")]
            Standard(Standard),
            #[prost(message, tag = "2")]
            Matte(Matte),
        }
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum BlendMode {
        Normal = 0,
        Dissolve = 1,
        Darken = 2,
        Multiply = 3,
        ColorBurn = 4,
        LinearBurn = 5,
        DarkerColor = 6,
        Lighten = 7,
        Screen = 8,
        ColorDodge = 9,
        LinearDodge = 10,
        LighterColor = 11,
        Overlay = 12,
        SoftLight = 13,
        HardLight = 14,
        VividLight = 15,
        LinearLight = 16,
        PinLight = 17,
        HardMix = 18,
        Difference = 19,
        Exclusion = 20,
        Subtract = 21,
        Divide = 22,
        Hue = 23,
        Saturation = 24,
        Color = 25,
        Luminosity = 26,
    }
    impl BlendMode {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                BlendMode::Normal => "BLEND_MODE_NORMAL",
                BlendMode::Dissolve => "BLEND_MODE_DISSOLVE",
                BlendMode::Darken => "BLEND_MODE_DARKEN",
                BlendMode::Multiply => "BLEND_MODE_MULTIPLY",
                BlendMode::ColorBurn => "BLEND_MODE_COLOR_BURN",
                BlendMode::LinearBurn => "BLEND_MODE_LINEAR_BURN",
                BlendMode::DarkerColor => "BLEND_MODE_DARKER_COLOR",
                BlendMode::Lighten => "BLEND_MODE_LIGHTEN",
                BlendMode::Screen => "BLEND_MODE_SCREEN",
                BlendMode::ColorDodge => "BLEND_MODE_COLOR_DODGE",
                BlendMode::LinearDodge => "BLEND_MODE_LINEAR_DODGE",
                BlendMode::LighterColor => "BLEND_MODE_LIGHTER_COLOR",
                BlendMode::Overlay => "BLEND_MODE_OVERLAY",
                BlendMode::SoftLight => "BLEND_MODE_SOFT_LIGHT",
                BlendMode::HardLight => "BLEND_MODE_HARD_LIGHT",
                BlendMode::VividLight => "BLEND_MODE_VIVID_LIGHT",
                BlendMode::LinearLight => "BLEND_MODE_LINEAR_LIGHT",
                BlendMode::PinLight => "BLEND_MODE_PIN_LIGHT",
                BlendMode::HardMix => "BLEND_MODE_HARD_MIX",
                BlendMode::Difference => "BLEND_MODE_DIFFERENCE",
                BlendMode::Exclusion => "BLEND_MODE_EXCLUSION",
                BlendMode::Subtract => "BLEND_MODE_SUBTRACT",
                BlendMode::Divide => "BLEND_MODE_DIVIDE",
                BlendMode::Hue => "BLEND_MODE_HUE",
                BlendMode::Saturation => "BLEND_MODE_SATURATION",
                BlendMode::Color => "BLEND_MODE_COLOR",
                BlendMode::Luminosity => "BLEND_MODE_LUMINOSITY",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "BLEND_MODE_NORMAL" => Some(Self::Normal),
                "BLEND_MODE_DISSOLVE" => Some(Self::Dissolve),
                "BLEND_MODE_DARKEN" => Some(Self::Darken),
                "BLEND_MODE_MULTIPLY" => Some(Self::Multiply),
                "BLEND_MODE_COLOR_BURN" => Some(Self::ColorBurn),
                "BLEND_MODE_LINEAR_BURN" => Some(Self::LinearBurn),
                "BLEND_MODE_DARKER_COLOR" => Some(Self::DarkerColor),
                "BLEND_MODE_LIGHTEN" => Some(Self::Lighten),
                "BLEND_MODE_SCREEN" => Some(Self::Screen),
                "BLEND_MODE_COLOR_DODGE" => Some(Self::ColorDodge),
                "BLEND_MODE_LINEAR_DODGE" => Some(Self::LinearDodge),
                "BLEND_MODE_LIGHTER_COLOR" => Some(Self::LighterColor),
                "BLEND_MODE_OVERLAY" => Some(Self::Overlay),
                "BLEND_MODE_SOFT_LIGHT" => Some(Self::SoftLight),
                "BLEND_MODE_HARD_LIGHT" => Some(Self::HardLight),
                "BLEND_MODE_VIVID_LIGHT" => Some(Self::VividLight),
                "BLEND_MODE_LINEAR_LIGHT" => Some(Self::LinearLight),
                "BLEND_MODE_PIN_LIGHT" => Some(Self::PinLight),
                "BLEND_MODE_HARD_MIX" => Some(Self::HardMix),
                "BLEND_MODE_DIFFERENCE" => Some(Self::Difference),
                "BLEND_MODE_EXCLUSION" => Some(Self::Exclusion),
                "BLEND_MODE_SUBTRACT" => Some(Self::Subtract),
                "BLEND_MODE_DIVIDE" => Some(Self::Divide),
                "BLEND_MODE_HUE" => Some(Self::Hue),
                "BLEND_MODE_SATURATION" => Some(Self::Saturation),
                "BLEND_MODE_COLOR" => Some(Self::Color),
                "BLEND_MODE_LUMINOSITY" => Some(Self::Luminosity),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DigitalAudio {}
/// Nested message and enum types in `DigitalAudio`.
pub mod digital_audio {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Setup {
        #[prost(message, repeated, tag = "1")]
        pub buses: ::prost::alloc::vec::Vec<Bus>,
        #[prost(message, optional, tag = "2")]
        pub monitor_device: ::core::option::Option<Device>,
        #[prost(message, optional, tag = "3")]
        pub main_output_device: ::core::option::Option<Device>,
        #[prost(bool, tag = "4")]
        pub enable_sdi_ndi_device: bool,
        #[prost(message, optional, tag = "5")]
        pub sdi_ndi_device: ::core::option::Option<Device>,
        #[prost(bool, tag = "6")]
        pub monitor_on_mains: bool,
        #[prost(bool, tag = "7")]
        pub disable_main_output_device: bool,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Bus {
        #[prost(string, tag = "1")]
        pub name: ::prost::alloc::string::String,
        #[prost(bool, tag = "2")]
        pub muted: bool,
        #[prost(bool, tag = "3")]
        pub solo: bool,
        #[prost(bool, tag = "4")]
        pub test_tone: bool,
        #[prost(double, tag = "5")]
        pub master_level: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Device {
        #[prost(string, tag = "1")]
        pub name: ::prost::alloc::string::String,
        #[prost(string, tag = "2")]
        pub render_id: ::prost::alloc::string::String,
        #[prost(message, repeated, tag = "3")]
        pub formats: ::prost::alloc::vec::Vec<device::Format>,
        #[prost(message, optional, tag = "4")]
        pub routing: ::core::option::Option<device::Routing>,
    }
    /// Nested message and enum types in `Device`.
    pub mod device {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Format {
            #[prost(uint32, tag = "1")]
            pub sample_rate: u32,
            #[prost(uint32, tag = "2")]
            pub bit_depth: u32,
            #[prost(enumeration = "format::Type", tag = "3")]
            pub r#type: i32,
        }
        /// Nested message and enum types in `Format`.
        pub mod format {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Type {
                Int = 0,
                Float = 1,
            }
            impl Type {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Type::Int => "TYPE_INT",
                        Type::Float => "TYPE_FLOAT",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "TYPE_INT" => Some(Self::Int),
                        "TYPE_FLOAT" => Some(Self::Float),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Map {
            #[prost(uint32, tag = "1")]
            pub channel_index: u32,
            #[prost(uint32, repeated, tag = "2")]
            pub mapped_indices: ::prost::alloc::vec::Vec<u32>,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Channel {
            #[prost(bool, tag = "1")]
            pub mute_enable: bool,
            #[prost(bool, tag = "2")]
            pub solo_enable: bool,
            #[prost(bool, tag = "3")]
            pub tone_enable: bool,
            #[prost(double, tag = "4")]
            pub audio_delay: f64,
            #[prost(double, tag = "5")]
            pub level: f64,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Routing {
            #[prost(message, repeated, tag = "1")]
            pub channels: ::prost::alloc::vec::Vec<Channel>,
            #[prost(message, repeated, tag = "2")]
            pub map: ::prost::alloc::vec::Vec<Map>,
            #[prost(bool, tag = "3")]
            pub is_custom_map: bool,
            #[prost(message, optional, tag = "4")]
            pub master_channel: ::core::option::Option<Channel>,
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Graphics {}
/// Nested message and enum types in `Graphics`.
pub mod graphics {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Element {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub bounds: ::core::option::Option<Rect>,
        #[prost(double, tag = "4")]
        pub rotation: f64,
        #[prost(double, tag = "5")]
        pub opacity: f64,
        #[prost(bool, tag = "6")]
        pub locked: bool,
        #[prost(bool, tag = "7")]
        pub aspect_ratio_locked: bool,
        #[prost(message, optional, tag = "8")]
        pub path: ::core::option::Option<Path>,
        #[prost(message, optional, tag = "9")]
        pub fill: ::core::option::Option<Fill>,
        #[prost(message, optional, tag = "10")]
        pub stroke: ::core::option::Option<Stroke>,
        #[prost(message, optional, tag = "11")]
        pub shadow: ::core::option::Option<Shadow>,
        #[prost(message, optional, tag = "12")]
        pub feather: ::core::option::Option<Feather>,
        #[prost(message, optional, tag = "13")]
        pub text: ::core::option::Option<Text>,
        #[prost(enumeration = "element::FlipMode", tag = "15")]
        pub flip_mode: i32,
        #[prost(bool, tag = "16")]
        pub hidden: bool,
        #[prost(oneof = "element::Mask", tags = "14")]
        pub mask: ::core::option::Option<element::Mask>,
    }
    /// Nested message and enum types in `Element`.
    pub mod element {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum FlipMode {
            None = 0,
            Vertical = 1,
            Horizontal = 2,
            Both = 3,
        }
        impl FlipMode {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    FlipMode::None => "FLIP_MODE_NONE",
                    FlipMode::Vertical => "FLIP_MODE_VERTICAL",
                    FlipMode::Horizontal => "FLIP_MODE_HORIZONTAL",
                    FlipMode::Both => "FLIP_MODE_BOTH",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "FLIP_MODE_NONE" => Some(Self::None),
                    "FLIP_MODE_VERTICAL" => Some(Self::Vertical),
                    "FLIP_MODE_HORIZONTAL" => Some(Self::Horizontal),
                    "FLIP_MODE_BOTH" => Some(Self::Both),
                    _ => None,
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Mask {
            #[prost(message, tag = "14")]
            TextLineMask(super::text::LineFillMask),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Rect {
        #[prost(message, optional, tag = "1")]
        pub origin: ::core::option::Option<Point>,
        #[prost(message, optional, tag = "2")]
        pub size: ::core::option::Option<Size>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Point {
        #[prost(double, tag = "1")]
        pub x: f64,
        #[prost(double, tag = "2")]
        pub y: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Size {
        #[prost(double, tag = "1")]
        pub width: f64,
        #[prost(double, tag = "2")]
        pub height: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct EdgeInsets {
        #[prost(double, tag = "1")]
        pub left: f64,
        #[prost(double, tag = "2")]
        pub right: f64,
        #[prost(double, tag = "3")]
        pub top: f64,
        #[prost(double, tag = "4")]
        pub bottom: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Path {
        #[prost(bool, tag = "1")]
        pub closed: bool,
        #[prost(message, repeated, tag = "2")]
        pub points: ::prost::alloc::vec::Vec<path::BezierPoint>,
        #[prost(message, optional, tag = "3")]
        pub shape: ::core::option::Option<path::Shape>,
    }
    /// Nested message and enum types in `Path`.
    pub mod path {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct BezierPoint {
            #[prost(message, optional, tag = "1")]
            pub point: ::core::option::Option<super::Point>,
            #[prost(message, optional, tag = "2")]
            pub q0: ::core::option::Option<super::Point>,
            #[prost(message, optional, tag = "3")]
            pub q1: ::core::option::Option<super::Point>,
            #[prost(bool, tag = "4")]
            pub curved: bool,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Shape {
            #[prost(enumeration = "shape::Type", tag = "1")]
            pub r#type: i32,
            #[prost(oneof = "shape::AdditionalData", tags = "2, 3, 4, 5")]
            pub additional_data: ::core::option::Option<shape::AdditionalData>,
        }
        /// Nested message and enum types in `Shape`.
        pub mod shape {
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct RoundedRectangle {
                #[prost(double, tag = "1")]
                pub roundness: f64,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Arrow {
                #[prost(message, optional, tag = "1")]
                pub corner: ::core::option::Option<super::super::Point>,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Polygon {
                #[prost(uint32, tag = "1")]
                pub number_sides: u32,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Star {
                #[prost(double, tag = "1")]
                pub inner_radius: f64,
                #[prost(uint32, tag = "2")]
                pub number_points: u32,
            }
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Type {
                Unknown = 0,
                Rectangle = 1,
                Ellipse = 2,
                IsoscelesTriangle = 3,
                RightTriangle = 4,
                Rhombus = 5,
                Star = 6,
                Polygon = 7,
                Custom = 8,
                RightArrow = 9,
                DoubleArrow = 10,
                RoundedRectangle = 11,
            }
            impl Type {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Type::Unknown => "TYPE_UNKNOWN",
                        Type::Rectangle => "TYPE_RECTANGLE",
                        Type::Ellipse => "TYPE_ELLIPSE",
                        Type::IsoscelesTriangle => "TYPE_ISOSCELES_TRIANGLE",
                        Type::RightTriangle => "TYPE_RIGHT_TRIANGLE",
                        Type::Rhombus => "TYPE_RHOMBUS",
                        Type::Star => "TYPE_STAR",
                        Type::Polygon => "TYPE_POLYGON",
                        Type::Custom => "TYPE_CUSTOM",
                        Type::RightArrow => "TYPE_RIGHT_ARROW",
                        Type::DoubleArrow => "TYPE_DOUBLE_ARROW",
                        Type::RoundedRectangle => "TYPE_ROUNDED_RECTANGLE",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "TYPE_UNKNOWN" => Some(Self::Unknown),
                        "TYPE_RECTANGLE" => Some(Self::Rectangle),
                        "TYPE_ELLIPSE" => Some(Self::Ellipse),
                        "TYPE_ISOSCELES_TRIANGLE" => Some(Self::IsoscelesTriangle),
                        "TYPE_RIGHT_TRIANGLE" => Some(Self::RightTriangle),
                        "TYPE_RHOMBUS" => Some(Self::Rhombus),
                        "TYPE_STAR" => Some(Self::Star),
                        "TYPE_POLYGON" => Some(Self::Polygon),
                        "TYPE_CUSTOM" => Some(Self::Custom),
                        "TYPE_RIGHT_ARROW" => Some(Self::RightArrow),
                        "TYPE_DOUBLE_ARROW" => Some(Self::DoubleArrow),
                        "TYPE_ROUNDED_RECTANGLE" => Some(Self::RoundedRectangle),
                        _ => None,
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Oneof)]
            pub enum AdditionalData {
                #[prost(message, tag = "2")]
                RoundedRectangle(RoundedRectangle),
                #[prost(message, tag = "3")]
                Polygon(Polygon),
                #[prost(message, tag = "4")]
                Star(Star),
                #[prost(message, tag = "5")]
                Arrow(Arrow),
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Fill {
        #[prost(bool, tag = "4")]
        pub enable: bool,
        #[prost(oneof = "fill::FillType", tags = "1, 2, 3, 5")]
        pub fill_type: ::core::option::Option<fill::FillType>,
    }
    /// Nested message and enum types in `Fill`.
    pub mod fill {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum FillType {
            #[prost(message, tag = "1")]
            Color(super::super::Color),
            #[prost(message, tag = "2")]
            Gradient(super::Gradient),
            #[prost(message, tag = "3")]
            Media(super::super::Media),
            #[prost(message, tag = "5")]
            BackgroundEffect(super::BackgroundEffect),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BackgroundEffect {
        #[prost(oneof = "background_effect::EffectType", tags = "2, 3")]
        pub effect_type: ::core::option::Option<background_effect::EffectType>,
    }
    /// Nested message and enum types in `BackgroundEffect`.
    pub mod background_effect {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct BackgroundEffectBlur {
            #[prost(double, tag = "1")]
            pub saturation: f64,
            #[prost(double, tag = "2")]
            pub blur_amount: f64,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct BackgroundEffectInvert {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum EffectType {
            #[prost(message, tag = "2")]
            BackgroundBlur(BackgroundEffectBlur),
            #[prost(message, tag = "3")]
            BackgroundInvert(BackgroundEffectInvert),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Gradient {
        #[prost(enumeration = "gradient::Type", tag = "1")]
        pub r#type: i32,
        #[prost(double, tag = "2")]
        pub angle: f64,
        #[prost(double, tag = "3")]
        pub length: f64,
        #[prost(message, repeated, tag = "4")]
        pub stops: ::prost::alloc::vec::Vec<gradient::ColorStop>,
    }
    /// Nested message and enum types in `Gradient`.
    pub mod gradient {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct ColorStop {
            #[prost(message, optional, tag = "1")]
            pub color: ::core::option::Option<super::super::Color>,
            #[prost(double, tag = "2")]
            pub position: f64,
            #[prost(double, tag = "3")]
            pub blend_point: f64,
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Type {
            Linear = 0,
            Radial = 1,
            Angle = 2,
        }
        impl Type {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Type::Linear => "TYPE_LINEAR",
                    Type::Radial => "TYPE_RADIAL",
                    Type::Angle => "TYPE_ANGLE",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "TYPE_LINEAR" => Some(Self::Linear),
                    "TYPE_RADIAL" => Some(Self::Radial),
                    "TYPE_ANGLE" => Some(Self::Angle),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Shadow {
        #[prost(enumeration = "shadow::Style", tag = "1")]
        pub style: i32,
        #[prost(double, tag = "2")]
        pub angle: f64,
        #[prost(double, tag = "3")]
        pub offset: f64,
        #[prost(double, tag = "4")]
        pub radius: f64,
        #[prost(message, optional, tag = "5")]
        pub color: ::core::option::Option<super::Color>,
        #[prost(double, tag = "6")]
        pub opacity: f64,
        #[prost(bool, tag = "7")]
        pub enable: bool,
    }
    /// Nested message and enum types in `Shadow`.
    pub mod shadow {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Style {
            Drop = 0,
        }
        impl Style {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Style::Drop => "STYLE_DROP",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "STYLE_DROP" => Some(Self::Drop),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Stroke {
        #[prost(enumeration = "stroke::Style", tag = "1")]
        pub style: i32,
        #[prost(double, tag = "2")]
        pub width: f64,
        #[prost(message, optional, tag = "3")]
        pub color: ::core::option::Option<super::Color>,
        #[prost(double, repeated, tag = "4")]
        pub pattern: ::prost::alloc::vec::Vec<f64>,
        #[prost(bool, tag = "5")]
        pub enable: bool,
    }
    /// Nested message and enum types in `Stroke`.
    pub mod stroke {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Style {
            SolidLine = 0,
            SquareDash = 1,
            ShortDash = 2,
            LongDash = 3,
        }
        impl Style {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Style::SolidLine => "STYLE_SOLID_LINE",
                    Style::SquareDash => "STYLE_SQUARE_DASH",
                    Style::ShortDash => "STYLE_SHORT_DASH",
                    Style::LongDash => "STYLE_LONG_DASH",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "STYLE_SOLID_LINE" => Some(Self::SolidLine),
                    "STYLE_SQUARE_DASH" => Some(Self::SquareDash),
                    "STYLE_SHORT_DASH" => Some(Self::ShortDash),
                    "STYLE_LONG_DASH" => Some(Self::LongDash),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Feather {
        #[prost(enumeration = "feather::Style", tag = "1")]
        pub style: i32,
        #[prost(double, tag = "2")]
        pub radius: f64,
        #[prost(bool, tag = "3")]
        pub enable: bool,
    }
    /// Nested message and enum types in `Feather`.
    pub mod feather {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Style {
            Inside = 0,
            Center = 1,
            Outside = 2,
        }
        impl Style {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Style::Inside => "STYLE_INSIDE",
                    Style::Center => "STYLE_CENTER",
                    Style::Outside => "STYLE_OUTSIDE",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "STYLE_INSIDE" => Some(Self::Inside),
                    "STYLE_CENTER" => Some(Self::Center),
                    "STYLE_OUTSIDE" => Some(Self::Outside),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Text {
        #[prost(message, optional, tag = "3")]
        pub attributes: ::core::option::Option<text::Attributes>,
        #[prost(message, optional, tag = "4")]
        pub shadow: ::core::option::Option<Shadow>,
        #[prost(bytes = "vec", tag = "5")]
        pub rtf_data: ::prost::alloc::vec::Vec<u8>,
        #[prost(enumeration = "text::VerticalAlignment", tag = "6")]
        pub vertical_alignment: i32,
        #[prost(enumeration = "text::ScaleBehavior", tag = "7")]
        pub scale_behavior: i32,
        #[prost(message, optional, tag = "8")]
        pub margins: ::core::option::Option<EdgeInsets>,
        #[prost(bool, tag = "9")]
        pub is_superscript_standardized: bool,
        #[prost(enumeration = "text::Transform", tag = "10")]
        pub transform: i32,
        #[prost(string, tag = "11")]
        pub transform_delimiter: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "12")]
        pub chord_pro: ::core::option::Option<text::ChordPro>,
    }
    /// Nested message and enum types in `Text`.
    pub mod text {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct LineFillMask {
            #[prost(bool, tag = "1")]
            pub enabled: bool,
            #[prost(double, tag = "2")]
            pub height_offset: f64,
            #[prost(double, tag = "3")]
            pub vertical_offset: f64,
            #[prost(enumeration = "line_fill_mask::LineMaskStyle", tag = "4")]
            pub mask_style: i32,
            #[prost(double, tag = "5")]
            pub width_offset: f64,
            #[prost(double, tag = "6")]
            pub horizontal_offset: f64,
        }
        /// Nested message and enum types in `LineFillMask`.
        pub mod line_fill_mask {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum LineMaskStyle {
                FullWidth = 0,
                LineWidth = 1,
                MaxLineWidth = 2,
            }
            impl LineMaskStyle {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        LineMaskStyle::FullWidth => "LINE_MASK_STYLE_FULL_WIDTH",
                        LineMaskStyle::LineWidth => "LINE_MASK_STYLE_LINE_WIDTH",
                        LineMaskStyle::MaxLineWidth => "LINE_MASK_STYLE_MAX_LINE_WIDTH",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "LINE_MASK_STYLE_FULL_WIDTH" => Some(Self::FullWidth),
                        "LINE_MASK_STYLE_LINE_WIDTH" => Some(Self::LineWidth),
                        "LINE_MASK_STYLE_MAX_LINE_WIDTH" => Some(Self::MaxLineWidth),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct GradientFill {
            #[prost(message, optional, tag = "1")]
            pub gradient: ::core::option::Option<super::Gradient>,
            #[prost(bool, tag = "2")]
            pub stretch_to_document_bounds: bool,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct CutOutFill {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct MediaFill {
            #[prost(message, optional, tag = "1")]
            pub media: ::core::option::Option<super::super::Media>,
            #[prost(bool, tag = "2")]
            pub use_object_bounds: bool,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct ChordPro {
            #[prost(bool, tag = "1")]
            pub enabled: bool,
            #[prost(enumeration = "chord_pro::Notation", tag = "2")]
            pub notation: i32,
            #[prost(message, optional, tag = "3")]
            pub color: ::core::option::Option<super::super::Color>,
        }
        /// Nested message and enum types in `ChordPro`.
        pub mod chord_pro {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Notation {
                Chords = 0,
                Numbers = 1,
                Numerals = 2,
                DoReMi = 3,
            }
            impl Notation {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Notation::Chords => "NOTATION_CHORDS",
                        Notation::Numbers => "NOTATION_NUMBERS",
                        Notation::Numerals => "NOTATION_NUMERALS",
                        Notation::DoReMi => "NOTATION_DO_RE_MI",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "NOTATION_CHORDS" => Some(Self::Chords),
                        "NOTATION_NUMBERS" => Some(Self::Numbers),
                        "NOTATION_NUMERALS" => Some(Self::Numerals),
                        "NOTATION_DO_RE_MI" => Some(Self::DoReMi),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Attributes {
            #[prost(message, optional, tag = "1")]
            pub font: ::core::option::Option<attributes::Font>,
            #[prost(enumeration = "attributes::Capitalization", tag = "2")]
            pub capitalization: i32,
            #[prost(message, optional, tag = "4")]
            pub underline_style: ::core::option::Option<attributes::Underline>,
            #[prost(message, optional, tag = "5")]
            pub underline_color: ::core::option::Option<super::super::Color>,
            #[prost(message, optional, tag = "6")]
            pub paragraph_style: ::core::option::Option<attributes::Paragraph>,
            #[prost(double, tag = "7")]
            pub kerning: f64,
            #[prost(int32, tag = "8")]
            pub superscript: i32,
            #[prost(message, optional, tag = "9")]
            pub strikethrough_style: ::core::option::Option<attributes::Underline>,
            #[prost(message, optional, tag = "10")]
            pub strikethrough_color: ::core::option::Option<super::super::Color>,
            #[prost(double, tag = "11")]
            pub stroke_width: f64,
            #[prost(message, optional, tag = "12")]
            pub stroke_color: ::core::option::Option<super::super::Color>,
            #[prost(message, repeated, tag = "13")]
            pub custom_attributes: ::prost::alloc::vec::Vec<attributes::CustomAttribute>,
            #[prost(message, optional, tag = "15")]
            pub background_color: ::core::option::Option<super::super::Color>,
            #[prost(oneof = "attributes::Fill", tags = "3, 14, 16, 17, 18")]
            pub fill: ::core::option::Option<attributes::Fill>,
        }
        /// Nested message and enum types in `Attributes`.
        pub mod attributes {
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Font {
                #[prost(string, tag = "1")]
                pub name: ::prost::alloc::string::String,
                #[prost(double, tag = "2")]
                pub size: f64,
                #[prost(bool, tag = "4")]
                pub italic: bool,
                #[prost(bool, tag = "8")]
                pub bold: bool,
                #[prost(string, tag = "9")]
                pub family: ::prost::alloc::string::String,
                #[prost(string, tag = "10")]
                pub face: ::prost::alloc::string::String,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Underline {
                #[prost(enumeration = "underline::Style", tag = "1")]
                pub style: i32,
                #[prost(enumeration = "underline::Pattern", tag = "2")]
                pub pattern: i32,
                #[prost(bool, tag = "3")]
                pub by_word: bool,
            }
            /// Nested message and enum types in `Underline`.
            pub mod underline {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Style {
                    None = 0,
                    Single = 1,
                    Thick = 2,
                    Double = 3,
                }
                impl Style {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Style::None => "STYLE_NONE",
                            Style::Single => "STYLE_SINGLE",
                            Style::Thick => "STYLE_THICK",
                            Style::Double => "STYLE_DOUBLE",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "STYLE_NONE" => Some(Self::None),
                            "STYLE_SINGLE" => Some(Self::Single),
                            "STYLE_THICK" => Some(Self::Thick),
                            "STYLE_DOUBLE" => Some(Self::Double),
                            _ => None,
                        }
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Pattern {
                    Solid = 0,
                    Dot = 1,
                    Dash = 2,
                    DashDot = 3,
                    DashDotDot = 4,
                }
                impl Pattern {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Pattern::Solid => "PATTERN_SOLID",
                            Pattern::Dot => "PATTERN_DOT",
                            Pattern::Dash => "PATTERN_DASH",
                            Pattern::DashDot => "PATTERN_DASH_DOT",
                            Pattern::DashDotDot => "PATTERN_DASH_DOT_DOT",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "PATTERN_SOLID" => Some(Self::Solid),
                            "PATTERN_DOT" => Some(Self::Dot),
                            "PATTERN_DASH" => Some(Self::Dash),
                            "PATTERN_DASH_DOT" => Some(Self::DashDot),
                            "PATTERN_DASH_DOT_DOT" => Some(Self::DashDotDot),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Paragraph {
                #[prost(enumeration = "paragraph::Alignment", tag = "1")]
                pub alignment: i32,
                #[prost(double, tag = "2")]
                pub first_line_head_indent: f64,
                #[prost(double, tag = "3")]
                pub head_indent: f64,
                #[prost(double, tag = "4")]
                pub tail_indent: f64,
                #[prost(double, tag = "5")]
                pub line_height_multiple: f64,
                #[prost(double, tag = "6")]
                pub maximum_line_height: f64,
                #[prost(double, tag = "7")]
                pub minimum_line_height: f64,
                #[prost(double, tag = "8")]
                pub line_spacing: f64,
                #[prost(double, tag = "9")]
                pub paragraph_spacing: f64,
                #[prost(double, tag = "10")]
                pub paragraph_spacing_before: f64,
                #[prost(message, repeated, tag = "11")]
                pub tab_stops: ::prost::alloc::vec::Vec<paragraph::TabStop>,
                #[prost(double, tag = "12")]
                pub default_tab_interval: f64,
                #[prost(message, optional, tag = "13")]
                pub text_list: ::core::option::Option<paragraph::TextList>,
                #[prost(message, repeated, tag = "14")]
                pub text_lists: ::prost::alloc::vec::Vec<paragraph::TextList>,
            }
            /// Nested message and enum types in `Paragraph`.
            pub mod paragraph {
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct TabStop {
                    #[prost(double, tag = "1")]
                    pub location: f64,
                    #[prost(enumeration = "tab_stop::Alignment", tag = "2")]
                    pub alignment: i32,
                }
                /// Nested message and enum types in `TabStop`.
                pub mod tab_stop {
                    #[derive(
                        Clone,
                        Copy,
                        Debug,
                        PartialEq,
                        Eq,
                        Hash,
                        PartialOrd,
                        Ord,
                        ::prost::Enumeration
                    )]
                    #[repr(i32)]
                    pub enum Alignment {
                        Left = 0,
                        Right = 1,
                        Center = 2,
                        Justified = 3,
                        Natural = 4,
                    }
                    impl Alignment {
                        /// String value of the enum field names used in the ProtoBuf definition.
                        ///
                        /// The values are not transformed in any way and thus are considered stable
                        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                        pub fn as_str_name(&self) -> &'static str {
                            match self {
                                Alignment::Left => "ALIGNMENT_LEFT",
                                Alignment::Right => "ALIGNMENT_RIGHT",
                                Alignment::Center => "ALIGNMENT_CENTER",
                                Alignment::Justified => "ALIGNMENT_JUSTIFIED",
                                Alignment::Natural => "ALIGNMENT_NATURAL",
                            }
                        }
                        /// Creates an enum from field names used in the ProtoBuf definition.
                        pub fn from_str_name(
                            value: &str,
                        ) -> ::core::option::Option<Self> {
                            match value {
                                "ALIGNMENT_LEFT" => Some(Self::Left),
                                "ALIGNMENT_RIGHT" => Some(Self::Right),
                                "ALIGNMENT_CENTER" => Some(Self::Center),
                                "ALIGNMENT_JUSTIFIED" => Some(Self::Justified),
                                "ALIGNMENT_NATURAL" => Some(Self::Natural),
                                _ => None,
                            }
                        }
                    }
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct TextList {
                    #[prost(bool, tag = "1")]
                    pub is_enabled: bool,
                    #[prost(enumeration = "text_list::NumberType", tag = "2")]
                    pub number_type: i32,
                    #[prost(string, tag = "3")]
                    pub prefix: ::prost::alloc::string::String,
                    #[prost(string, tag = "4")]
                    pub postfix: ::prost::alloc::string::String,
                    #[prost(int32, tag = "5")]
                    pub starting_number: i32,
                }
                /// Nested message and enum types in `TextList`.
                pub mod text_list {
                    #[derive(
                        Clone,
                        Copy,
                        Debug,
                        PartialEq,
                        Eq,
                        Hash,
                        PartialOrd,
                        Ord,
                        ::prost::Enumeration
                    )]
                    #[repr(i32)]
                    pub enum NumberType {
                        Box = 0,
                        Check = 1,
                        Circle = 2,
                        Diamond = 3,
                        Disc = 4,
                        Hyphen = 5,
                        Square = 6,
                        Decimal = 7,
                        LowercaseAlpha = 8,
                        UppercaseAlpha = 9,
                        LowercaseRoman = 10,
                        UppercaseRoman = 11,
                    }
                    impl NumberType {
                        /// String value of the enum field names used in the ProtoBuf definition.
                        ///
                        /// The values are not transformed in any way and thus are considered stable
                        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                        pub fn as_str_name(&self) -> &'static str {
                            match self {
                                NumberType::Box => "NUMBER_TYPE_BOX",
                                NumberType::Check => "NUMBER_TYPE_CHECK",
                                NumberType::Circle => "NUMBER_TYPE_CIRCLE",
                                NumberType::Diamond => "NUMBER_TYPE_DIAMOND",
                                NumberType::Disc => "NUMBER_TYPE_DISC",
                                NumberType::Hyphen => "NUMBER_TYPE_HYPHEN",
                                NumberType::Square => "NUMBER_TYPE_SQUARE",
                                NumberType::Decimal => "NUMBER_TYPE_DECIMAL",
                                NumberType::LowercaseAlpha => "NUMBER_TYPE_LOWERCASE_ALPHA",
                                NumberType::UppercaseAlpha => "NUMBER_TYPE_UPPERCASE_ALPHA",
                                NumberType::LowercaseRoman => "NUMBER_TYPE_LOWERCASE_ROMAN",
                                NumberType::UppercaseRoman => "NUMBER_TYPE_UPPERCASE_ROMAN",
                            }
                        }
                        /// Creates an enum from field names used in the ProtoBuf definition.
                        pub fn from_str_name(
                            value: &str,
                        ) -> ::core::option::Option<Self> {
                            match value {
                                "NUMBER_TYPE_BOX" => Some(Self::Box),
                                "NUMBER_TYPE_CHECK" => Some(Self::Check),
                                "NUMBER_TYPE_CIRCLE" => Some(Self::Circle),
                                "NUMBER_TYPE_DIAMOND" => Some(Self::Diamond),
                                "NUMBER_TYPE_DISC" => Some(Self::Disc),
                                "NUMBER_TYPE_HYPHEN" => Some(Self::Hyphen),
                                "NUMBER_TYPE_SQUARE" => Some(Self::Square),
                                "NUMBER_TYPE_DECIMAL" => Some(Self::Decimal),
                                "NUMBER_TYPE_LOWERCASE_ALPHA" => Some(Self::LowercaseAlpha),
                                "NUMBER_TYPE_UPPERCASE_ALPHA" => Some(Self::UppercaseAlpha),
                                "NUMBER_TYPE_LOWERCASE_ROMAN" => Some(Self::LowercaseRoman),
                                "NUMBER_TYPE_UPPERCASE_ROMAN" => Some(Self::UppercaseRoman),
                                _ => None,
                            }
                        }
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Alignment {
                    Left = 0,
                    Right = 1,
                    Center = 2,
                    Justified = 3,
                    Natural = 4,
                }
                impl Alignment {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Alignment::Left => "ALIGNMENT_LEFT",
                            Alignment::Right => "ALIGNMENT_RIGHT",
                            Alignment::Center => "ALIGNMENT_CENTER",
                            Alignment::Justified => "ALIGNMENT_JUSTIFIED",
                            Alignment::Natural => "ALIGNMENT_NATURAL",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "ALIGNMENT_LEFT" => Some(Self::Left),
                            "ALIGNMENT_RIGHT" => Some(Self::Right),
                            "ALIGNMENT_CENTER" => Some(Self::Center),
                            "ALIGNMENT_JUSTIFIED" => Some(Self::Justified),
                            "ALIGNMENT_NATURAL" => Some(Self::Natural),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct CustomAttribute {
                #[prost(message, optional, tag = "1")]
                pub range: ::core::option::Option<super::super::super::IntRange>,
                #[prost(
                    oneof = "custom_attribute::Attribute",
                    tags = "2, 3, 4, 5, 6, 7, 8, 9, 10"
                )]
                pub attribute: ::core::option::Option<custom_attribute::Attribute>,
            }
            /// Nested message and enum types in `CustomAttribute`.
            pub mod custom_attribute {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Capitalization {
                    None = 0,
                    AllCaps = 1,
                    SmallCaps = 2,
                    TitleCase = 3,
                    StartCase = 4,
                }
                impl Capitalization {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Capitalization::None => "CAPITALIZATION_NONE",
                            Capitalization::AllCaps => "CAPITALIZATION_ALL_CAPS",
                            Capitalization::SmallCaps => "CAPITALIZATION_SMALL_CAPS",
                            Capitalization::TitleCase => "CAPITALIZATION_TITLE_CASE",
                            Capitalization::StartCase => "CAPITALIZATION_START_CASE",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "CAPITALIZATION_NONE" => Some(Self::None),
                            "CAPITALIZATION_ALL_CAPS" => Some(Self::AllCaps),
                            "CAPITALIZATION_SMALL_CAPS" => Some(Self::SmallCaps),
                            "CAPITALIZATION_TITLE_CASE" => Some(Self::TitleCase),
                            "CAPITALIZATION_START_CASE" => Some(Self::StartCase),
                            _ => None,
                        }
                    }
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Oneof)]
                pub enum Attribute {
                    #[prost(enumeration = "Capitalization", tag = "2")]
                    Capitalization(i32),
                    #[prost(double, tag = "3")]
                    OriginalFontSize(f64),
                    #[prost(double, tag = "4")]
                    FontScaleFactor(f64),
                    #[prost(message, tag = "5")]
                    TextGradientFill(super::super::GradientFill),
                    #[prost(bool, tag = "6")]
                    ShouldPreserveForegroundColor(bool),
                    #[prost(string, tag = "7")]
                    Chord(::prost::alloc::string::String),
                    #[prost(message, tag = "8")]
                    CutOutFill(super::super::CutOutFill),
                    #[prost(message, tag = "9")]
                    MediaFill(super::super::MediaFill),
                    #[prost(message, tag = "10")]
                    BackgroundEffect(super::super::super::BackgroundEffect),
                }
            }
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Capitalization {
                None = 0,
                AllCaps = 1,
                SmallCaps = 2,
                TitleCase = 3,
                StartCase = 4,
            }
            impl Capitalization {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Capitalization::None => "CAPITALIZATION_NONE",
                        Capitalization::AllCaps => "CAPITALIZATION_ALL_CAPS",
                        Capitalization::SmallCaps => "CAPITALIZATION_SMALL_CAPS",
                        Capitalization::TitleCase => "CAPITALIZATION_TITLE_CASE",
                        Capitalization::StartCase => "CAPITALIZATION_START_CASE",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "CAPITALIZATION_NONE" => Some(Self::None),
                        "CAPITALIZATION_ALL_CAPS" => Some(Self::AllCaps),
                        "CAPITALIZATION_SMALL_CAPS" => Some(Self::SmallCaps),
                        "CAPITALIZATION_TITLE_CASE" => Some(Self::TitleCase),
                        "CAPITALIZATION_START_CASE" => Some(Self::StartCase),
                        _ => None,
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Oneof)]
            pub enum Fill {
                #[prost(message, tag = "3")]
                TextSolidFill(super::super::super::Color),
                #[prost(message, tag = "14")]
                TextGradientFill(super::GradientFill),
                #[prost(message, tag = "16")]
                CutOutFill(super::CutOutFill),
                #[prost(message, tag = "17")]
                MediaFill(super::MediaFill),
                #[prost(message, tag = "18")]
                BackgroundEffect(super::super::BackgroundEffect),
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum VerticalAlignment {
            Top = 0,
            Middle = 1,
            Bottom = 2,
        }
        impl VerticalAlignment {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    VerticalAlignment::Top => "VERTICAL_ALIGNMENT_TOP",
                    VerticalAlignment::Middle => "VERTICAL_ALIGNMENT_MIDDLE",
                    VerticalAlignment::Bottom => "VERTICAL_ALIGNMENT_BOTTOM",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "VERTICAL_ALIGNMENT_TOP" => Some(Self::Top),
                    "VERTICAL_ALIGNMENT_MIDDLE" => Some(Self::Middle),
                    "VERTICAL_ALIGNMENT_BOTTOM" => Some(Self::Bottom),
                    _ => None,
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum ScaleBehavior {
            None = 0,
            AdjustContainerHeight = 1,
            ScaleFontDown = 2,
            ScaleFontUp = 3,
            ScaleFontUpDown = 4,
        }
        impl ScaleBehavior {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    ScaleBehavior::None => "SCALE_BEHAVIOR_NONE",
                    ScaleBehavior::AdjustContainerHeight => {
                        "SCALE_BEHAVIOR_ADJUST_CONTAINER_HEIGHT"
                    }
                    ScaleBehavior::ScaleFontDown => "SCALE_BEHAVIOR_SCALE_FONT_DOWN",
                    ScaleBehavior::ScaleFontUp => "SCALE_BEHAVIOR_SCALE_FONT_UP",
                    ScaleBehavior::ScaleFontUpDown => "SCALE_BEHAVIOR_SCALE_FONT_UP_DOWN",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SCALE_BEHAVIOR_NONE" => Some(Self::None),
                    "SCALE_BEHAVIOR_ADJUST_CONTAINER_HEIGHT" => {
                        Some(Self::AdjustContainerHeight)
                    }
                    "SCALE_BEHAVIOR_SCALE_FONT_DOWN" => Some(Self::ScaleFontDown),
                    "SCALE_BEHAVIOR_SCALE_FONT_UP" => Some(Self::ScaleFontUp),
                    "SCALE_BEHAVIOR_SCALE_FONT_UP_DOWN" => Some(Self::ScaleFontUpDown),
                    _ => None,
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Transform {
            None = 0,
            SingleLine = 1,
            OneWordPerLine = 2,
            OneCharacterPerLine = 3,
            ReplaceLineReturns = 4,
        }
        impl Transform {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Transform::None => "TRANSFORM_NONE",
                    Transform::SingleLine => "TRANSFORM_SINGLE_LINE",
                    Transform::OneWordPerLine => "TRANSFORM_ONE_WORD_PER_LINE",
                    Transform::OneCharacterPerLine => "TRANSFORM_ONE_CHARACTER_PER_LINE",
                    Transform::ReplaceLineReturns => "TRANSFORM_REPLACE_LINE_RETURNS",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "TRANSFORM_NONE" => Some(Self::None),
                    "TRANSFORM_SINGLE_LINE" => Some(Self::SingleLine),
                    "TRANSFORM_ONE_WORD_PER_LINE" => Some(Self::OneWordPerLine),
                    "TRANSFORM_ONE_CHARACTER_PER_LINE" => Some(Self::OneCharacterPerLine),
                    "TRANSFORM_REPLACE_LINE_RETURNS" => Some(Self::ReplaceLineReturns),
                    _ => None,
                }
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Media {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(message, optional, tag = "2")]
    pub url: ::core::option::Option<Url>,
    #[prost(message, optional, tag = "3")]
    pub metadata: ::core::option::Option<media::Metadata>,
    #[prost(oneof = "media::TypeProperties", tags = "4, 5, 6, 7, 8")]
    pub type_properties: ::core::option::Option<media::TypeProperties>,
}
/// Nested message and enum types in `Media`.
pub mod media {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Metadata {
        #[prost(string, tag = "1")]
        pub manufacture_name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "2")]
        pub manufacture_url: ::core::option::Option<super::Url>,
        #[prost(string, tag = "3")]
        pub information: ::prost::alloc::string::String,
        #[prost(string, tag = "4")]
        pub artist: ::prost::alloc::string::String,
        #[prost(string, tag = "5")]
        pub format: ::prost::alloc::string::String,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct VideoDevice {
        #[prost(enumeration = "video_device::Type", tag = "1")]
        pub r#type: i32,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(string, tag = "3")]
        pub unique_id: ::prost::alloc::string::String,
        #[prost(string, tag = "4")]
        pub model_id: ::prost::alloc::string::String,
        #[prost(uint32, tag = "5")]
        pub format_index: u32,
        #[prost(message, optional, tag = "6")]
        pub audio_routing: ::core::option::Option<super::digital_audio::device::Routing>,
    }
    /// Nested message and enum types in `VideoDevice`.
    pub mod video_device {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Type {
            Generic = 0,
            Directshow = 1,
            Blackmagic = 2,
            Aja = 3,
            Av = 4,
            Syphon = 5,
            Ndi = 6,
            Bluefish = 7,
        }
        impl Type {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Type::Generic => "TYPE_GENERIC",
                    Type::Directshow => "TYPE_DIRECTSHOW",
                    Type::Blackmagic => "TYPE_BLACKMAGIC",
                    Type::Aja => "TYPE_AJA",
                    Type::Av => "TYPE_AV",
                    Type::Syphon => "TYPE_SYPHON",
                    Type::Ndi => "TYPE_NDI",
                    Type::Bluefish => "TYPE_BLUEFISH",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "TYPE_GENERIC" => Some(Self::Generic),
                    "TYPE_DIRECTSHOW" => Some(Self::Directshow),
                    "TYPE_BLACKMAGIC" => Some(Self::Blackmagic),
                    "TYPE_AJA" => Some(Self::Aja),
                    "TYPE_AV" => Some(Self::Av),
                    "TYPE_SYPHON" => Some(Self::Syphon),
                    "TYPE_NDI" => Some(Self::Ndi),
                    "TYPE_BLUEFISH" => Some(Self::Bluefish),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AudioDevice {
        #[prost(string, tag = "1")]
        pub name: ::prost::alloc::string::String,
        #[prost(string, tag = "2")]
        pub unique_id: ::prost::alloc::string::String,
        #[prost(string, tag = "3")]
        pub model_id: ::prost::alloc::string::String,
        #[prost(uint32, tag = "4")]
        pub channel_count: u32,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Audio {}
    /// Nested message and enum types in `Audio`.
    pub mod audio {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Channel {
            #[prost(uint32, tag = "1")]
            pub index: u32,
            #[prost(bool, tag = "2")]
            pub muted: bool,
            #[prost(double, tag = "3")]
            pub volume: f64,
            #[prost(bool, tag = "4")]
            pub compress_limit: bool,
            #[prost(message, repeated, tag = "5")]
            pub outputs: ::prost::alloc::vec::Vec<channel::Output>,
        }
        /// Nested message and enum types in `Channel`.
        pub mod channel {
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Output {
                #[prost(int32, tag = "2")]
                pub channel_index: i32,
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AudioProperties {
        #[prost(double, tag = "1")]
        pub volume: f64,
        #[prost(message, repeated, tag = "2")]
        pub audio_channels: ::prost::alloc::vec::Vec<audio::Channel>,
        #[prost(bool, tag = "3")]
        pub is_custom_mapping: bool,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct TransportProperties {
        #[prost(double, tag = "1")]
        pub play_rate: f64,
        #[prost(double, tag = "2")]
        pub in_point: f64,
        #[prost(double, tag = "3")]
        pub out_point: f64,
        #[prost(double, tag = "7")]
        pub fade_in_duration: f64,
        #[prost(double, tag = "8")]
        pub fade_out_duration: f64,
        #[prost(bool, tag = "9")]
        pub should_fade_in: bool,
        #[prost(bool, tag = "10")]
        pub should_fade_out: bool,
        #[prost(double, tag = "11")]
        pub end_point: f64,
        #[prost(enumeration = "transport_properties::PlaybackBehavior", tag = "12")]
        pub playback_behavior: i32,
        #[prost(double, tag = "13")]
        pub loop_time: f64,
        #[prost(uint32, tag = "14")]
        pub times_to_loop: u32,
    }
    /// Nested message and enum types in `TransportProperties`.
    pub mod transport_properties {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum PlaybackBehavior {
            Stop = 0,
            Loop = 1,
            LoopForCount = 2,
            LoopForTime = 3,
        }
        impl PlaybackBehavior {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    PlaybackBehavior::Stop => "PLAYBACK_BEHAVIOR_STOP",
                    PlaybackBehavior::Loop => "PLAYBACK_BEHAVIOR_LOOP",
                    PlaybackBehavior::LoopForCount => "PLAYBACK_BEHAVIOR_LOOP_FOR_COUNT",
                    PlaybackBehavior::LoopForTime => "PLAYBACK_BEHAVIOR_LOOP_FOR_TIME",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "PLAYBACK_BEHAVIOR_STOP" => Some(Self::Stop),
                    "PLAYBACK_BEHAVIOR_LOOP" => Some(Self::Loop),
                    "PLAYBACK_BEHAVIOR_LOOP_FOR_COUNT" => Some(Self::LoopForCount),
                    "PLAYBACK_BEHAVIOR_LOOP_FOR_TIME" => Some(Self::LoopForTime),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct DrawingProperties {
        #[prost(enumeration = "drawing_properties::ScaleBehavior", tag = "1")]
        pub scale_behavior: i32,
        #[prost(enumeration = "drawing_properties::ScaleAlignment", tag = "2")]
        pub scale_alignment: i32,
        #[prost(bool, tag = "3")]
        pub flipped_horizontally: bool,
        #[prost(bool, tag = "4")]
        pub flipped_vertically: bool,
        #[prost(message, optional, tag = "5")]
        pub natural_size: ::core::option::Option<super::graphics::Size>,
        #[prost(double, tag = "6")]
        pub custom_image_rotation: f64,
        #[prost(message, optional, tag = "7")]
        pub custom_image_bounds: ::core::option::Option<super::graphics::Rect>,
        #[prost(bool, tag = "8")]
        pub custom_image_aspect_locked: bool,
        #[prost(bool, tag = "9")]
        pub alpha_inverted: bool,
        #[prost(enumeration = "drawing_properties::NativeRotationType", tag = "10")]
        pub native_rotation: i32,
        #[prost(message, optional, tag = "11")]
        pub selected_effect_preset_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(message, repeated, tag = "12")]
        pub effects: ::prost::alloc::vec::Vec<super::Effect>,
        #[prost(bool, tag = "13")]
        pub crop_enable: bool,
        #[prost(message, optional, tag = "14")]
        pub crop_insets: ::core::option::Option<super::graphics::EdgeInsets>,
    }
    /// Nested message and enum types in `DrawingProperties`.
    pub mod drawing_properties {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum ScaleBehavior {
            Fit = 0,
            Fill = 1,
            Stretch = 2,
            Custom = 3,
        }
        impl ScaleBehavior {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    ScaleBehavior::Fit => "SCALE_BEHAVIOR_FIT",
                    ScaleBehavior::Fill => "SCALE_BEHAVIOR_FILL",
                    ScaleBehavior::Stretch => "SCALE_BEHAVIOR_STRETCH",
                    ScaleBehavior::Custom => "SCALE_BEHAVIOR_CUSTOM",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SCALE_BEHAVIOR_FIT" => Some(Self::Fit),
                    "SCALE_BEHAVIOR_FILL" => Some(Self::Fill),
                    "SCALE_BEHAVIOR_STRETCH" => Some(Self::Stretch),
                    "SCALE_BEHAVIOR_CUSTOM" => Some(Self::Custom),
                    _ => None,
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum ScaleAlignment {
            MiddleCenter = 0,
            TopLeft = 1,
            TopCenter = 2,
            TopRight = 3,
            MiddleRight = 4,
            BottomRight = 5,
            BottomCenter = 6,
            BottomLeft = 7,
            MiddleLeft = 8,
        }
        impl ScaleAlignment {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    ScaleAlignment::MiddleCenter => "SCALE_ALIGNMENT_MIDDLE_CENTER",
                    ScaleAlignment::TopLeft => "SCALE_ALIGNMENT_TOP_LEFT",
                    ScaleAlignment::TopCenter => "SCALE_ALIGNMENT_TOP_CENTER",
                    ScaleAlignment::TopRight => "SCALE_ALIGNMENT_TOP_RIGHT",
                    ScaleAlignment::MiddleRight => "SCALE_ALIGNMENT_MIDDLE_RIGHT",
                    ScaleAlignment::BottomRight => "SCALE_ALIGNMENT_BOTTOM_RIGHT",
                    ScaleAlignment::BottomCenter => "SCALE_ALIGNMENT_BOTTOM_CENTER",
                    ScaleAlignment::BottomLeft => "SCALE_ALIGNMENT_BOTTOM_LEFT",
                    ScaleAlignment::MiddleLeft => "SCALE_ALIGNMENT_MIDDLE_LEFT",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SCALE_ALIGNMENT_MIDDLE_CENTER" => Some(Self::MiddleCenter),
                    "SCALE_ALIGNMENT_TOP_LEFT" => Some(Self::TopLeft),
                    "SCALE_ALIGNMENT_TOP_CENTER" => Some(Self::TopCenter),
                    "SCALE_ALIGNMENT_TOP_RIGHT" => Some(Self::TopRight),
                    "SCALE_ALIGNMENT_MIDDLE_RIGHT" => Some(Self::MiddleRight),
                    "SCALE_ALIGNMENT_BOTTOM_RIGHT" => Some(Self::BottomRight),
                    "SCALE_ALIGNMENT_BOTTOM_CENTER" => Some(Self::BottomCenter),
                    "SCALE_ALIGNMENT_BOTTOM_LEFT" => Some(Self::BottomLeft),
                    "SCALE_ALIGNMENT_MIDDLE_LEFT" => Some(Self::MiddleLeft),
                    _ => None,
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum NativeRotationType {
            RotateStandard = 0,
            Rotate90 = 90,
            Rotate180 = 180,
            Rotate270 = 270,
        }
        impl NativeRotationType {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    NativeRotationType::RotateStandard => {
                        "NATIVE_ROTATION_TYPE_ROTATE_STANDARD"
                    }
                    NativeRotationType::Rotate90 => "NATIVE_ROTATION_TYPE_ROTATE_90",
                    NativeRotationType::Rotate180 => "NATIVE_ROTATION_TYPE_ROTATE_180",
                    NativeRotationType::Rotate270 => "NATIVE_ROTATION_TYPE_ROTATE_270",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "NATIVE_ROTATION_TYPE_ROTATE_STANDARD" => Some(Self::RotateStandard),
                    "NATIVE_ROTATION_TYPE_ROTATE_90" => Some(Self::Rotate90),
                    "NATIVE_ROTATION_TYPE_ROTATE_180" => Some(Self::Rotate180),
                    "NATIVE_ROTATION_TYPE_ROTATE_270" => Some(Self::Rotate270),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct VideoProperties {
        #[prost(double, tag = "1")]
        pub frame_rate: f64,
        #[prost(enumeration = "video_properties::FieldType", tag = "2")]
        pub field_type: i32,
        #[prost(double, tag = "3")]
        pub thumbnail_position: f64,
        #[prost(enumeration = "video_properties::EndBehavior", tag = "4")]
        pub end_behavior: i32,
        #[prost(bool, tag = "5")]
        pub soft_loop: bool,
        #[prost(double, tag = "6")]
        pub soft_loop_duration: f64,
    }
    /// Nested message and enum types in `VideoProperties`.
    pub mod video_properties {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum FieldType {
            Unknown = 0,
            Progressive = 1,
            InterlacedUpperFirst = 2,
            InterlacedLowerFirst = 3,
        }
        impl FieldType {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    FieldType::Unknown => "FIELD_TYPE_UNKNOWN",
                    FieldType::Progressive => "FIELD_TYPE_PROGRESSIVE",
                    FieldType::InterlacedUpperFirst => {
                        "FIELD_TYPE_INTERLACED_UPPER_FIRST"
                    }
                    FieldType::InterlacedLowerFirst => {
                        "FIELD_TYPE_INTERLACED_LOWER_FIRST"
                    }
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "FIELD_TYPE_UNKNOWN" => Some(Self::Unknown),
                    "FIELD_TYPE_PROGRESSIVE" => Some(Self::Progressive),
                    "FIELD_TYPE_INTERLACED_UPPER_FIRST" => {
                        Some(Self::InterlacedUpperFirst)
                    }
                    "FIELD_TYPE_INTERLACED_LOWER_FIRST" => {
                        Some(Self::InterlacedLowerFirst)
                    }
                    _ => None,
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum EndBehavior {
            Stop = 0,
            StopOnBlack = 1,
            StopOnClear = 2,
            FadeToBlack = 3,
            FadeToClear = 4,
        }
        impl EndBehavior {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    EndBehavior::Stop => "END_BEHAVIOR_STOP",
                    EndBehavior::StopOnBlack => "END_BEHAVIOR_STOP_ON_BLACK",
                    EndBehavior::StopOnClear => "END_BEHAVIOR_STOP_ON_CLEAR",
                    EndBehavior::FadeToBlack => "END_BEHAVIOR_FADE_TO_BLACK",
                    EndBehavior::FadeToClear => "END_BEHAVIOR_FADE_TO_CLEAR",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "END_BEHAVIOR_STOP" => Some(Self::Stop),
                    "END_BEHAVIOR_STOP_ON_BLACK" => Some(Self::StopOnBlack),
                    "END_BEHAVIOR_STOP_ON_CLEAR" => Some(Self::StopOnClear),
                    "END_BEHAVIOR_FADE_TO_BLACK" => Some(Self::FadeToBlack),
                    "END_BEHAVIOR_FADE_TO_CLEAR" => Some(Self::FadeToClear),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct LiveVideoProperties {
        #[prost(message, optional, tag = "1")]
        pub video_device: ::core::option::Option<VideoDevice>,
        #[prost(message, optional, tag = "2")]
        pub audio_device: ::core::option::Option<AudioDevice>,
        #[prost(int32, tag = "3")]
        pub live_video_index: i32,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AudioTypeProperties {
        #[prost(message, optional, tag = "1")]
        pub audio: ::core::option::Option<AudioProperties>,
        #[prost(message, optional, tag = "2")]
        pub transport: ::core::option::Option<TransportProperties>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ImageTypeProperties {
        #[prost(message, optional, tag = "1")]
        pub drawing: ::core::option::Option<DrawingProperties>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct VideoTypeProperties {
        #[prost(message, optional, tag = "1")]
        pub drawing: ::core::option::Option<DrawingProperties>,
        #[prost(message, optional, tag = "2")]
        pub audio: ::core::option::Option<AudioProperties>,
        #[prost(message, optional, tag = "3")]
        pub transport: ::core::option::Option<TransportProperties>,
        #[prost(message, optional, tag = "4")]
        pub video: ::core::option::Option<VideoProperties>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct LiveVideoTypeProperties {
        #[prost(message, optional, tag = "1")]
        pub drawing: ::core::option::Option<DrawingProperties>,
        #[prost(message, optional, tag = "2")]
        pub audio: ::core::option::Option<AudioProperties>,
        #[prost(message, optional, tag = "3")]
        pub live_video: ::core::option::Option<LiveVideoProperties>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct WebContentTypeProperties {
        #[prost(message, optional, tag = "1")]
        pub drawing: ::core::option::Option<DrawingProperties>,
        #[prost(message, optional, tag = "2")]
        pub url: ::core::option::Option<super::Url>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum TypeProperties {
        #[prost(message, tag = "4")]
        Audio(AudioTypeProperties),
        #[prost(message, tag = "5")]
        Image(ImageTypeProperties),
        #[prost(message, tag = "6")]
        Video(VideoTypeProperties),
        #[prost(message, tag = "7")]
        LiveVideo(LiveVideoTypeProperties),
        #[prost(message, tag = "8")]
        WebContent(WebContentTypeProperties),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Background {
    #[prost(bool, tag = "3")]
    pub is_enabled: bool,
    #[prost(oneof = "background::Fill", tags = "1, 2")]
    pub fill: ::core::option::Option<background::Fill>,
}
/// Nested message and enum types in `Background`.
pub mod background {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Fill {
        #[prost(message, tag = "1")]
        Color(super::Color),
        #[prost(message, tag = "2")]
        Gradient(super::graphics::Gradient),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AlignmentGuide {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(enumeration = "alignment_guide::GuidelineOrientation", tag = "2")]
    pub orientation: i32,
    #[prost(double, tag = "3")]
    pub location: f64,
}
/// Nested message and enum types in `AlignmentGuide`.
pub mod alignment_guide {
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum GuidelineOrientation {
        Horizontal = 0,
        Vertical = 1,
    }
    impl GuidelineOrientation {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                GuidelineOrientation::Horizontal => "GUIDELINE_ORIENTATION_HORIZONTAL",
                GuidelineOrientation::Vertical => "GUIDELINE_ORIENTATION_VERTICAL",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "GUIDELINE_ORIENTATION_HORIZONTAL" => Some(Self::Horizontal),
                "GUIDELINE_ORIENTATION_VERTICAL" => Some(Self::Vertical),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Clock {
    #[prost(string, tag = "1")]
    pub format: ::prost::alloc::string::String,
}
/// Nested message and enum types in `Clock`.
pub mod clock {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Format {
        #[prost(enumeration = "format::DateFormatterStyle", tag = "2")]
        pub date_type: i32,
        #[prost(enumeration = "format::DateFormatterStyle", tag = "3")]
        pub time_format: i32,
        #[prost(bool, tag = "4")]
        pub military_time_enabled: bool,
    }
    /// Nested message and enum types in `Format`.
    pub mod format {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum DateFormatterStyle {
            None = 0,
            Short = 1,
            Medium = 2,
            Long = 3,
            Full = 4,
        }
        impl DateFormatterStyle {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    DateFormatterStyle::None => "DATE_FORMATTER_STYLE_NONE",
                    DateFormatterStyle::Short => "DATE_FORMATTER_STYLE_SHORT",
                    DateFormatterStyle::Medium => "DATE_FORMATTER_STYLE_MEDIUM",
                    DateFormatterStyle::Long => "DATE_FORMATTER_STYLE_LONG",
                    DateFormatterStyle::Full => "DATE_FORMATTER_STYLE_FULL",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "DATE_FORMATTER_STYLE_NONE" => Some(Self::None),
                    "DATE_FORMATTER_STYLE_SHORT" => Some(Self::Short),
                    "DATE_FORMATTER_STYLE_MEDIUM" => Some(Self::Medium),
                    "DATE_FORMATTER_STYLE_LONG" => Some(Self::Long),
                    "DATE_FORMATTER_STYLE_FULL" => Some(Self::Full),
                    _ => None,
                }
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Timer {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub name: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "10")]
    pub configuration: ::core::option::Option<timer::Configuration>,
}
/// Nested message and enum types in `Timer`.
pub mod timer {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Format {
        #[prost(enumeration = "format::Style", tag = "1")]
        pub hour: i32,
        #[prost(enumeration = "format::Style", tag = "2")]
        pub minute: i32,
        #[prost(enumeration = "format::Style", tag = "3")]
        pub second: i32,
        #[prost(enumeration = "format::Style", tag = "4")]
        pub millisecond: i32,
        #[prost(bool, tag = "5")]
        pub is_wall_clock_time: bool,
        #[prost(bool, tag = "6")]
        pub is_24_hour_time: bool,
        #[prost(bool, tag = "7")]
        pub show_milliseconds_under_minute_only: bool,
    }
    /// Nested message and enum types in `Format`.
    pub mod format {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Style {
            StyeNone = 0,
            Short = 1,
            Long = 2,
            RemoveShort = 3,
            RemoveLong = 4,
        }
        impl Style {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Style::StyeNone => "STYE_NONE",
                    Style::Short => "STYLE_SHORT",
                    Style::Long => "STYLE_LONG",
                    Style::RemoveShort => "STYLE_REMOVE_SHORT",
                    Style::RemoveLong => "STYLE_REMOVE_LONG",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "STYE_NONE" => Some(Self::StyeNone),
                    "STYLE_SHORT" => Some(Self::Short),
                    "STYLE_LONG" => Some(Self::Long),
                    "STYLE_REMOVE_SHORT" => Some(Self::RemoveShort),
                    "STYLE_REMOVE_LONG" => Some(Self::RemoveLong),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Configuration {
        #[prost(bool, tag = "4")]
        pub allows_overrun: bool,
        #[prost(oneof = "configuration::TimerType", tags = "1, 2, 3")]
        pub timer_type: ::core::option::Option<configuration::TimerType>,
    }
    /// Nested message and enum types in `Configuration`.
    pub mod configuration {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TimerTypeCountdown {
            #[prost(double, tag = "1")]
            pub duration: f64,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TimerTypeCountdownToTime {
            #[prost(double, tag = "1")]
            pub time_of_day: f64,
            #[prost(enumeration = "timer_type_countdown_to_time::TimePeriod", tag = "2")]
            pub period: i32,
        }
        /// Nested message and enum types in `TimerTypeCountdownToTime`.
        pub mod timer_type_countdown_to_time {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum TimePeriod {
                Am = 0,
                Pm = 1,
                TimePeriod24 = 2,
            }
            impl TimePeriod {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        TimePeriod::Am => "TIME_PERIOD_AM",
                        TimePeriod::Pm => "TIME_PERIOD_PM",
                        TimePeriod::TimePeriod24 => "TIME_PERIOD_24",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "TIME_PERIOD_AM" => Some(Self::Am),
                        "TIME_PERIOD_PM" => Some(Self::Pm),
                        "TIME_PERIOD_24" => Some(Self::TimePeriod24),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TimerTypeElapsedTime {
            #[prost(double, tag = "1")]
            pub start_time: f64,
            #[prost(double, tag = "2")]
            pub end_time: f64,
            #[prost(bool, tag = "3")]
            pub has_end_time: bool,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum TimerType {
            #[prost(message, tag = "1")]
            Countdown(TimerTypeCountdown),
            #[prost(message, tag = "2")]
            CountdownToTime(TimerTypeCountdownToTime),
            #[prost(message, tag = "3")]
            ElapsedTime(TimerTypeElapsedTime),
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct TimersDocument {
    #[prost(message, optional, tag = "1")]
    pub application_info: ::core::option::Option<ApplicationInfo>,
    #[prost(message, optional, tag = "2")]
    pub clock: ::core::option::Option<Clock>,
    #[prost(message, repeated, tag = "3")]
    pub timers: ::prost::alloc::vec::Vec<Timer>,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Slide {
    #[prost(message, repeated, tag = "1")]
    pub elements: ::prost::alloc::vec::Vec<slide::Element>,
    #[prost(message, repeated, tag = "2")]
    pub element_build_order: ::prost::alloc::vec::Vec<crate::protobuf::RVUuid>,
    #[prost(message, repeated, tag = "3")]
    pub guidelines: ::prost::alloc::vec::Vec<AlignmentGuide>,
    #[prost(bool, tag = "4")]
    pub draws_background_color: bool,
    #[prost(message, optional, tag = "5")]
    pub background_color: ::core::option::Option<Color>,
    #[prost(message, optional, tag = "6")]
    pub size: ::core::option::Option<graphics::Size>,
    #[prost(message, optional, tag = "7")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
}
/// Nested message and enum types in `Slide`.
pub mod slide {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Element {
        #[prost(message, optional, tag = "1")]
        pub element: ::core::option::Option<super::graphics::Element>,
        #[prost(message, optional, tag = "2")]
        pub build_in: ::core::option::Option<element::Build>,
        #[prost(message, optional, tag = "3")]
        pub build_out: ::core::option::Option<element::Build>,
        #[prost(uint32, tag = "4")]
        pub info: u32,
        #[prost(enumeration = "element::TextRevealType", tag = "5")]
        pub reveal_type: i32,
        #[prost(message, repeated, tag = "6")]
        pub data_links: ::prost::alloc::vec::Vec<element::DataLink>,
        #[prost(message, repeated, tag = "7")]
        pub child_builds: ::prost::alloc::vec::Vec<element::ChildBuild>,
        #[prost(uint32, tag = "8")]
        pub reveal_from_index: u32,
        #[prost(message, optional, tag = "9")]
        pub text_scroller: ::core::option::Option<element::TextScroller>,
    }
    /// Nested message and enum types in `Element`.
    pub mod element {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Build {
            #[prost(message, optional, tag = "1")]
            pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
            #[prost(message, optional, tag = "2")]
            pub element_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
            #[prost(enumeration = "build::Start", tag = "3")]
            pub start: i32,
            #[prost(double, tag = "4")]
            pub delay_time: f64,
            #[prost(message, optional, tag = "5")]
            pub transition: ::core::option::Option<super::super::Transition>,
        }
        /// Nested message and enum types in `Build`.
        pub mod build {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Start {
                OnClick = 0,
                WithPrevious = 1,
                AfterPrevious = 2,
                WithSlide = 3,
            }
            impl Start {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Start::OnClick => "START_ON_CLICK",
                        Start::WithPrevious => "START_WITH_PREVIOUS",
                        Start::AfterPrevious => "START_AFTER_PREVIOUS",
                        Start::WithSlide => "START_WITH_SLIDE",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "START_ON_CLICK" => Some(Self::OnClick),
                        "START_WITH_PREVIOUS" => Some(Self::WithPrevious),
                        "START_AFTER_PREVIOUS" => Some(Self::AfterPrevious),
                        "START_WITH_SLIDE" => Some(Self::WithSlide),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct ChildBuild {
            #[prost(message, optional, tag = "1")]
            pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
            #[prost(enumeration = "child_build::Start", tag = "2")]
            pub start: i32,
            #[prost(double, tag = "3")]
            pub delay_time: f64,
            #[prost(uint32, tag = "4")]
            pub index: u32,
        }
        /// Nested message and enum types in `ChildBuild`.
        pub mod child_build {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Start {
                OnClick = 0,
                WithPrevious = 1,
                AfterPrevious = 2,
                WithSlide = 3,
            }
            impl Start {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Start::OnClick => "START_ON_CLICK",
                        Start::WithPrevious => "START_WITH_PREVIOUS",
                        Start::AfterPrevious => "START_AFTER_PREVIOUS",
                        Start::WithSlide => "START_WITH_SLIDE",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "START_ON_CLICK" => Some(Self::OnClick),
                        "START_WITH_PREVIOUS" => Some(Self::WithPrevious),
                        "START_AFTER_PREVIOUS" => Some(Self::AfterPrevious),
                        "START_WITH_SLIDE" => Some(Self::WithSlide),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct DataLink {
            #[prost(
                oneof = "data_link::PropertyType",
                tags = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 34"
            )]
            pub property_type: ::core::option::Option<data_link::PropertyType>,
        }
        /// Nested message and enum types in `DataLink`.
        pub mod data_link {
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct RssFeed {
                #[prost(message, optional, tag = "1")]
                pub url: ::core::option::Option<super::super::super::Url>,
                #[prost(enumeration = "rss_feed::ContentType", tag = "2")]
                pub content: i32,
                #[prost(string, tag = "3")]
                pub text_delimiter: ::prost::alloc::string::String,
            }
            /// Nested message and enum types in `RSSFeed`.
            pub mod rss_feed {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum ContentType {
                    TitleOnly = 0,
                    TitleAndDescription = 1,
                }
                impl ContentType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            ContentType::TitleOnly => "CONTENT_TYPE_TITLE_ONLY",
                            ContentType::TitleAndDescription => {
                                "CONTENT_TYPE_TITLE_AND_DESCRIPTION"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "CONTENT_TYPE_TITLE_ONLY" => Some(Self::TitleOnly),
                            "CONTENT_TYPE_TITLE_AND_DESCRIPTION" => {
                                Some(Self::TitleAndDescription)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct FileFeed {
                #[prost(message, optional, tag = "1")]
                pub url: ::core::option::Option<super::super::super::Url>,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Ticker {
                #[prost(double, tag = "4")]
                pub play_rate: f64,
                #[prost(bool, tag = "5")]
                pub should_loop: bool,
                #[prost(double, tag = "6")]
                pub loop_delay: f64,
                #[prost(string, tag = "7")]
                pub text_delimiter: ::prost::alloc::string::String,
                #[prost(oneof = "ticker::SourceType", tags = "1, 2, 3")]
                pub source_type: ::core::option::Option<ticker::SourceType>,
            }
            /// Nested message and enum types in `Ticker`.
            pub mod ticker {
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct TextType {
                    #[prost(string, tag = "1")]
                    pub text: ::prost::alloc::string::String,
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct RssType {
                    #[prost(message, optional, tag = "1")]
                    pub url: ::core::option::Option<super::super::super::super::Url>,
                    #[prost(enumeration = "rss_type::ContentType", tag = "2")]
                    pub content: i32,
                }
                /// Nested message and enum types in `RSSType`.
                pub mod rss_type {
                    #[derive(
                        Clone,
                        Copy,
                        Debug,
                        PartialEq,
                        Eq,
                        Hash,
                        PartialOrd,
                        Ord,
                        ::prost::Enumeration
                    )]
                    #[repr(i32)]
                    pub enum ContentType {
                        TitleOnly = 0,
                        TitleAndDescription = 1,
                    }
                    impl ContentType {
                        /// String value of the enum field names used in the ProtoBuf definition.
                        ///
                        /// The values are not transformed in any way and thus are considered stable
                        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                        pub fn as_str_name(&self) -> &'static str {
                            match self {
                                ContentType::TitleOnly => "CONTENT_TYPE_TITLE_ONLY",
                                ContentType::TitleAndDescription => {
                                    "CONTENT_TYPE_TITLE_AND_DESCRIPTION"
                                }
                            }
                        }
                        /// Creates an enum from field names used in the ProtoBuf definition.
                        pub fn from_str_name(
                            value: &str,
                        ) -> ::core::option::Option<Self> {
                            match value {
                                "CONTENT_TYPE_TITLE_ONLY" => Some(Self::TitleOnly),
                                "CONTENT_TYPE_TITLE_AND_DESCRIPTION" => {
                                    Some(Self::TitleAndDescription)
                                }
                                _ => None,
                            }
                        }
                    }
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct FileType {
                    #[prost(message, optional, tag = "1")]
                    pub url: ::core::option::Option<super::super::super::super::Url>,
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Oneof)]
                pub enum SourceType {
                    #[prost(message, tag = "1")]
                    TextType(TextType),
                    #[prost(message, tag = "2")]
                    RssType(RssType),
                    #[prost(message, tag = "3")]
                    FileType(FileType),
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct AlternateElementText {
                #[prost(message, optional, tag = "1")]
                pub other_element_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
                #[prost(string, tag = "2")]
                pub other_element_name: ::prost::alloc::string::String,
                #[prost(uint32, tag = "3")]
                pub text_transform_options: u32,
                #[prost(
                    enumeration = "alternate_element_text::TextTransformOption",
                    tag = "4"
                )]
                pub text_transform: i32,
            }
            /// Nested message and enum types in `AlternateElementText`.
            pub mod alternate_element_text {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum TextTransformOption {
                    None = 0,
                    RemoveLineReturns = 1,
                    OneWordPerLine = 2,
                    OneCharacterPerLine = 3,
                }
                impl TextTransformOption {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            TextTransformOption::None => "TEXT_TRANSFORM_OPTION_NONE",
                            TextTransformOption::RemoveLineReturns => {
                                "TEXT_TRANSFORM_OPTION_REMOVE_LINE_RETURNS"
                            }
                            TextTransformOption::OneWordPerLine => {
                                "TEXT_TRANSFORM_OPTION_ONE_WORD_PER_LINE"
                            }
                            TextTransformOption::OneCharacterPerLine => {
                                "TEXT_TRANSFORM_OPTION_ONE_CHARACTER_PER_LINE"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "TEXT_TRANSFORM_OPTION_NONE" => Some(Self::None),
                            "TEXT_TRANSFORM_OPTION_REMOVE_LINE_RETURNS" => {
                                Some(Self::RemoveLineReturns)
                            }
                            "TEXT_TRANSFORM_OPTION_ONE_WORD_PER_LINE" => {
                                Some(Self::OneWordPerLine)
                            }
                            "TEXT_TRANSFORM_OPTION_ONE_CHARACTER_PER_LINE" => {
                                Some(Self::OneCharacterPerLine)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct CcliText {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct ColorTrigger {
                #[prost(double, tag = "1")]
                pub time: f64,
                #[prost(message, optional, tag = "2")]
                pub color: ::core::option::Option<super::super::super::Color>,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct TimerText {
                #[prost(message, optional, tag = "1")]
                pub timer_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
                #[prost(string, tag = "2")]
                pub timer_name: ::prost::alloc::string::String,
                #[prost(message, optional, tag = "3")]
                pub timer_format: ::core::option::Option<
                    super::super::super::timer::Format,
                >,
                #[prost(string, tag = "4")]
                pub timer_format_string: ::prost::alloc::string::String,
                #[prost(message, repeated, tag = "5")]
                pub color_triggers: ::prost::alloc::vec::Vec<ColorTrigger>,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct ClockText {
                #[prost(string, tag = "1")]
                pub clock_format_string: ::prost::alloc::string::String,
                #[prost(message, optional, tag = "5")]
                pub format: ::core::option::Option<super::super::super::clock::Format>,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct ChordChart {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct OutputScreen {
                #[prost(message, optional, tag = "1")]
                pub screen_id: ::core::option::Option<crate::protobuf::RVUuid>,
                #[prost(string, tag = "2")]
                pub screen_name: ::prost::alloc::string::String,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct PcoLive {
                #[prost(enumeration = "pco_live::Theme", tag = "2")]
                pub theme: i32,
                #[prost(enumeration = "pco_live::CountdownType", tag = "3")]
                pub countdown_type: i32,
            }
            /// Nested message and enum types in `PCOLive`.
            pub mod pco_live {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Theme {
                    PcoliveThemeLight = 0,
                    PcoliveThemeDark = 1,
                }
                impl Theme {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Theme::PcoliveThemeLight => "PCOLIVE_THEME_LIGHT",
                            Theme::PcoliveThemeDark => "PCOLIVE_THEME_DARK",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "PCOLIVE_THEME_LIGHT" => Some(Self::PcoliveThemeLight),
                            "PCOLIVE_THEME_DARK" => Some(Self::PcoliveThemeDark),
                            _ => None,
                        }
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum CountdownType {
                    FullItemLength = 0,
                    EndItemOnTime = 1,
                    EndServiceOnTime = 2,
                }
                impl CountdownType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            CountdownType::FullItemLength => {
                                "COUNTDOWN_TYPE_FULL_ITEM_LENGTH"
                            }
                            CountdownType::EndItemOnTime => {
                                "COUNTDOWN_TYPE_END_ITEM_ON_TIME"
                            }
                            CountdownType::EndServiceOnTime => {
                                "COUNTDOWN_TYPE_END_SERVICE_ON_TIME"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "COUNTDOWN_TYPE_FULL_ITEM_LENGTH" => {
                                Some(Self::FullItemLength)
                            }
                            "COUNTDOWN_TYPE_END_ITEM_ON_TIME" => {
                                Some(Self::EndItemOnTime)
                            }
                            "COUNTDOWN_TYPE_END_SERVICE_ON_TIME" => {
                                Some(Self::EndServiceOnTime)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct AlternateElementFill {
                #[prost(message, optional, tag = "1")]
                pub other_element_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
                #[prost(string, tag = "2")]
                pub other_element_name: ::prost::alloc::string::String,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct VisibilityLink {
                #[prost(enumeration = "visibility_link::VisibilityCriterion", tag = "1")]
                pub visibility_criterion: i32,
                #[prost(message, repeated, tag = "2")]
                pub conditions: ::prost::alloc::vec::Vec<visibility_link::Condition>,
            }
            /// Nested message and enum types in `VisibilityLink`.
            pub mod visibility_link {
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct Condition {
                    #[prost(
                        oneof = "condition::ConditionType",
                        tags = "1, 2, 3, 4, 5, 6"
                    )]
                    pub condition_type: ::core::option::Option<condition::ConditionType>,
                }
                /// Nested message and enum types in `Condition`.
                pub mod condition {
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Message)]
                    pub struct ElementVisibility {
                        #[prost(message, optional, tag = "1")]
                        pub other_element_uuid: ::core::option::Option<
                            crate::protobuf::RVUuid,
                        >,
                        #[prost(string, tag = "2")]
                        pub other_element_name: ::prost::alloc::string::String,
                        #[prost(
                            enumeration = "element_visibility::ElementVisibilityCriterion",
                            tag = "3"
                        )]
                        pub visibility_criterion: i32,
                    }
                    /// Nested message and enum types in `ElementVisibility`.
                    pub mod element_visibility {
                        #[derive(
                            Clone,
                            Copy,
                            Debug,
                            PartialEq,
                            Eq,
                            Hash,
                            PartialOrd,
                            Ord,
                            ::prost::Enumeration
                        )]
                        #[repr(i32)]
                        pub enum ElementVisibilityCriterion {
                            HasText = 0,
                            HasNoText = 1,
                        }
                        impl ElementVisibilityCriterion {
                            /// String value of the enum field names used in the ProtoBuf definition.
                            ///
                            /// The values are not transformed in any way and thus are considered stable
                            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                            pub fn as_str_name(&self) -> &'static str {
                                match self {
                                    ElementVisibilityCriterion::HasText => {
                                        "ELEMENT_VISIBILITY_CRITERION_HAS_TEXT"
                                    }
                                    ElementVisibilityCriterion::HasNoText => {
                                        "ELEMENT_VISIBILITY_CRITERION_HAS_NO_TEXT"
                                    }
                                }
                            }
                            /// Creates an enum from field names used in the ProtoBuf definition.
                            pub fn from_str_name(
                                value: &str,
                            ) -> ::core::option::Option<Self> {
                                match value {
                                    "ELEMENT_VISIBILITY_CRITERION_HAS_TEXT" => {
                                        Some(Self::HasText)
                                    }
                                    "ELEMENT_VISIBILITY_CRITERION_HAS_NO_TEXT" => {
                                        Some(Self::HasNoText)
                                    }
                                    _ => None,
                                }
                            }
                        }
                    }
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Message)]
                    pub struct TimerVisibility {
                        #[prost(message, optional, tag = "1")]
                        pub timer_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
                        #[prost(string, tag = "2")]
                        pub timer_name: ::prost::alloc::string::String,
                        #[prost(
                            enumeration = "timer_visibility::TimerVisibilityCriterion",
                            tag = "3"
                        )]
                        pub visibility_criterion: i32,
                    }
                    /// Nested message and enum types in `TimerVisibility`.
                    pub mod timer_visibility {
                        #[derive(
                            Clone,
                            Copy,
                            Debug,
                            PartialEq,
                            Eq,
                            Hash,
                            PartialOrd,
                            Ord,
                            ::prost::Enumeration
                        )]
                        #[repr(i32)]
                        pub enum TimerVisibilityCriterion {
                            HasTimeRemaining = 0,
                            HasExpired = 1,
                            IsRunning = 2,
                            NotRunning = 3,
                        }
                        impl TimerVisibilityCriterion {
                            /// String value of the enum field names used in the ProtoBuf definition.
                            ///
                            /// The values are not transformed in any way and thus are considered stable
                            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                            pub fn as_str_name(&self) -> &'static str {
                                match self {
                                    TimerVisibilityCriterion::HasTimeRemaining => {
                                        "TIMER_VISIBILITY_CRITERION_HAS_TIME_REMAINING"
                                    }
                                    TimerVisibilityCriterion::HasExpired => {
                                        "TIMER_VISIBILITY_CRITERION_HAS_EXPIRED"
                                    }
                                    TimerVisibilityCriterion::IsRunning => {
                                        "TIMER_VISIBILITY_CRITERION_IS_RUNNING"
                                    }
                                    TimerVisibilityCriterion::NotRunning => {
                                        "TIMER_VISIBILITY_CRITERION_NOT_RUNNING"
                                    }
                                }
                            }
                            /// Creates an enum from field names used in the ProtoBuf definition.
                            pub fn from_str_name(
                                value: &str,
                            ) -> ::core::option::Option<Self> {
                                match value {
                                    "TIMER_VISIBILITY_CRITERION_HAS_TIME_REMAINING" => {
                                        Some(Self::HasTimeRemaining)
                                    }
                                    "TIMER_VISIBILITY_CRITERION_HAS_EXPIRED" => {
                                        Some(Self::HasExpired)
                                    }
                                    "TIMER_VISIBILITY_CRITERION_IS_RUNNING" => {
                                        Some(Self::IsRunning)
                                    }
                                    "TIMER_VISIBILITY_CRITERION_NOT_RUNNING" => {
                                        Some(Self::NotRunning)
                                    }
                                    _ => None,
                                }
                            }
                        }
                    }
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Message)]
                    pub struct VideoCountdownVisibility {
                        #[prost(
                            enumeration = "video_countdown_visibility::VideoCountdownVisibilityCriterion",
                            tag = "1"
                        )]
                        pub visibility_criterion: i32,
                    }
                    /// Nested message and enum types in `VideoCountdownVisibility`.
                    pub mod video_countdown_visibility {
                        #[derive(
                            Clone,
                            Copy,
                            Debug,
                            PartialEq,
                            Eq,
                            Hash,
                            PartialOrd,
                            Ord,
                            ::prost::Enumeration
                        )]
                        #[repr(i32)]
                        pub enum VideoCountdownVisibilityCriterion {
                            HasTimeRemaining = 0,
                            HasExpired = 1,
                            IsRunning = 2,
                            NotRunning = 3,
                            Looping = 4,
                            NotLooping = 5,
                        }
                        impl VideoCountdownVisibilityCriterion {
                            /// String value of the enum field names used in the ProtoBuf definition.
                            ///
                            /// The values are not transformed in any way and thus are considered stable
                            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                            pub fn as_str_name(&self) -> &'static str {
                                match self {
                                    VideoCountdownVisibilityCriterion::HasTimeRemaining => {
                                        "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_HAS_TIME_REMAINING"
                                    }
                                    VideoCountdownVisibilityCriterion::HasExpired => {
                                        "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_HAS_EXPIRED"
                                    }
                                    VideoCountdownVisibilityCriterion::IsRunning => {
                                        "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_IS_RUNNING"
                                    }
                                    VideoCountdownVisibilityCriterion::NotRunning => {
                                        "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_NOT_RUNNING"
                                    }
                                    VideoCountdownVisibilityCriterion::Looping => {
                                        "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_LOOPING"
                                    }
                                    VideoCountdownVisibilityCriterion::NotLooping => {
                                        "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_NOT_LOOPING"
                                    }
                                }
                            }
                            /// Creates an enum from field names used in the ProtoBuf definition.
                            pub fn from_str_name(
                                value: &str,
                            ) -> ::core::option::Option<Self> {
                                match value {
                                    "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_HAS_TIME_REMAINING" => {
                                        Some(Self::HasTimeRemaining)
                                    }
                                    "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_HAS_EXPIRED" => {
                                        Some(Self::HasExpired)
                                    }
                                    "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_IS_RUNNING" => {
                                        Some(Self::IsRunning)
                                    }
                                    "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_NOT_RUNNING" => {
                                        Some(Self::NotRunning)
                                    }
                                    "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_LOOPING" => {
                                        Some(Self::Looping)
                                    }
                                    "VIDEO_COUNTDOWN_VISIBILITY_CRITERION_NOT_LOOPING" => {
                                        Some(Self::NotLooping)
                                    }
                                    _ => None,
                                }
                            }
                        }
                    }
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Message)]
                    pub struct AudioCountdownVisibility {
                        #[prost(
                            enumeration = "audio_countdown_visibility::AudioCountdownVisibilityCriterion",
                            tag = "1"
                        )]
                        pub visibility_criterion: i32,
                    }
                    /// Nested message and enum types in `AudioCountdownVisibility`.
                    pub mod audio_countdown_visibility {
                        #[derive(
                            Clone,
                            Copy,
                            Debug,
                            PartialEq,
                            Eq,
                            Hash,
                            PartialOrd,
                            Ord,
                            ::prost::Enumeration
                        )]
                        #[repr(i32)]
                        pub enum AudioCountdownVisibilityCriterion {
                            HasTimeRemaining = 0,
                            HasExpired = 1,
                            IsRunning = 2,
                            NotRunning = 3,
                            Looping = 4,
                            NotLooping = 5,
                        }
                        impl AudioCountdownVisibilityCriterion {
                            /// String value of the enum field names used in the ProtoBuf definition.
                            ///
                            /// The values are not transformed in any way and thus are considered stable
                            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                            pub fn as_str_name(&self) -> &'static str {
                                match self {
                                    AudioCountdownVisibilityCriterion::HasTimeRemaining => {
                                        "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_HAS_TIME_REMAINING"
                                    }
                                    AudioCountdownVisibilityCriterion::HasExpired => {
                                        "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_HAS_EXPIRED"
                                    }
                                    AudioCountdownVisibilityCriterion::IsRunning => {
                                        "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_IS_RUNNING"
                                    }
                                    AudioCountdownVisibilityCriterion::NotRunning => {
                                        "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_NOT_RUNNING"
                                    }
                                    AudioCountdownVisibilityCriterion::Looping => {
                                        "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_LOOPING"
                                    }
                                    AudioCountdownVisibilityCriterion::NotLooping => {
                                        "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_NOT_LOOPING"
                                    }
                                }
                            }
                            /// Creates an enum from field names used in the ProtoBuf definition.
                            pub fn from_str_name(
                                value: &str,
                            ) -> ::core::option::Option<Self> {
                                match value {
                                    "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_HAS_TIME_REMAINING" => {
                                        Some(Self::HasTimeRemaining)
                                    }
                                    "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_HAS_EXPIRED" => {
                                        Some(Self::HasExpired)
                                    }
                                    "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_IS_RUNNING" => {
                                        Some(Self::IsRunning)
                                    }
                                    "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_NOT_RUNNING" => {
                                        Some(Self::NotRunning)
                                    }
                                    "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_LOOPING" => {
                                        Some(Self::Looping)
                                    }
                                    "AUDIO_COUNTDOWN_VISIBILITY_CRITERION_NOT_LOOPING" => {
                                        Some(Self::NotLooping)
                                    }
                                    _ => None,
                                }
                            }
                        }
                    }
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Message)]
                    pub struct CaptureSessionVisibility {
                        #[prost(
                            enumeration = "capture_session_visibility::CaptureSessionVisibilityCriterion",
                            tag = "1"
                        )]
                        pub visibility_criterion: i32,
                    }
                    /// Nested message and enum types in `CaptureSessionVisibility`.
                    pub mod capture_session_visibility {
                        #[derive(
                            Clone,
                            Copy,
                            Debug,
                            PartialEq,
                            Eq,
                            Hash,
                            PartialOrd,
                            Ord,
                            ::prost::Enumeration
                        )]
                        #[repr(i32)]
                        pub enum CaptureSessionVisibilityCriterion {
                            Active = 0,
                            Inactive = 1,
                        }
                        impl CaptureSessionVisibilityCriterion {
                            /// String value of the enum field names used in the ProtoBuf definition.
                            ///
                            /// The values are not transformed in any way and thus are considered stable
                            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                            pub fn as_str_name(&self) -> &'static str {
                                match self {
                                    CaptureSessionVisibilityCriterion::Active => {
                                        "CAPTURE_SESSION_VISIBILITY_CRITERION_ACTIVE"
                                    }
                                    CaptureSessionVisibilityCriterion::Inactive => {
                                        "CAPTURE_SESSION_VISIBILITY_CRITERION_INACTIVE"
                                    }
                                }
                            }
                            /// Creates an enum from field names used in the ProtoBuf definition.
                            pub fn from_str_name(
                                value: &str,
                            ) -> ::core::option::Option<Self> {
                                match value {
                                    "CAPTURE_SESSION_VISIBILITY_CRITERION_ACTIVE" => {
                                        Some(Self::Active)
                                    }
                                    "CAPTURE_SESSION_VISIBILITY_CRITERION_INACTIVE" => {
                                        Some(Self::Inactive)
                                    }
                                    _ => None,
                                }
                            }
                        }
                    }
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Message)]
                    pub struct VideoInputVisibility {
                        #[prost(int32, tag = "1")]
                        pub video_input_index: i32,
                        #[prost(
                            enumeration = "video_input_visibility::VideoInputVisibilityCriterion",
                            tag = "2"
                        )]
                        pub visibility_criterion: i32,
                    }
                    /// Nested message and enum types in `VideoInputVisibility`.
                    pub mod video_input_visibility {
                        #[derive(
                            Clone,
                            Copy,
                            Debug,
                            PartialEq,
                            Eq,
                            Hash,
                            PartialOrd,
                            Ord,
                            ::prost::Enumeration
                        )]
                        #[repr(i32)]
                        pub enum VideoInputVisibilityCriterion {
                            Active = 0,
                            Inactive = 1,
                        }
                        impl VideoInputVisibilityCriterion {
                            /// String value of the enum field names used in the ProtoBuf definition.
                            ///
                            /// The values are not transformed in any way and thus are considered stable
                            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                            pub fn as_str_name(&self) -> &'static str {
                                match self {
                                    VideoInputVisibilityCriterion::Active => {
                                        "VIDEO_INPUT_VISIBILITY_CRITERION_ACTIVE"
                                    }
                                    VideoInputVisibilityCriterion::Inactive => {
                                        "VIDEO_INPUT_VISIBILITY_CRITERION_INACTIVE"
                                    }
                                }
                            }
                            /// Creates an enum from field names used in the ProtoBuf definition.
                            pub fn from_str_name(
                                value: &str,
                            ) -> ::core::option::Option<Self> {
                                match value {
                                    "VIDEO_INPUT_VISIBILITY_CRITERION_ACTIVE" => {
                                        Some(Self::Active)
                                    }
                                    "VIDEO_INPUT_VISIBILITY_CRITERION_INACTIVE" => {
                                        Some(Self::Inactive)
                                    }
                                    _ => None,
                                }
                            }
                        }
                    }
                    #[allow(clippy::derive_partial_eq_without_eq)]
                    #[derive(Clone, PartialEq, ::prost::Oneof)]
                    pub enum ConditionType {
                        #[prost(message, tag = "1")]
                        ElementVisibility(ElementVisibility),
                        #[prost(message, tag = "2")]
                        TimerVisibility(TimerVisibility),
                        #[prost(message, tag = "3")]
                        VideoCountdownVisibility(VideoCountdownVisibility),
                        #[prost(message, tag = "4")]
                        CaptureSessionVisibility(CaptureSessionVisibility),
                        #[prost(message, tag = "5")]
                        VideoInputVisibility(VideoInputVisibility),
                        #[prost(message, tag = "6")]
                        AudioCountdownVisibility(AudioCountdownVisibility),
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum VisibilityCriterion {
                    All = 0,
                    Any = 1,
                    None = 2,
                }
                impl VisibilityCriterion {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            VisibilityCriterion::All => "VISIBILITY_CRITERION_ALL",
                            VisibilityCriterion::Any => "VISIBILITY_CRITERION_ANY",
                            VisibilityCriterion::None => "VISIBILITY_CRITERION_NONE",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "VISIBILITY_CRITERION_ALL" => Some(Self::All),
                            "VISIBILITY_CRITERION_ANY" => Some(Self::Any),
                            "VISIBILITY_CRITERION_NONE" => Some(Self::None),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct SlideText {
                #[prost(enumeration = "slide_text::SlideSourceType", tag = "1")]
                pub source_slide: i32,
                #[prost(enumeration = "slide_text::TextSourceOption", tag = "2")]
                pub source_option: i32,
                #[prost(bool, tag = "3")]
                pub preserve_notes_format: bool,
                #[prost(string, tag = "4")]
                pub name_to_match: ::prost::alloc::string::String,
                #[prost(enumeration = "slide_text::TextTransformOption", tag = "5")]
                pub element_text_transform: i32,
            }
            /// Nested message and enum types in `SlideText`.
            pub mod slide_text {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum SlideSourceType {
                    CurrentSlide = 0,
                    NextSlide = 1,
                }
                impl SlideSourceType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            SlideSourceType::CurrentSlide => {
                                "SLIDE_SOURCE_TYPE_CURRENT_SLIDE"
                            }
                            SlideSourceType::NextSlide => "SLIDE_SOURCE_TYPE_NEXT_SLIDE",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "SLIDE_SOURCE_TYPE_CURRENT_SLIDE" => Some(Self::CurrentSlide),
                            "SLIDE_SOURCE_TYPE_NEXT_SLIDE" => Some(Self::NextSlide),
                            _ => None,
                        }
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum TextSourceOption {
                    Text = 0,
                    Notes = 1,
                    ElementMatchingName = 2,
                }
                impl TextSourceOption {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            TextSourceOption::Text => "TEXT_SOURCE_OPTION_TEXT",
                            TextSourceOption::Notes => "TEXT_SOURCE_OPTION_NOTES",
                            TextSourceOption::ElementMatchingName => {
                                "TEXT_SOURCE_OPTION_ELEMENT_MATCHING_NAME"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "TEXT_SOURCE_OPTION_TEXT" => Some(Self::Text),
                            "TEXT_SOURCE_OPTION_NOTES" => Some(Self::Notes),
                            "TEXT_SOURCE_OPTION_ELEMENT_MATCHING_NAME" => {
                                Some(Self::ElementMatchingName)
                            }
                            _ => None,
                        }
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum TextTransformOption {
                    None = 0,
                    RemoveLineReturns = 1,
                    OneWordPerLine = 2,
                    OneCharacterPerLine = 3,
                }
                impl TextTransformOption {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            TextTransformOption::None => "TEXT_TRANSFORM_OPTION_NONE",
                            TextTransformOption::RemoveLineReturns => {
                                "TEXT_TRANSFORM_OPTION_REMOVE_LINE_RETURNS"
                            }
                            TextTransformOption::OneWordPerLine => {
                                "TEXT_TRANSFORM_OPTION_ONE_WORD_PER_LINE"
                            }
                            TextTransformOption::OneCharacterPerLine => {
                                "TEXT_TRANSFORM_OPTION_ONE_CHARACTER_PER_LINE"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "TEXT_TRANSFORM_OPTION_NONE" => Some(Self::None),
                            "TEXT_TRANSFORM_OPTION_REMOVE_LINE_RETURNS" => {
                                Some(Self::RemoveLineReturns)
                            }
                            "TEXT_TRANSFORM_OPTION_ONE_WORD_PER_LINE" => {
                                Some(Self::OneWordPerLine)
                            }
                            "TEXT_TRANSFORM_OPTION_ONE_CHARACTER_PER_LINE" => {
                                Some(Self::OneCharacterPerLine)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct SlideImage {
                #[prost(enumeration = "slide_image::SlideSourceType", tag = "1")]
                pub source_slide: i32,
            }
            /// Nested message and enum types in `SlideImage`.
            pub mod slide_image {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum SlideSourceType {
                    CurrentSlide = 0,
                    NextSlide = 1,
                }
                impl SlideSourceType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            SlideSourceType::CurrentSlide => {
                                "SLIDE_SOURCE_TYPE_CURRENT_SLIDE"
                            }
                            SlideSourceType::NextSlide => "SLIDE_SOURCE_TYPE_NEXT_SLIDE",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "SLIDE_SOURCE_TYPE_CURRENT_SLIDE" => Some(Self::CurrentSlide),
                            "SLIDE_SOURCE_TYPE_NEXT_SLIDE" => Some(Self::NextSlide),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct StageMessage {
                #[prost(bool, tag = "1")]
                pub should_flash: bool,
                #[prost(message, optional, tag = "2")]
                pub flash_color: ::core::option::Option<super::super::super::Color>,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct VideoCountdown {
                #[prost(message, optional, tag = "1")]
                pub timer_format: ::core::option::Option<
                    super::super::super::timer::Format,
                >,
                #[prost(string, tag = "2")]
                pub timer_format_string: ::prost::alloc::string::String,
                #[prost(message, repeated, tag = "3")]
                pub color_triggers: ::prost::alloc::vec::Vec<ColorTrigger>,
                #[prost(bool, tag = "4")]
                pub ignore_looping_videos: bool,
                #[prost(
                    enumeration = "video_countdown::VideoCountdownSource",
                    tag = "5"
                )]
                pub video_countdown_source: i32,
            }
            /// Nested message and enum types in `VideoCountdown`.
            pub mod video_countdown {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum VideoCountdownSource {
                    Presentation = 0,
                    Announcement = 1,
                }
                impl VideoCountdownSource {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            VideoCountdownSource::Presentation => {
                                "VIDEO_COUNTDOWN_SOURCE_PRESENTATION"
                            }
                            VideoCountdownSource::Announcement => {
                                "VIDEO_COUNTDOWN_SOURCE_ANNOUNCEMENT"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "VIDEO_COUNTDOWN_SOURCE_PRESENTATION" => {
                                Some(Self::Presentation)
                            }
                            "VIDEO_COUNTDOWN_SOURCE_ANNOUNCEMENT" => {
                                Some(Self::Announcement)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct AudioCountdown {
                #[prost(message, optional, tag = "1")]
                pub timer_format: ::core::option::Option<
                    super::super::super::timer::Format,
                >,
                #[prost(string, tag = "2")]
                pub timer_format_string: ::prost::alloc::string::String,
                #[prost(message, repeated, tag = "3")]
                pub color_triggers: ::prost::alloc::vec::Vec<ColorTrigger>,
                #[prost(bool, tag = "4")]
                pub ignore_looping_audio: bool,
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct GroupName {
                #[prost(enumeration = "group_name::GroupSourceType", tag = "1")]
                pub group_source: i32,
            }
            /// Nested message and enum types in `GroupName`.
            pub mod group_name {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum GroupSourceType {
                    CurrentSlide = 0,
                    NextSlide = 1,
                    NextGroup = 2,
                }
                impl GroupSourceType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            GroupSourceType::CurrentSlide => {
                                "GROUP_SOURCE_TYPE_CURRENT_SLIDE"
                            }
                            GroupSourceType::NextSlide => "GROUP_SOURCE_TYPE_NEXT_SLIDE",
                            GroupSourceType::NextGroup => "GROUP_SOURCE_TYPE_NEXT_GROUP",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "GROUP_SOURCE_TYPE_CURRENT_SLIDE" => Some(Self::CurrentSlide),
                            "GROUP_SOURCE_TYPE_NEXT_SLIDE" => Some(Self::NextSlide),
                            "GROUP_SOURCE_TYPE_NEXT_GROUP" => Some(Self::NextGroup),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct GroupColor {
                #[prost(enumeration = "group_color::GroupSourceType", tag = "1")]
                pub group_source: i32,
            }
            /// Nested message and enum types in `GroupColor`.
            pub mod group_color {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum GroupSourceType {
                    CurrentSlide = 0,
                    NextSlide = 1,
                    NextGroup = 2,
                }
                impl GroupSourceType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            GroupSourceType::CurrentSlide => {
                                "GROUP_SOURCE_TYPE_CURRENT_SLIDE"
                            }
                            GroupSourceType::NextSlide => "GROUP_SOURCE_TYPE_NEXT_SLIDE",
                            GroupSourceType::NextGroup => "GROUP_SOURCE_TYPE_NEXT_GROUP",
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "GROUP_SOURCE_TYPE_CURRENT_SLIDE" => Some(Self::CurrentSlide),
                            "GROUP_SOURCE_TYPE_NEXT_SLIDE" => Some(Self::NextSlide),
                            "GROUP_SOURCE_TYPE_NEXT_GROUP" => Some(Self::NextGroup),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct SlideLabelText {
                #[prost(enumeration = "slide_label_text::SlideLabelSource", tag = "1")]
                pub slide_label_source: i32,
            }
            /// Nested message and enum types in `SlideLabelText`.
            pub mod slide_label_text {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum SlideLabelSource {
                    CurrentSlide = 0,
                    NextSlide = 1,
                }
                impl SlideLabelSource {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            SlideLabelSource::CurrentSlide => {
                                "SLIDE_LABEL_SOURCE_CURRENT_SLIDE"
                            }
                            SlideLabelSource::NextSlide => {
                                "SLIDE_LABEL_SOURCE_NEXT_SLIDE"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "SLIDE_LABEL_SOURCE_CURRENT_SLIDE" => {
                                Some(Self::CurrentSlide)
                            }
                            "SLIDE_LABEL_SOURCE_NEXT_SLIDE" => Some(Self::NextSlide),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct SlideLabelColor {
                #[prost(enumeration = "slide_label_color::SlideLabelSource", tag = "1")]
                pub slide_label_source: i32,
            }
            /// Nested message and enum types in `SlideLabelColor`.
            pub mod slide_label_color {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum SlideLabelSource {
                    CurrentSlide = 0,
                    NextSlide = 1,
                }
                impl SlideLabelSource {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            SlideLabelSource::CurrentSlide => {
                                "SLIDE_LABEL_SOURCE_CURRENT_SLIDE"
                            }
                            SlideLabelSource::NextSlide => {
                                "SLIDE_LABEL_SOURCE_NEXT_SLIDE"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "SLIDE_LABEL_SOURCE_CURRENT_SLIDE" => {
                                Some(Self::CurrentSlide)
                            }
                            "SLIDE_LABEL_SOURCE_NEXT_SLIDE" => Some(Self::NextSlide),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct PresentationNotes {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct Presentation {
                #[prost(enumeration = "presentation::PresentationSource", tag = "1")]
                pub presentation_source: i32,
            }
            /// Nested message and enum types in `Presentation`.
            pub mod presentation {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum PresentationSource {
                    Presentation = 0,
                    Announcement = 1,
                }
                impl PresentationSource {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            PresentationSource::Presentation => {
                                "PRESENTATION_SOURCE_PRESENTATION"
                            }
                            PresentationSource::Announcement => {
                                "PRESENTATION_SOURCE_ANNOUNCEMENT"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "PRESENTATION_SOURCE_PRESENTATION" => {
                                Some(Self::Presentation)
                            }
                            "PRESENTATION_SOURCE_ANNOUNCEMENT" => {
                                Some(Self::Announcement)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct PlaylistItem {
                #[prost(
                    enumeration = "playlist_item::PlaylistItemSourceType",
                    tag = "1"
                )]
                pub playlist_item_source: i32,
                #[prost(bool, tag = "2")]
                pub show_arrangement: bool,
            }
            /// Nested message and enum types in `PlaylistItem`.
            pub mod playlist_item {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum PlaylistItemSourceType {
                    Current = 0,
                    Next = 1,
                    CurrentHeader = 2,
                    NextHeader = 3,
                    ParentPlaylist = 4,
                }
                impl PlaylistItemSourceType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            PlaylistItemSourceType::Current => {
                                "PLAYLIST_ITEM_SOURCE_TYPE_CURRENT"
                            }
                            PlaylistItemSourceType::Next => {
                                "PLAYLIST_ITEM_SOURCE_TYPE_NEXT"
                            }
                            PlaylistItemSourceType::CurrentHeader => {
                                "PLAYLIST_ITEM_SOURCE_TYPE_CURRENT_HEADER"
                            }
                            PlaylistItemSourceType::NextHeader => {
                                "PLAYLIST_ITEM_SOURCE_TYPE_NEXT_HEADER"
                            }
                            PlaylistItemSourceType::ParentPlaylist => {
                                "PLAYLIST_ITEM_SOURCE_TYPE_PARENT_PLAYLIST"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "PLAYLIST_ITEM_SOURCE_TYPE_CURRENT" => Some(Self::Current),
                            "PLAYLIST_ITEM_SOURCE_TYPE_NEXT" => Some(Self::Next),
                            "PLAYLIST_ITEM_SOURCE_TYPE_CURRENT_HEADER" => {
                                Some(Self::CurrentHeader)
                            }
                            "PLAYLIST_ITEM_SOURCE_TYPE_NEXT_HEADER" => {
                                Some(Self::NextHeader)
                            }
                            "PLAYLIST_ITEM_SOURCE_TYPE_PARENT_PLAYLIST" => {
                                Some(Self::ParentPlaylist)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct AutoAdvanceTimeRemaining {
                #[prost(
                    enumeration = "auto_advance_time_remaining::AutoAdvanceSource",
                    tag = "1"
                )]
                pub auto_advance_source: i32,
                #[prost(message, optional, tag = "2")]
                pub timer_format: ::core::option::Option<
                    super::super::super::timer::Format,
                >,
            }
            /// Nested message and enum types in `AutoAdvanceTimeRemaining`.
            pub mod auto_advance_time_remaining {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum AutoAdvanceSource {
                    Presentation = 0,
                    Announcement = 1,
                }
                impl AutoAdvanceSource {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            AutoAdvanceSource::Presentation => {
                                "AUTO_ADVANCE_SOURCE_PRESENTATION"
                            }
                            AutoAdvanceSource::Announcement => {
                                "AUTO_ADVANCE_SOURCE_ANNOUNCEMENT"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "AUTO_ADVANCE_SOURCE_PRESENTATION" => {
                                Some(Self::Presentation)
                            }
                            "AUTO_ADVANCE_SOURCE_ANNOUNCEMENT" => {
                                Some(Self::Announcement)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct CaptureStatusText {
                #[prost(oneof = "capture_status_text::TextType", tags = "1, 2")]
                pub text_type: ::core::option::Option<capture_status_text::TextType>,
            }
            /// Nested message and enum types in `CaptureStatusText`.
            pub mod capture_status_text {
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct StatusText {}
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct ElapsedTime {
                    #[prost(message, optional, tag = "1")]
                    pub timer_format: ::core::option::Option<
                        super::super::super::super::timer::Format,
                    >,
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Oneof)]
                pub enum TextType {
                    #[prost(message, tag = "1")]
                    StatusText(StatusText),
                    #[prost(message, tag = "2")]
                    ElapsedTime(ElapsedTime),
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct CaptureStatusColor {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct SlideCount {
                #[prost(enumeration = "slide_count::SlideCountSourceType", tag = "1")]
                pub slide_count_source_type: i32,
            }
            /// Nested message and enum types in `SlideCount`.
            pub mod slide_count {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum SlideCountSourceType {
                    Current = 0,
                    Remaining = 1,
                    Total = 2,
                }
                impl SlideCountSourceType {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            SlideCountSourceType::Current => {
                                "SLIDE_COUNT_SOURCE_TYPE_CURRENT"
                            }
                            SlideCountSourceType::Remaining => {
                                "SLIDE_COUNT_SOURCE_TYPE_REMAINING"
                            }
                            SlideCountSourceType::Total => {
                                "SLIDE_COUNT_SOURCE_TYPE_TOTAL"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "SLIDE_COUNT_SOURCE_TYPE_CURRENT" => Some(Self::Current),
                            "SLIDE_COUNT_SOURCE_TYPE_REMAINING" => Some(Self::Remaining),
                            "SLIDE_COUNT_SOURCE_TYPE_TOTAL" => Some(Self::Total),
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct PlaybackMarkerIdentifier {
                #[prost(
                    enumeration = "playback_marker_identifier::Destination",
                    tag = "1"
                )]
                pub destination: i32,
                #[prost(enumeration = "playback_marker_identifier::Type", tag = "2")]
                pub r#type: i32,
                #[prost(string, tag = "3")]
                pub name: ::prost::alloc::string::String,
            }
            /// Nested message and enum types in `PlaybackMarkerIdentifier`.
            pub mod playback_marker_identifier {
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Destination {
                    PlaybackMarkerDestinationPresentation = 0,
                    PlaybackMarkerDestinationAnnouncement = 1,
                    PlaybackMarkerDestinationAudio = 2,
                }
                impl Destination {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Destination::PlaybackMarkerDestinationPresentation => {
                                "PLAYBACK_MARKER_DESTINATION_PRESENTATION"
                            }
                            Destination::PlaybackMarkerDestinationAnnouncement => {
                                "PLAYBACK_MARKER_DESTINATION_ANNOUNCEMENT"
                            }
                            Destination::PlaybackMarkerDestinationAudio => {
                                "PLAYBACK_MARKER_DESTINATION_AUDIO"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "PLAYBACK_MARKER_DESTINATION_PRESENTATION" => {
                                Some(Self::PlaybackMarkerDestinationPresentation)
                            }
                            "PLAYBACK_MARKER_DESTINATION_ANNOUNCEMENT" => {
                                Some(Self::PlaybackMarkerDestinationAnnouncement)
                            }
                            "PLAYBACK_MARKER_DESTINATION_AUDIO" => {
                                Some(Self::PlaybackMarkerDestinationAudio)
                            }
                            _ => None,
                        }
                    }
                }
                #[derive(
                    Clone,
                    Copy,
                    Debug,
                    PartialEq,
                    Eq,
                    Hash,
                    PartialOrd,
                    Ord,
                    ::prost::Enumeration
                )]
                #[repr(i32)]
                pub enum Type {
                    PlaybackMarkerIdentifierFirst = 0,
                    PlaybackMarkerIdentifierPrevious = 1,
                    PlaybackMarkerIdentifierNext = 2,
                    PlaybackMarkerIdentifierLast = 3,
                    PlaybackMarkerIdentifierName = 4,
                }
                impl Type {
                    /// String value of the enum field names used in the ProtoBuf definition.
                    ///
                    /// The values are not transformed in any way and thus are considered stable
                    /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                    pub fn as_str_name(&self) -> &'static str {
                        match self {
                            Type::PlaybackMarkerIdentifierFirst => {
                                "PLAYBACK_MARKER_IDENTIFIER_FIRST"
                            }
                            Type::PlaybackMarkerIdentifierPrevious => {
                                "PLAYBACK_MARKER_IDENTIFIER_PREVIOUS"
                            }
                            Type::PlaybackMarkerIdentifierNext => {
                                "PLAYBACK_MARKER_IDENTIFIER_NEXT"
                            }
                            Type::PlaybackMarkerIdentifierLast => {
                                "PLAYBACK_MARKER_IDENTIFIER_LAST"
                            }
                            Type::PlaybackMarkerIdentifierName => {
                                "PLAYBACK_MARKER_IDENTIFIER_NAME"
                            }
                        }
                    }
                    /// Creates an enum from field names used in the ProtoBuf definition.
                    pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                        match value {
                            "PLAYBACK_MARKER_IDENTIFIER_FIRST" => {
                                Some(Self::PlaybackMarkerIdentifierFirst)
                            }
                            "PLAYBACK_MARKER_IDENTIFIER_PREVIOUS" => {
                                Some(Self::PlaybackMarkerIdentifierPrevious)
                            }
                            "PLAYBACK_MARKER_IDENTIFIER_NEXT" => {
                                Some(Self::PlaybackMarkerIdentifierNext)
                            }
                            "PLAYBACK_MARKER_IDENTIFIER_LAST" => {
                                Some(Self::PlaybackMarkerIdentifierLast)
                            }
                            "PLAYBACK_MARKER_IDENTIFIER_NAME" => {
                                Some(Self::PlaybackMarkerIdentifierName)
                            }
                            _ => None,
                        }
                    }
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct PlaybackMarkerText {
                #[prost(message, optional, tag = "1")]
                pub identifier: ::core::option::Option<PlaybackMarkerIdentifier>,
                #[prost(bool, tag = "4")]
                pub should_use_marker_color: bool,
                #[prost(oneof = "playback_marker_text::TextType", tags = "2, 3")]
                pub text_type: ::core::option::Option<playback_marker_text::TextType>,
            }
            /// Nested message and enum types in `PlaybackMarkerText`.
            pub mod playback_marker_text {
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct Name {}
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Message)]
                pub struct Time {
                    #[prost(message, optional, tag = "1")]
                    pub format: ::core::option::Option<
                        super::super::super::super::timer::Format,
                    >,
                }
                #[allow(clippy::derive_partial_eq_without_eq)]
                #[derive(Clone, PartialEq, ::prost::Oneof)]
                pub enum TextType {
                    #[prost(message, tag = "2")]
                    Name(Name),
                    #[prost(message, tag = "3")]
                    Time(Time),
                }
            }
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct ChordProChart {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct TimecodeText {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Message)]
            pub struct TimecodeStatus {}
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Oneof)]
            pub enum PropertyType {
                #[prost(message, tag = "1")]
                Ticker(Ticker),
                #[prost(message, tag = "2")]
                AlternateText(AlternateElementText),
                #[prost(message, tag = "3")]
                TimerText(TimerText),
                #[prost(message, tag = "4")]
                ClockText(ClockText),
                #[prost(message, tag = "5")]
                ChordChart(ChordChart),
                #[prost(message, tag = "6")]
                OutputScreen(OutputScreen),
                #[prost(message, tag = "7")]
                PcoLive(PcoLive),
                #[prost(message, tag = "8")]
                AlternateFill(AlternateElementFill),
                #[prost(message, tag = "9")]
                VisibilityLink(VisibilityLink),
                #[prost(message, tag = "10")]
                SlideText(SlideText),
                #[prost(message, tag = "11")]
                StageMessage(StageMessage),
                #[prost(message, tag = "12")]
                VideoCountdown(VideoCountdown),
                #[prost(message, tag = "13")]
                SlideImage(SlideImage),
                #[prost(message, tag = "14")]
                CcliText(CcliText),
                #[prost(message, tag = "15")]
                GroupName(GroupName),
                #[prost(message, tag = "16")]
                GroupColor(GroupColor),
                #[prost(message, tag = "17")]
                PresentationNotes(PresentationNotes),
                #[prost(message, tag = "18")]
                PlaylistItem(PlaylistItem),
                #[prost(message, tag = "19")]
                AutoAdvanceTimeRemaining(AutoAdvanceTimeRemaining),
                #[prost(message, tag = "20")]
                CaptureStatusText(CaptureStatusText),
                #[prost(message, tag = "21")]
                CaptureStatusColor(CaptureStatusColor),
                #[prost(message, tag = "22")]
                SlideCount(SlideCount),
                #[prost(message, tag = "23")]
                AudioCountdown(AudioCountdown),
                #[prost(message, tag = "24")]
                Presentation(Presentation),
                #[prost(message, tag = "25")]
                SlideLabelText(SlideLabelText),
                #[prost(message, tag = "26")]
                SlideLabelColor(SlideLabelColor),
                #[prost(message, tag = "27")]
                RssFeed(RssFeed),
                #[prost(message, tag = "28")]
                FileFeed(FileFeed),
                #[prost(message, tag = "29")]
                ChordProChart(ChordProChart),
                #[prost(message, tag = "30")]
                PlaybackMarkerText(PlaybackMarkerText),
                #[prost(message, tag = "32")]
                PlaybackMarkerColor(PlaybackMarkerIdentifier),
                #[prost(message, tag = "33")]
                TimecodeText(TimecodeText),
                #[prost(message, tag = "34")]
                TimecodeStatus(TimecodeStatus),
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TextScroller {
            #[prost(bool, tag = "1")]
            pub should_scroll: bool,
            #[prost(double, tag = "2")]
            pub scroll_rate: f64,
            #[prost(bool, tag = "3")]
            pub should_repeat: bool,
            #[prost(double, tag = "4")]
            pub repeat_distance: f64,
            #[prost(enumeration = "text_scroller::Direction", tag = "5")]
            pub scrolling_direction: i32,
            #[prost(bool, tag = "6")]
            pub starts_off_screen: bool,
            #[prost(double, tag = "7")]
            pub fade_left: f64,
            #[prost(double, tag = "8")]
            pub fade_right: f64,
        }
        /// Nested message and enum types in `TextScroller`.
        pub mod text_scroller {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum Direction {
                Left = 0,
                Right = 1,
                Up = 2,
                Down = 3,
            }
            impl Direction {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        Direction::Left => "DIRECTION_LEFT",
                        Direction::Right => "DIRECTION_RIGHT",
                        Direction::Up => "DIRECTION_UP",
                        Direction::Down => "DIRECTION_DOWN",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "DIRECTION_LEFT" => Some(Self::Left),
                        "DIRECTION_RIGHT" => Some(Self::Right),
                        "DIRECTION_UP" => Some(Self::Up),
                        "DIRECTION_DOWN" => Some(Self::Down),
                        _ => None,
                    }
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum TextRevealType {
            None = 0,
            Bullet = 1,
            Underline = 2,
        }
        impl TextRevealType {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    TextRevealType::None => "TEXT_REVEAL_TYPE_NONE",
                    TextRevealType::Bullet => "TEXT_REVEAL_TYPE_BULLET",
                    TextRevealType::Underline => "TEXT_REVEAL_TYPE_UNDERLINE",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "TEXT_REVEAL_TYPE_NONE" => Some(Self::None),
                    "TEXT_REVEAL_TYPE_BULLET" => Some(Self::Bullet),
                    "TEXT_REVEAL_TYPE_UNDERLINE" => Some(Self::Underline),
                    _ => None,
                }
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PresentationSlide {
    #[prost(message, optional, tag = "1")]
    pub base_slide: ::core::option::Option<Slide>,
    #[prost(message, optional, tag = "2")]
    pub notes: ::core::option::Option<presentation_slide::Notes>,
    #[prost(message, repeated, tag = "3")]
    pub template_guidelines: ::prost::alloc::vec::Vec<AlignmentGuide>,
    #[prost(message, optional, tag = "4")]
    pub chord_chart: ::core::option::Option<Url>,
    #[prost(message, optional, tag = "5")]
    pub transition: ::core::option::Option<Transition>,
}
/// Nested message and enum types in `PresentationSlide`.
pub mod presentation_slide {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Notes {
        #[prost(bytes = "vec", tag = "1")]
        pub rtf_data: ::prost::alloc::vec::Vec<u8>,
        #[prost(message, optional, tag = "2")]
        pub attributes: ::core::option::Option<super::graphics::text::Attributes>,
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PropSlide {
    #[prost(message, optional, tag = "1")]
    pub base_slide: ::core::option::Option<Slide>,
    #[prost(message, optional, tag = "2")]
    pub transition: ::core::option::Option<Transition>,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct TemplateIdentification {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub name: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub slide_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "4")]
    pub slide_name: ::prost::alloc::string::String,
    #[prost(uint32, tag = "5")]
    pub slide_index: u32,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Message {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub title: ::prost::alloc::string::String,
    #[prost(double, tag = "3")]
    pub time_to_remove: f64,
    #[prost(bool, tag = "4")]
    pub visible_on_network: bool,
    #[prost(message, optional, tag = "6")]
    pub template: ::core::option::Option<TemplateIdentification>,
    #[prost(enumeration = "message::ClearType", tag = "9")]
    pub clear_type: i32,
    #[prost(string, tag = "10")]
    pub message_text: ::prost::alloc::string::String,
    #[prost(message, repeated, tag = "11")]
    pub tokens: ::prost::alloc::vec::Vec<message::Token>,
    #[prost(message, repeated, tag = "12")]
    pub token_values: ::prost::alloc::vec::Vec<message::TokenValue>,
}
/// Nested message and enum types in `Message`.
pub mod message {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Token {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(oneof = "token::TokenType", tags = "2, 3, 4")]
        pub token_type: ::core::option::Option<token::TokenType>,
    }
    /// Nested message and enum types in `Token`.
    pub mod token {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TokenTypeText {
            #[prost(string, tag = "1")]
            pub name: ::prost::alloc::string::String,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TokenTypeTimer {
            #[prost(string, tag = "1")]
            pub name: ::prost::alloc::string::String,
            #[prost(message, optional, tag = "2")]
            pub timer_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TokenTypeClock {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum TokenType {
            #[prost(message, tag = "2")]
            Text(TokenTypeText),
            #[prost(message, tag = "3")]
            Timer(TokenTypeTimer),
            #[prost(message, tag = "4")]
            Clock(TokenTypeClock),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct TokenValue {
        #[prost(message, optional, tag = "1")]
        pub token_id: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "5")]
        pub token_name: ::prost::alloc::string::String,
        #[prost(oneof = "token_value::TokenValueType", tags = "2, 3, 4")]
        pub token_value_type: ::core::option::Option<token_value::TokenValueType>,
    }
    /// Nested message and enum types in `TokenValue`.
    pub mod token_value {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TokenValueText {
            #[prost(string, tag = "1")]
            pub value: ::prost::alloc::string::String,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TokenValueTimer {
            #[prost(message, optional, tag = "1")]
            pub configuration: ::core::option::Option<
                super::super::timer::Configuration,
            >,
            #[prost(message, optional, tag = "2")]
            pub format: ::core::option::Option<super::super::timer::Format>,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TokenValueClock {
            #[prost(message, optional, tag = "1")]
            pub format: ::core::option::Option<super::super::clock::Format>,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum TokenValueType {
            #[prost(message, tag = "2")]
            Text(TokenValueText),
            #[prost(message, tag = "3")]
            Timer(TokenValueTimer),
            #[prost(message, tag = "4")]
            Clock(TokenValueClock),
        }
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum ClearType {
        Manual = 0,
        AfterTime = 1,
        AfterTimers = 2,
    }
    impl ClearType {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                ClearType::Manual => "CLEAR_TYPE_MANUAL",
                ClearType::AfterTime => "CLEAR_TYPE_AFTER_TIME",
                ClearType::AfterTimers => "CLEAR_TYPE_AFTER_TIMERS",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CLEAR_TYPE_MANUAL" => Some(Self::Manual),
                "CLEAR_TYPE_AFTER_TIME" => Some(Self::AfterTime),
                "CLEAR_TYPE_AFTER_TIMERS" => Some(Self::AfterTimers),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MessageDocument {
    #[prost(message, optional, tag = "1")]
    pub application_info: ::core::option::Option<ApplicationInfo>,
    #[prost(message, repeated, tag = "2")]
    pub messages: ::prost::alloc::vec::Vec<Message>,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Stage {}
/// Nested message and enum types in `Stage`.
pub mod stage {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Layout {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub slide: ::core::option::Option<super::Slide>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Document {
        #[prost(message, optional, tag = "1")]
        pub application_info: ::core::option::Option<super::ApplicationInfo>,
        #[prost(message, repeated, tag = "2")]
        pub layouts: ::prost::alloc::vec::Vec<Layout>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ScreenAssignment {
        #[prost(message, optional, tag = "1")]
        pub screen: ::core::option::Option<super::CollectionElementType>,
        #[prost(message, optional, tag = "2")]
        pub layout: ::core::option::Option<super::CollectionElementType>,
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct VideoInput {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub user_description: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub video_input_device: ::core::option::Option<media::VideoDevice>,
    #[prost(message, optional, tag = "4")]
    pub display_color: ::core::option::Option<Color>,
    #[prost(message, optional, tag = "5")]
    pub thumbnail_path: ::core::option::Option<Url>,
    #[prost(enumeration = "video_input::AudioDeviceType", tag = "8")]
    pub audio_type: i32,
    #[prost(oneof = "video_input::AltAudioSource", tags = "6, 7")]
    pub alt_audio_source: ::core::option::Option<video_input::AltAudioSource>,
}
/// Nested message and enum types in `VideoInput`.
pub mod video_input {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SettingsDocument {
        #[prost(message, repeated, tag = "1")]
        pub inputs: ::prost::alloc::vec::Vec<super::VideoInput>,
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum AudioDeviceType {
        Default = 0,
        None = 1,
        Alternate = 2,
    }
    impl AudioDeviceType {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                AudioDeviceType::Default => "AUDIO_DEVICE_TYPE_DEFAULT",
                AudioDeviceType::None => "AUDIO_DEVICE_TYPE_NONE",
                AudioDeviceType::Alternate => "AUDIO_DEVICE_TYPE_ALTERNATE",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "AUDIO_DEVICE_TYPE_DEFAULT" => Some(Self::Default),
                "AUDIO_DEVICE_TYPE_NONE" => Some(Self::None),
                "AUDIO_DEVICE_TYPE_ALTERNATE" => Some(Self::Alternate),
                _ => None,
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum AltAudioSource {
        #[prost(message, tag = "6")]
        AudioDevice(super::digital_audio::Device),
        #[prost(message, tag = "7")]
        VideoDevice(super::media::VideoDevice),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AudioInput {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub user_description: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "5")]
    pub behavior_mode: ::core::option::Option<audio_input::BehaviorMode>,
    #[prost(oneof = "audio_input::Source", tags = "3, 4")]
    pub source: ::core::option::Option<audio_input::Source>,
}
/// Nested message and enum types in `AudioInput`.
pub mod audio_input {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BehaviorMode {
        #[prost(oneof = "behavior_mode::Mode", tags = "1, 2, 3, 4")]
        pub mode: ::core::option::Option<behavior_mode::Mode>,
    }
    /// Nested message and enum types in `BehaviorMode`.
    pub mod behavior_mode {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct On {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Off {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AutoOff {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AutoOn {
            #[prost(uint32, repeated, tag = "1")]
            pub linked_video_inputs: ::prost::alloc::vec::Vec<u32>,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Mode {
            #[prost(message, tag = "1")]
            On(On),
            #[prost(message, tag = "2")]
            Off(Off),
            #[prost(message, tag = "3")]
            AutoOn(AutoOn),
            #[prost(message, tag = "4")]
            AutoOff(AutoOff),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Source {
        #[prost(message, tag = "3")]
        AudioDevice(super::digital_audio::Device),
        #[prost(message, tag = "4")]
        VideoDevice(super::media::VideoDevice),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Action {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub name: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub label: ::core::option::Option<action::Label>,
    #[prost(double, tag = "4")]
    pub delay_time: f64,
    #[prost(message, optional, tag = "5")]
    pub old_type: ::core::option::Option<action::OldType>,
    #[prost(bool, tag = "6")]
    pub is_enabled: bool,
    #[prost(message, optional, tag = "7")]
    pub layer_identification: ::core::option::Option<action::LayerIdentification>,
    #[prost(double, tag = "8")]
    pub duration: f64,
    #[prost(enumeration = "action::ActionType", tag = "9")]
    pub r#type: i32,
    #[prost(
        oneof = "action::ActionTypeData",
        tags = "16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42"
    )]
    pub action_type_data: ::core::option::Option<action::ActionTypeData>,
}
/// Nested message and enum types in `Action`.
pub mod action {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct OldType {
        #[prost(enumeration = "old_type::Category", tag = "1")]
        pub category: i32,
        #[prost(int32, tag = "2")]
        pub application_type: i32,
    }
    /// Nested message and enum types in `OldType`.
    pub mod old_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Category {
            Unknown = 0,
            Media = 1,
            Application = 2,
        }
        impl Category {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Category::Unknown => "CATEGORY_UNKNOWN",
                    Category::Media => "CATEGORY_MEDIA",
                    Category::Application => "CATEGORY_APPLICATION",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "CATEGORY_UNKNOWN" => Some(Self::Unknown),
                    "CATEGORY_MEDIA" => Some(Self::Media),
                    "CATEGORY_APPLICATION" => Some(Self::Application),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Label {
        #[prost(string, tag = "2")]
        pub text: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub color: ::core::option::Option<super::Color>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct LayerIdentification {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct PlaylistItemType {
        #[prost(message, optional, tag = "1")]
        pub playlist_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub playlist_name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub item_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "4")]
        pub item_name: ::prost::alloc::string::String,
        #[prost(bool, tag = "5")]
        pub select_playlist: bool,
        #[prost(bool, tag = "6")]
        pub always_retrigger: bool,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BlendModeType {
        #[prost(enumeration = "blend_mode_type::BlendMode", tag = "1")]
        pub blend_mode: i32,
        #[prost(message, optional, tag = "2")]
        pub blend: ::core::option::Option<super::layer::Blending>,
    }
    /// Nested message and enum types in `BlendModeType`.
    pub mod blend_mode_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum BlendMode {
            Normal = 0,
            Dissolve = 1,
            Darken = 2,
            Multiply = 3,
            ColorBurn = 4,
            LinearBurn = 5,
            DarkerColor = 6,
            Lighten = 7,
            Screen = 8,
            ColorDodge = 9,
            LinearDodge = 10,
            LighterColor = 11,
            Overlay = 12,
            SoftLight = 13,
            HardLight = 14,
            VividLight = 15,
            LinearLight = 16,
            PinLight = 17,
            HardMix = 18,
            Difference = 19,
            Exclusion = 20,
            Subtract = 21,
            Divide = 22,
            Hue = 23,
            Saturation = 24,
            Color = 25,
            Luminosity = 26,
        }
        impl BlendMode {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    BlendMode::Normal => "BLEND_MODE_NORMAL",
                    BlendMode::Dissolve => "BLEND_MODE_DISSOLVE",
                    BlendMode::Darken => "BLEND_MODE_DARKEN",
                    BlendMode::Multiply => "BLEND_MODE_MULTIPLY",
                    BlendMode::ColorBurn => "BLEND_MODE_COLOR_BURN",
                    BlendMode::LinearBurn => "BLEND_MODE_LINEAR_BURN",
                    BlendMode::DarkerColor => "BLEND_MODE_DARKER_COLOR",
                    BlendMode::Lighten => "BLEND_MODE_LIGHTEN",
                    BlendMode::Screen => "BLEND_MODE_SCREEN",
                    BlendMode::ColorDodge => "BLEND_MODE_COLOR_DODGE",
                    BlendMode::LinearDodge => "BLEND_MODE_LINEAR_DODGE",
                    BlendMode::LighterColor => "BLEND_MODE_LIGHTER_COLOR",
                    BlendMode::Overlay => "BLEND_MODE_OVERLAY",
                    BlendMode::SoftLight => "BLEND_MODE_SOFT_LIGHT",
                    BlendMode::HardLight => "BLEND_MODE_HARD_LIGHT",
                    BlendMode::VividLight => "BLEND_MODE_VIVID_LIGHT",
                    BlendMode::LinearLight => "BLEND_MODE_LINEAR_LIGHT",
                    BlendMode::PinLight => "BLEND_MODE_PIN_LIGHT",
                    BlendMode::HardMix => "BLEND_MODE_HARD_MIX",
                    BlendMode::Difference => "BLEND_MODE_DIFFERENCE",
                    BlendMode::Exclusion => "BLEND_MODE_EXCLUSION",
                    BlendMode::Subtract => "BLEND_MODE_SUBTRACT",
                    BlendMode::Divide => "BLEND_MODE_DIVIDE",
                    BlendMode::Hue => "BLEND_MODE_HUE",
                    BlendMode::Saturation => "BLEND_MODE_SATURATION",
                    BlendMode::Color => "BLEND_MODE_COLOR",
                    BlendMode::Luminosity => "BLEND_MODE_LUMINOSITY",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "BLEND_MODE_NORMAL" => Some(Self::Normal),
                    "BLEND_MODE_DISSOLVE" => Some(Self::Dissolve),
                    "BLEND_MODE_DARKEN" => Some(Self::Darken),
                    "BLEND_MODE_MULTIPLY" => Some(Self::Multiply),
                    "BLEND_MODE_COLOR_BURN" => Some(Self::ColorBurn),
                    "BLEND_MODE_LINEAR_BURN" => Some(Self::LinearBurn),
                    "BLEND_MODE_DARKER_COLOR" => Some(Self::DarkerColor),
                    "BLEND_MODE_LIGHTEN" => Some(Self::Lighten),
                    "BLEND_MODE_SCREEN" => Some(Self::Screen),
                    "BLEND_MODE_COLOR_DODGE" => Some(Self::ColorDodge),
                    "BLEND_MODE_LINEAR_DODGE" => Some(Self::LinearDodge),
                    "BLEND_MODE_LIGHTER_COLOR" => Some(Self::LighterColor),
                    "BLEND_MODE_OVERLAY" => Some(Self::Overlay),
                    "BLEND_MODE_SOFT_LIGHT" => Some(Self::SoftLight),
                    "BLEND_MODE_HARD_LIGHT" => Some(Self::HardLight),
                    "BLEND_MODE_VIVID_LIGHT" => Some(Self::VividLight),
                    "BLEND_MODE_LINEAR_LIGHT" => Some(Self::LinearLight),
                    "BLEND_MODE_PIN_LIGHT" => Some(Self::PinLight),
                    "BLEND_MODE_HARD_MIX" => Some(Self::HardMix),
                    "BLEND_MODE_DIFFERENCE" => Some(Self::Difference),
                    "BLEND_MODE_EXCLUSION" => Some(Self::Exclusion),
                    "BLEND_MODE_SUBTRACT" => Some(Self::Subtract),
                    "BLEND_MODE_DIVIDE" => Some(Self::Divide),
                    "BLEND_MODE_HUE" => Some(Self::Hue),
                    "BLEND_MODE_SATURATION" => Some(Self::Saturation),
                    "BLEND_MODE_COLOR" => Some(Self::Color),
                    "BLEND_MODE_LUMINOSITY" => Some(Self::Luminosity),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct TransitionType {
        #[prost(string, tag = "1")]
        pub transition_name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "2")]
        pub transition: ::core::option::Option<super::Transition>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct DoubleType {
        #[prost(double, tag = "1")]
        pub value: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct EffectsType {
        #[prost(message, repeated, tag = "1")]
        pub effects: ::prost::alloc::vec::Vec<super::Effect>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MediaType {
        #[prost(double, tag = "1")]
        pub transition_duration: f64,
        #[prost(message, optional, tag = "2")]
        pub selected_effect_preset_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(message, optional, tag = "3")]
        pub transition: ::core::option::Option<super::Transition>,
        #[prost(message, repeated, tag = "4")]
        pub effects: ::prost::alloc::vec::Vec<super::Effect>,
        #[prost(message, optional, tag = "5")]
        pub element: ::core::option::Option<super::Media>,
        #[prost(enumeration = "media_type::LayerType", tag = "10")]
        pub layer_type: i32,
        #[prost(bool, tag = "11")]
        pub always_retrigger: bool,
        #[prost(message, repeated, tag = "12")]
        pub markers: ::prost::alloc::vec::Vec<media_type::PlaybackMarker>,
        #[prost(oneof = "media_type::MediaType", tags = "6, 7, 8, 9")]
        pub media_type: ::core::option::Option<media_type::MediaType>,
    }
    /// Nested message and enum types in `MediaType`.
    pub mod media_type {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Image {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Video {
            #[prost(enumeration = "video::PlaybackBehavior", tag = "1")]
            pub playback_behavior: i32,
            #[prost(enumeration = "video::EndBehavior", tag = "2")]
            pub end_behavior: i32,
            #[prost(double, tag = "3")]
            pub loop_time: f64,
            #[prost(uint32, tag = "4")]
            pub times_to_loop: u32,
            #[prost(bool, tag = "5")]
            pub soft_loop: bool,
            #[prost(double, tag = "6")]
            pub soft_loop_duration: f64,
        }
        /// Nested message and enum types in `Video`.
        pub mod video {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum PlaybackBehavior {
                Stop = 0,
                Loop = 1,
                LoopForCount = 2,
                LoopForTime = 3,
            }
            impl PlaybackBehavior {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        PlaybackBehavior::Stop => "PLAYBACK_BEHAVIOR_STOP",
                        PlaybackBehavior::Loop => "PLAYBACK_BEHAVIOR_LOOP",
                        PlaybackBehavior::LoopForCount => {
                            "PLAYBACK_BEHAVIOR_LOOP_FOR_COUNT"
                        }
                        PlaybackBehavior::LoopForTime => {
                            "PLAYBACK_BEHAVIOR_LOOP_FOR_TIME"
                        }
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "PLAYBACK_BEHAVIOR_STOP" => Some(Self::Stop),
                        "PLAYBACK_BEHAVIOR_LOOP" => Some(Self::Loop),
                        "PLAYBACK_BEHAVIOR_LOOP_FOR_COUNT" => Some(Self::LoopForCount),
                        "PLAYBACK_BEHAVIOR_LOOP_FOR_TIME" => Some(Self::LoopForTime),
                        _ => None,
                    }
                }
            }
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum EndBehavior {
                Stop = 0,
                StopOnBlack = 1,
                StopOnClear = 2,
                FadeToBlack = 3,
                FadeToClear = 4,
            }
            impl EndBehavior {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        EndBehavior::Stop => "END_BEHAVIOR_STOP",
                        EndBehavior::StopOnBlack => "END_BEHAVIOR_STOP_ON_BLACK",
                        EndBehavior::StopOnClear => "END_BEHAVIOR_STOP_ON_CLEAR",
                        EndBehavior::FadeToBlack => "END_BEHAVIOR_FADE_TO_BLACK",
                        EndBehavior::FadeToClear => "END_BEHAVIOR_FADE_TO_CLEAR",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "END_BEHAVIOR_STOP" => Some(Self::Stop),
                        "END_BEHAVIOR_STOP_ON_BLACK" => Some(Self::StopOnBlack),
                        "END_BEHAVIOR_STOP_ON_CLEAR" => Some(Self::StopOnClear),
                        "END_BEHAVIOR_FADE_TO_BLACK" => Some(Self::FadeToBlack),
                        "END_BEHAVIOR_FADE_TO_CLEAR" => Some(Self::FadeToClear),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Audio {
            #[prost(enumeration = "audio::PlaybackBehavior", tag = "1")]
            pub playback_behavior: i32,
            #[prost(double, tag = "2")]
            pub loop_time: f64,
            #[prost(uint32, tag = "3")]
            pub times_to_loop: u32,
            #[prost(enumeration = "audio::MediaActionAudioType", tag = "4")]
            pub audio_type: i32,
        }
        /// Nested message and enum types in `Audio`.
        pub mod audio {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum PlaybackBehavior {
                Stop = 0,
                Loop = 1,
                LoopForCount = 2,
                LoopForTime = 3,
            }
            impl PlaybackBehavior {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        PlaybackBehavior::Stop => "PLAYBACK_BEHAVIOR_STOP",
                        PlaybackBehavior::Loop => "PLAYBACK_BEHAVIOR_LOOP",
                        PlaybackBehavior::LoopForCount => {
                            "PLAYBACK_BEHAVIOR_LOOP_FOR_COUNT"
                        }
                        PlaybackBehavior::LoopForTime => {
                            "PLAYBACK_BEHAVIOR_LOOP_FOR_TIME"
                        }
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "PLAYBACK_BEHAVIOR_STOP" => Some(Self::Stop),
                        "PLAYBACK_BEHAVIOR_LOOP" => Some(Self::Loop),
                        "PLAYBACK_BEHAVIOR_LOOP_FOR_COUNT" => Some(Self::LoopForCount),
                        "PLAYBACK_BEHAVIOR_LOOP_FOR_TIME" => Some(Self::LoopForTime),
                        _ => None,
                    }
                }
            }
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum MediaActionAudioType {
                Tune = 0,
                Sound = 1,
            }
            impl MediaActionAudioType {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        MediaActionAudioType::Tune => "MEDIA_ACTION_AUDIO_TYPE_TUNE",
                        MediaActionAudioType::Sound => "MEDIA_ACTION_AUDIO_TYPE_SOUND",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "MEDIA_ACTION_AUDIO_TYPE_TUNE" => Some(Self::Tune),
                        "MEDIA_ACTION_AUDIO_TYPE_SOUND" => Some(Self::Sound),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct LiveVideo {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct PlaybackMarker {
            #[prost(message, optional, tag = "1")]
            pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
            #[prost(double, tag = "2")]
            pub time: f64,
            #[prost(message, optional, tag = "3")]
            pub color: ::core::option::Option<super::super::Color>,
            #[prost(string, tag = "4")]
            pub name: ::prost::alloc::string::String,
            #[prost(message, repeated, tag = "5")]
            pub actions: ::prost::alloc::vec::Vec<super::super::Action>,
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum LayerType {
            Background = 0,
            Foreground = 1,
            Fill = 2,
            Input = 3,
        }
        impl LayerType {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    LayerType::Background => "LAYER_TYPE_BACKGROUND",
                    LayerType::Foreground => "LAYER_TYPE_FOREGROUND",
                    LayerType::Fill => "LAYER_TYPE_FILL",
                    LayerType::Input => "LAYER_TYPE_INPUT",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "LAYER_TYPE_BACKGROUND" => Some(Self::Background),
                    "LAYER_TYPE_FOREGROUND" => Some(Self::Foreground),
                    "LAYER_TYPE_FILL" => Some(Self::Fill),
                    "LAYER_TYPE_INPUT" => Some(Self::Input),
                    _ => None,
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum MediaType {
            #[prost(message, tag = "6")]
            Image(Image),
            #[prost(message, tag = "7")]
            Video(Video),
            #[prost(message, tag = "8")]
            Audio(Audio),
            #[prost(message, tag = "9")]
            LiveVideo(LiveVideo),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SlideType {
        #[prost(oneof = "slide_type::Slide", tags = "2, 3")]
        pub slide: ::core::option::Option<slide_type::Slide>,
    }
    /// Nested message and enum types in `SlideType`.
    pub mod slide_type {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Slide {
            #[prost(message, tag = "2")]
            Presentation(super::super::PresentationSlide),
            #[prost(message, tag = "3")]
            Prop(super::super::PropSlide),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BackgroundType {
        #[prost(message, optional, tag = "1")]
        pub element: ::core::option::Option<super::Background>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct TimerType {
        #[prost(enumeration = "timer_type::TimerAction", tag = "2")]
        pub action_type: i32,
        #[prost(message, optional, tag = "4")]
        pub timer_identification: ::core::option::Option<super::CollectionElementType>,
        #[prost(message, optional, tag = "10")]
        pub timer_configuration: ::core::option::Option<super::timer::Configuration>,
    }
    /// Nested message and enum types in `TimerType`.
    pub mod timer_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum TimerAction {
            ActionStart = 0,
            ActionStop = 1,
            ActionReset = 2,
            ActionResetAndStart = 3,
            ActionStopAndReset = 4,
        }
        impl TimerAction {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    TimerAction::ActionStart => "ACTION_START",
                    TimerAction::ActionStop => "ACTION_STOP",
                    TimerAction::ActionReset => "ACTION_RESET",
                    TimerAction::ActionResetAndStart => "ACTION_RESET_AND_START",
                    TimerAction::ActionStopAndReset => "ACTION_STOP_AND_RESET",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "ACTION_START" => Some(Self::ActionStart),
                    "ACTION_STOP" => Some(Self::ActionStop),
                    "ACTION_RESET" => Some(Self::ActionReset),
                    "ACTION_RESET_AND_START" => Some(Self::ActionResetAndStart),
                    "ACTION_STOP_AND_RESET" => Some(Self::ActionStopAndReset),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ClearType {
        #[prost(enumeration = "clear_type::ClearTargetLayer", tag = "2")]
        pub target_layer: i32,
        #[prost(enumeration = "clear_type::ContentDestination", tag = "4")]
        pub content_destination: i32,
    }
    /// Nested message and enum types in `ClearType`.
    pub mod clear_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum ClearTargetLayer {
            All = 0,
            Audio = 1,
            Background = 2,
            LiveVideo = 3,
            Prop = 4,
            Slide = 5,
            Logo = 6,
            Messages = 7,
            AudioEffects = 8,
        }
        impl ClearTargetLayer {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    ClearTargetLayer::All => "CLEAR_TARGET_LAYER_ALL",
                    ClearTargetLayer::Audio => "CLEAR_TARGET_LAYER_AUDIO",
                    ClearTargetLayer::Background => "CLEAR_TARGET_LAYER_BACKGROUND",
                    ClearTargetLayer::LiveVideo => "CLEAR_TARGET_LAYER_LIVE_VIDEO",
                    ClearTargetLayer::Prop => "CLEAR_TARGET_LAYER_PROP",
                    ClearTargetLayer::Slide => "CLEAR_TARGET_LAYER_SLIDE",
                    ClearTargetLayer::Logo => "CLEAR_TARGET_LAYER_LOGO",
                    ClearTargetLayer::Messages => "CLEAR_TARGET_LAYER_MESSAGES",
                    ClearTargetLayer::AudioEffects => "CLEAR_TARGET_LAYER_AUDIO_EFFECTS",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "CLEAR_TARGET_LAYER_ALL" => Some(Self::All),
                    "CLEAR_TARGET_LAYER_AUDIO" => Some(Self::Audio),
                    "CLEAR_TARGET_LAYER_BACKGROUND" => Some(Self::Background),
                    "CLEAR_TARGET_LAYER_LIVE_VIDEO" => Some(Self::LiveVideo),
                    "CLEAR_TARGET_LAYER_PROP" => Some(Self::Prop),
                    "CLEAR_TARGET_LAYER_SLIDE" => Some(Self::Slide),
                    "CLEAR_TARGET_LAYER_LOGO" => Some(Self::Logo),
                    "CLEAR_TARGET_LAYER_MESSAGES" => Some(Self::Messages),
                    "CLEAR_TARGET_LAYER_AUDIO_EFFECTS" => Some(Self::AudioEffects),
                    _ => None,
                }
            }
        }
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum ContentDestination {
            Global = 0,
            Announcements = 1,
        }
        impl ContentDestination {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    ContentDestination::Global => "CONTENT_DESTINATION_GLOBAL",
                    ContentDestination::Announcements => {
                        "CONTENT_DESTINATION_ANNOUNCEMENTS"
                    }
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "CONTENT_DESTINATION_GLOBAL" => Some(Self::Global),
                    "CONTENT_DESTINATION_ANNOUNCEMENTS" => Some(Self::Announcements),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ClearGroupType {
        #[prost(message, optional, tag = "1")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct TransportControlType {
        #[prost(oneof = "transport_control_type::Command", tags = "1, 2, 3")]
        pub command: ::core::option::Option<transport_control_type::Command>,
    }
    /// Nested message and enum types in `TransportControlType`.
    pub mod transport_control_type {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Play {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Pause {}
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct JumpToTime {
            #[prost(double, tag = "1")]
            pub time: f64,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Command {
            #[prost(message, tag = "1")]
            Play(Play),
            #[prost(message, tag = "2")]
            Pause(Pause),
            #[prost(message, tag = "3")]
            JumpToTime(JumpToTime),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct StageLayoutType {
        #[prost(message, repeated, tag = "4")]
        pub stage_screen_assignments: ::prost::alloc::vec::Vec<
            super::stage::ScreenAssignment,
        >,
        #[prost(enumeration = "stage_layout_type::SlideTarget", tag = "5")]
        pub slide_target: i32,
    }
    /// Nested message and enum types in `StageLayoutType`.
    pub mod stage_layout_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum SlideTarget {
            NoChange = 0,
            StageOnly = 1,
            All = 2,
        }
        impl SlideTarget {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    SlideTarget::NoChange => "SLIDE_TARGET_NO_CHANGE",
                    SlideTarget::StageOnly => "SLIDE_TARGET_STAGE_ONLY",
                    SlideTarget::All => "SLIDE_TARGET_ALL",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SLIDE_TARGET_NO_CHANGE" => Some(Self::NoChange),
                    "SLIDE_TARGET_STAGE_ONLY" => Some(Self::StageOnly),
                    "SLIDE_TARGET_ALL" => Some(Self::All),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SlideDestinationType {
        #[prost(enumeration = "slide_destination_type::SlideTarget", tag = "1")]
        pub slide_target: i32,
    }
    /// Nested message and enum types in `SlideDestinationType`.
    pub mod slide_destination_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum SlideTarget {
            NoChange = 0,
            StageOnly = 1,
            All = 2,
        }
        impl SlideTarget {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    SlideTarget::NoChange => "SLIDE_TARGET_NO_CHANGE",
                    SlideTarget::StageOnly => "SLIDE_TARGET_STAGE_ONLY",
                    SlideTarget::All => "SLIDE_TARGET_ALL",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SLIDE_TARGET_NO_CHANGE" => Some(Self::NoChange),
                    "SLIDE_TARGET_STAGE_ONLY" => Some(Self::StageOnly),
                    "SLIDE_TARGET_ALL" => Some(Self::All),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct PropType {
        #[prost(message, optional, tag = "3")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MaskType {
        #[prost(message, optional, tag = "3")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MessageType {
        #[prost(message, optional, tag = "1")]
        pub message_identificaton: ::core::option::Option<super::CollectionElementType>,
        #[prost(message, repeated, tag = "3")]
        pub content: ::prost::alloc::vec::Vec<super::message::TokenValue>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SocialMediaType {
        #[prost(enumeration = "social_media_type::SocialMediaAction", tag = "1")]
        pub action_type: i32,
        #[prost(double, tag = "2")]
        pub duration: f64,
    }
    /// Nested message and enum types in `SocialMediaType`.
    pub mod social_media_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum SocialMediaAction {
            LengthOfGotoNextTimer = 0,
            SpecifiedTime = 1,
        }
        impl SocialMediaAction {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    SocialMediaAction::LengthOfGotoNextTimer => {
                        "SOCIAL_MEDIA_ACTION_LENGTH_OF_GOTO_NEXT_TIMER"
                    }
                    SocialMediaAction::SpecifiedTime => {
                        "SOCIAL_MEDIA_ACTION_SPECIFIED_TIME"
                    }
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SOCIAL_MEDIA_ACTION_LENGTH_OF_GOTO_NEXT_TIMER" => {
                        Some(Self::LengthOfGotoNextTimer)
                    }
                    "SOCIAL_MEDIA_ACTION_SPECIFIED_TIME" => Some(Self::SpecifiedTime),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct CommunicationType {
        #[prost(message, optional, tag = "1")]
        pub device_identification: ::core::option::Option<super::CollectionElementType>,
        #[prost(string, tag = "2")]
        pub format: ::prost::alloc::string::String,
        #[prost(string, tag = "3")]
        pub description: ::prost::alloc::string::String,
        #[prost(message, repeated, tag = "4")]
        pub commands: ::prost::alloc::vec::Vec<communication_type::Command>,
        #[prost(oneof = "communication_type::CommandTypeData", tags = "5, 6, 7, 8")]
        pub command_type_data: ::core::option::Option<
            communication_type::CommandTypeData,
        >,
    }
    /// Nested message and enum types in `CommunicationType`.
    pub mod communication_type {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Command {
            #[prost(string, tag = "1")]
            pub name: ::prost::alloc::string::String,
            #[prost(string, tag = "2")]
            pub value: ::prost::alloc::string::String,
            #[prost(message, optional, tag = "3")]
            pub replacement_range: ::core::option::Option<super::super::IntRange>,
            #[prost(string, repeated, tag = "4")]
            pub possible_values: ::prost::alloc::vec::Vec<
                ::prost::alloc::string::String,
            >,
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct MidiCommand {
            #[prost(enumeration = "midi_command::State", tag = "1")]
            pub state: i32,
            #[prost(uint32, tag = "2")]
            pub channel: u32,
            #[prost(uint32, tag = "3")]
            pub note: u32,
            #[prost(uint32, tag = "4")]
            pub intensity: u32,
        }
        /// Nested message and enum types in `MIDICommand`.
        pub mod midi_command {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum State {
                On = 0,
                Off = 1,
            }
            impl State {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        State::On => "STATE_ON",
                        State::Off => "STATE_OFF",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "STATE_ON" => Some(Self::On),
                        "STATE_OFF" => Some(Self::Off),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct GlobalCacheCommand {
            #[prost(enumeration = "global_cache_command::CommandAction", tag = "1")]
            pub command_action: i32,
            #[prost(uint32, tag = "2")]
            pub output: u32,
            #[prost(uint32, tag = "3")]
            pub interval: u32,
        }
        /// Nested message and enum types in `GlobalCacheCommand`.
        pub mod global_cache_command {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum CommandAction {
                On = 0,
                Off = 1,
                OnOffWithInterval = 2,
                OffOnWithInterval = 3,
            }
            impl CommandAction {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        CommandAction::On => "COMMAND_ACTION_ON",
                        CommandAction::Off => "COMMAND_ACTION_OFF",
                        CommandAction::OnOffWithInterval => {
                            "COMMAND_ACTION_ON_OFF_WITH_INTERVAL"
                        }
                        CommandAction::OffOnWithInterval => {
                            "COMMAND_ACTION_OFF_ON_WITH_INTERVAL"
                        }
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "COMMAND_ACTION_ON" => Some(Self::On),
                        "COMMAND_ACTION_OFF" => Some(Self::Off),
                        "COMMAND_ACTION_ON_OFF_WITH_INTERVAL" => {
                            Some(Self::OnOffWithInterval)
                        }
                        "COMMAND_ACTION_OFF_ON_WITH_INTERVAL" => {
                            Some(Self::OffOnWithInterval)
                        }
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Gvg100Command {
            #[prost(enumeration = "gvg100_command::CommandAction", tag = "1")]
            pub command_action: i32,
        }
        /// Nested message and enum types in `GVG100Command`.
        pub mod gvg100_command {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum CommandAction {
                FadeToBlack = 0,
                DskToggle = 1,
            }
            impl CommandAction {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        CommandAction::FadeToBlack => "COMMAND_ACTION_FADE_TO_BLACK",
                        CommandAction::DskToggle => "COMMAND_ACTION_DSK_TOGGLE",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "COMMAND_ACTION_FADE_TO_BLACK" => Some(Self::FadeToBlack),
                        "COMMAND_ACTION_DSK_TOGGLE" => Some(Self::DskToggle),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct SonyBvsCommand {
            #[prost(enumeration = "sony_bvs_command::CommandAction", tag = "1")]
            pub command_action: i32,
        }
        /// Nested message and enum types in `SonyBVSCommand`.
        pub mod sony_bvs_command {
            #[derive(
                Clone,
                Copy,
                Debug,
                PartialEq,
                Eq,
                Hash,
                PartialOrd,
                Ord,
                ::prost::Enumeration
            )]
            #[repr(i32)]
            pub enum CommandAction {
                FadeToBlack = 0,
                KeyOn = 1,
                KeyOff = 2,
                DskOn = 3,
                DskOff = 4,
            }
            impl CommandAction {
                /// String value of the enum field names used in the ProtoBuf definition.
                ///
                /// The values are not transformed in any way and thus are considered stable
                /// (if the ProtoBuf definition does not change) and safe for programmatic use.
                pub fn as_str_name(&self) -> &'static str {
                    match self {
                        CommandAction::FadeToBlack => "COMMAND_ACTION_FADE_TO_BLACK",
                        CommandAction::KeyOn => "COMMAND_ACTION_KEY_ON",
                        CommandAction::KeyOff => "COMMAND_ACTION_KEY_OFF",
                        CommandAction::DskOn => "COMMAND_ACTION_DSK_ON",
                        CommandAction::DskOff => "COMMAND_ACTION_DSK_OFF",
                    }
                }
                /// Creates an enum from field names used in the ProtoBuf definition.
                pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                    match value {
                        "COMMAND_ACTION_FADE_TO_BLACK" => Some(Self::FadeToBlack),
                        "COMMAND_ACTION_KEY_ON" => Some(Self::KeyOn),
                        "COMMAND_ACTION_KEY_OFF" => Some(Self::KeyOff),
                        "COMMAND_ACTION_DSK_ON" => Some(Self::DskOn),
                        "COMMAND_ACTION_DSK_OFF" => Some(Self::DskOff),
                        _ => None,
                    }
                }
            }
        }
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum CommandTypeData {
            #[prost(message, tag = "5")]
            MidiCommand(MidiCommand),
            #[prost(message, tag = "6")]
            GlobalCacheCommand(GlobalCacheCommand),
            #[prost(message, tag = "7")]
            Gvg100Command(Gvg100Command),
            #[prost(message, tag = "8")]
            SonyBvsCommand(SonyBvsCommand),
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MultiScreenType {
        #[prost(message, optional, tag = "3")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct DocumentType {
        #[prost(message, optional, tag = "1")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
        #[prost(message, optional, tag = "2")]
        pub selected_arrangement: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(enumeration = "document_type::ContentDestination", tag = "3")]
        pub content_destination: i32,
    }
    /// Nested message and enum types in `DocumentType`.
    pub mod document_type {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum ContentDestination {
            Global = 0,
            Announcements = 1,
        }
        impl ContentDestination {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    ContentDestination::Global => "CONTENT_DESTINATION_GLOBAL",
                    ContentDestination::Announcements => {
                        "CONTENT_DESTINATION_ANNOUNCEMENTS"
                    }
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "CONTENT_DESTINATION_GLOBAL" => Some(Self::Global),
                    "CONTENT_DESTINATION_ANNOUNCEMENTS" => Some(Self::Announcements),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ExternalPresentationType {
        #[prost(message, optional, tag = "1")]
        pub url: ::core::option::Option<super::Url>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AudienceLookType {
        #[prost(message, optional, tag = "1")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AudioInputType {
        #[prost(int32, tag = "1")]
        pub index: i32,
        #[prost(bool, tag = "2")]
        pub override_mode: bool,
        #[prost(message, optional, tag = "3")]
        pub behavior_mode: ::core::option::Option<super::audio_input::BehaviorMode>,
        #[prost(bool, tag = "4")]
        pub override_volume: bool,
        #[prost(double, tag = "5")]
        pub volume: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MacroType {
        #[prost(message, optional, tag = "1")]
        pub identification: ::core::option::Option<super::CollectionElementType>,
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum ActionType {
        Unknown = 0,
        StageLayout = 1,
        Media = 2,
        Timer = 3,
        Communication = 4,
        Clear = 5,
        Prop = 6,
        Mask = 7,
        Message = 8,
        SocialMedia = 9,
        Multiscreen = 10,
        PresentationSlide = 11,
        ForegroundMedia = 12,
        BackgroundMedia = 13,
        PresentationDocument = 14,
        PropSlide = 15,
        ExternalPresentation = 17,
        AudienceLook = 18,
        AudioInput = 19,
        AudioBinPlaylist = 20,
        MediaBinPlaylist = 21,
        SlideDestination = 22,
        Macro = 23,
        ClearGroup = 24,
    }
    impl ActionType {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                ActionType::Unknown => "ACTION_TYPE_UNKNOWN",
                ActionType::StageLayout => "ACTION_TYPE_STAGE_LAYOUT",
                ActionType::Media => "ACTION_TYPE_MEDIA",
                ActionType::Timer => "ACTION_TYPE_TIMER",
                ActionType::Communication => "ACTION_TYPE_COMMUNICATION",
                ActionType::Clear => "ACTION_TYPE_CLEAR",
                ActionType::Prop => "ACTION_TYPE_PROP",
                ActionType::Mask => "ACTION_TYPE_MASK",
                ActionType::Message => "ACTION_TYPE_MESSAGE",
                ActionType::SocialMedia => "ACTION_TYPE_SOCIAL_MEDIA",
                ActionType::Multiscreen => "ACTION_TYPE_MULTISCREEN",
                ActionType::PresentationSlide => "ACTION_TYPE_PRESENTATION_SLIDE",
                ActionType::ForegroundMedia => "ACTION_TYPE_FOREGROUND_MEDIA",
                ActionType::BackgroundMedia => "ACTION_TYPE_BACKGROUND_MEDIA",
                ActionType::PresentationDocument => "ACTION_TYPE_PRESENTATION_DOCUMENT",
                ActionType::PropSlide => "ACTION_TYPE_PROP_SLIDE",
                ActionType::ExternalPresentation => "ACTION_TYPE_EXTERNAL_PRESENTATION",
                ActionType::AudienceLook => "ACTION_TYPE_AUDIENCE_LOOK",
                ActionType::AudioInput => "ACTION_TYPE_AUDIO_INPUT",
                ActionType::AudioBinPlaylist => "ACTION_TYPE_AUDIO_BIN_PLAYLIST",
                ActionType::MediaBinPlaylist => "ACTION_TYPE_MEDIA_BIN_PLAYLIST",
                ActionType::SlideDestination => "ACTION_TYPE_SLIDE_DESTINATION",
                ActionType::Macro => "ACTION_TYPE_MACRO",
                ActionType::ClearGroup => "ACTION_TYPE_CLEAR_GROUP",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "ACTION_TYPE_UNKNOWN" => Some(Self::Unknown),
                "ACTION_TYPE_STAGE_LAYOUT" => Some(Self::StageLayout),
                "ACTION_TYPE_MEDIA" => Some(Self::Media),
                "ACTION_TYPE_TIMER" => Some(Self::Timer),
                "ACTION_TYPE_COMMUNICATION" => Some(Self::Communication),
                "ACTION_TYPE_CLEAR" => Some(Self::Clear),
                "ACTION_TYPE_PROP" => Some(Self::Prop),
                "ACTION_TYPE_MASK" => Some(Self::Mask),
                "ACTION_TYPE_MESSAGE" => Some(Self::Message),
                "ACTION_TYPE_SOCIAL_MEDIA" => Some(Self::SocialMedia),
                "ACTION_TYPE_MULTISCREEN" => Some(Self::Multiscreen),
                "ACTION_TYPE_PRESENTATION_SLIDE" => Some(Self::PresentationSlide),
                "ACTION_TYPE_FOREGROUND_MEDIA" => Some(Self::ForegroundMedia),
                "ACTION_TYPE_BACKGROUND_MEDIA" => Some(Self::BackgroundMedia),
                "ACTION_TYPE_PRESENTATION_DOCUMENT" => Some(Self::PresentationDocument),
                "ACTION_TYPE_PROP_SLIDE" => Some(Self::PropSlide),
                "ACTION_TYPE_EXTERNAL_PRESENTATION" => Some(Self::ExternalPresentation),
                "ACTION_TYPE_AUDIENCE_LOOK" => Some(Self::AudienceLook),
                "ACTION_TYPE_AUDIO_INPUT" => Some(Self::AudioInput),
                "ACTION_TYPE_AUDIO_BIN_PLAYLIST" => Some(Self::AudioBinPlaylist),
                "ACTION_TYPE_MEDIA_BIN_PLAYLIST" => Some(Self::MediaBinPlaylist),
                "ACTION_TYPE_SLIDE_DESTINATION" => Some(Self::SlideDestination),
                "ACTION_TYPE_MACRO" => Some(Self::Macro),
                "ACTION_TYPE_CLEAR_GROUP" => Some(Self::ClearGroup),
                _ => None,
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum ActionTypeData {
        #[prost(message, tag = "16")]
        CollectionElement(super::CollectionElementType),
        #[prost(message, tag = "17")]
        PlaylistItem(PlaylistItemType),
        #[prost(message, tag = "18")]
        BlendMode(BlendModeType),
        #[prost(message, tag = "19")]
        Transition(TransitionType),
        #[prost(message, tag = "20")]
        Media(MediaType),
        #[prost(message, tag = "21")]
        DoubleItem(DoubleType),
        #[prost(message, tag = "22")]
        Effects(EffectsType),
        #[prost(message, tag = "23")]
        Slide(SlideType),
        #[prost(message, tag = "24")]
        Background(BackgroundType),
        #[prost(message, tag = "25")]
        Timer(TimerType),
        #[prost(message, tag = "26")]
        Clear(ClearType),
        #[prost(message, tag = "27")]
        Stage(StageLayoutType),
        #[prost(message, tag = "28")]
        Prop(PropType),
        #[prost(message, tag = "29")]
        Mask(MaskType),
        #[prost(message, tag = "30")]
        Message(MessageType),
        #[prost(message, tag = "31")]
        SocialMedia(SocialMediaType),
        #[prost(message, tag = "32")]
        Communication(CommunicationType),
        #[prost(message, tag = "33")]
        MultiScreen(MultiScreenType),
        #[prost(message, tag = "34")]
        PresentationDocument(DocumentType),
        #[prost(message, tag = "36")]
        ExternalPresentation(ExternalPresentationType),
        #[prost(message, tag = "37")]
        AudienceLook(AudienceLookType),
        #[prost(message, tag = "38")]
        AudioInput(AudioInputType),
        #[prost(message, tag = "39")]
        SlideDestination(SlideDestinationType),
        #[prost(message, tag = "40")]
        Macro(MacroType),
        #[prost(message, tag = "41")]
        ClearGroup(ClearGroupType),
        #[prost(message, tag = "42")]
        TransportControl(TransportControlType),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Cue {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub name: ::prost::alloc::string::String,
    #[prost(enumeration = "cue::CompletionTargetType", tag = "3")]
    pub completion_target_type: i32,
    #[prost(message, optional, tag = "4")]
    pub completion_target_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(enumeration = "cue::CompletionActionType", tag = "5")]
    pub completion_action_type: i32,
    #[prost(message, optional, tag = "6")]
    pub completion_action_uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(message, optional, tag = "7")]
    pub trigger_time: ::core::option::Option<cue::TimecodeTime>,
    #[prost(message, optional, tag = "8")]
    pub hot_key: ::core::option::Option<HotKey>,
    #[prost(message, repeated, tag = "10")]
    pub actions: ::prost::alloc::vec::Vec<Action>,
    #[prost(message, repeated, tag = "11")]
    pub pending_imports: ::prost::alloc::vec::Vec<cue::PendingImportsEntry>,
    #[prost(bool, tag = "12")]
    pub is_enabled: bool,
    #[prost(double, tag = "13")]
    pub completion_time: f64,
}
/// Nested message and enum types in `Cue`.
pub mod cue {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct TimecodeTime {
        #[prost(double, tag = "1")]
        pub time: f64,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct PendingImportsEntry {
        #[prost(string, tag = "1")]
        pub key: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "2")]
        pub value: ::core::option::Option<super::UrLs>,
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum CompletionTargetType {
        None = 0,
        Next = 1,
        Random = 2,
        Cue = 3,
        First = 4,
    }
    impl CompletionTargetType {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                CompletionTargetType::None => "COMPLETION_TARGET_TYPE_NONE",
                CompletionTargetType::Next => "COMPLETION_TARGET_TYPE_NEXT",
                CompletionTargetType::Random => "COMPLETION_TARGET_TYPE_RANDOM",
                CompletionTargetType::Cue => "COMPLETION_TARGET_TYPE_CUE",
                CompletionTargetType::First => "COMPLETION_TARGET_TYPE_FIRST",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "COMPLETION_TARGET_TYPE_NONE" => Some(Self::None),
                "COMPLETION_TARGET_TYPE_NEXT" => Some(Self::Next),
                "COMPLETION_TARGET_TYPE_RANDOM" => Some(Self::Random),
                "COMPLETION_TARGET_TYPE_CUE" => Some(Self::Cue),
                "COMPLETION_TARGET_TYPE_FIRST" => Some(Self::First),
                _ => None,
            }
        }
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum CompletionActionType {
        First = 0,
        Last = 1,
        AfterAction = 2,
        AfterTime = 3,
    }
    impl CompletionActionType {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                CompletionActionType::First => "COMPLETION_ACTION_TYPE_FIRST",
                CompletionActionType::Last => "COMPLETION_ACTION_TYPE_LAST",
                CompletionActionType::AfterAction => {
                    "COMPLETION_ACTION_TYPE_AFTER_ACTION"
                }
                CompletionActionType::AfterTime => "COMPLETION_ACTION_TYPE_AFTER_TIME",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "COMPLETION_ACTION_TYPE_FIRST" => Some(Self::First),
                "COMPLETION_ACTION_TYPE_LAST" => Some(Self::Last),
                "COMPLETION_ACTION_TYPE_AFTER_ACTION" => Some(Self::AfterAction),
                "COMPLETION_ACTION_TYPE_AFTER_TIME" => Some(Self::AfterTime),
                _ => None,
            }
        }
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Timestamp {
    #[prost(int64, tag = "1")]
    pub seconds: i64,
    #[prost(int32, tag = "2")]
    pub nanos: i32,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Group {
    #[prost(message, optional, tag = "1")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "2")]
    pub name: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub color: ::core::option::Option<Color>,
    #[prost(message, optional, tag = "4")]
    pub hot_key: ::core::option::Option<HotKey>,
    #[prost(message, optional, tag = "5")]
    pub application_group_identifier: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "6")]
    pub application_group_name: ::prost::alloc::string::String,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProGroupsDocument {
    #[prost(message, repeated, tag = "1")]
    pub groups: ::prost::alloc::vec::Vec<Group>,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Presentation {
    #[prost(message, optional, tag = "1")]
    pub application_info: ::core::option::Option<ApplicationInfo>,
    #[prost(message, optional, tag = "2")]
    pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(string, tag = "3")]
    pub name: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "4")]
    pub last_date_used: ::core::option::Option<Timestamp>,
    #[prost(message, optional, tag = "5")]
    pub last_modified_date: ::core::option::Option<Timestamp>,
    #[prost(string, tag = "6")]
    pub category: ::prost::alloc::string::String,
    #[prost(string, tag = "7")]
    pub notes: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "8")]
    pub background: ::core::option::Option<Background>,
    #[prost(message, optional, tag = "9")]
    pub chord_chart: ::core::option::Option<Url>,
    #[prost(message, optional, tag = "10")]
    pub selected_arrangement: ::core::option::Option<crate::protobuf::RVUuid>,
    #[prost(message, repeated, tag = "11")]
    pub arrangements: ::prost::alloc::vec::Vec<presentation::Arrangement>,
    #[prost(message, repeated, tag = "12")]
    pub cue_groups: ::prost::alloc::vec::Vec<presentation::CueGroup>,
    #[prost(message, repeated, tag = "13")]
    pub cues: ::prost::alloc::vec::Vec<Cue>,
    #[prost(message, optional, tag = "14")]
    pub ccli: ::core::option::Option<presentation::Ccli>,
    #[prost(message, optional, tag = "15")]
    pub bible_reference: ::core::option::Option<presentation::BibleReference>,
    #[prost(message, optional, tag = "16")]
    pub social_media: ::core::option::Option<presentation::SocialMedia>,
    #[prost(message, optional, tag = "17")]
    pub timeline: ::core::option::Option<presentation::Timeline>,
    #[prost(message, optional, tag = "18")]
    pub transition: ::core::option::Option<Transition>,
    #[prost(enumeration = "presentation::ContentDestination", tag = "19")]
    pub content_destination: i32,
    #[prost(message, optional, tag = "21")]
    pub multi_tracks_licensing: ::core::option::Option<
        presentation::MultiTracksLicensing,
    >,
    #[prost(string, tag = "22")]
    pub music_key: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "23")]
    pub music: ::core::option::Option<presentation::Music>,
    #[prost(oneof = "presentation::SlideShow", tags = "20")]
    pub slide_show: ::core::option::Option<presentation::SlideShow>,
}
/// Nested message and enum types in `Presentation`.
pub mod presentation {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Ccli {
        #[prost(string, tag = "1")]
        pub author: ::prost::alloc::string::String,
        #[prost(string, tag = "2")]
        pub artist_credits: ::prost::alloc::string::String,
        #[prost(string, tag = "3")]
        pub song_title: ::prost::alloc::string::String,
        #[prost(string, tag = "4")]
        pub publisher: ::prost::alloc::string::String,
        #[prost(uint32, tag = "5")]
        pub copyright_year: u32,
        #[prost(uint32, tag = "6")]
        pub song_number: u32,
        #[prost(bool, tag = "7")]
        pub display: bool,
        #[prost(string, tag = "8")]
        pub album: ::prost::alloc::string::String,
        #[prost(bytes = "vec", tag = "9")]
        pub artwork: ::prost::alloc::vec::Vec<u8>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BibleReference {
        #[prost(uint32, tag = "1")]
        pub book_index: u32,
        #[prost(string, tag = "2")]
        pub book_name: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub chapter_range: ::core::option::Option<super::IntRange>,
        #[prost(message, optional, tag = "4")]
        pub verse_range: ::core::option::Option<super::IntRange>,
        #[prost(string, tag = "5")]
        pub translation_name: ::prost::alloc::string::String,
        #[prost(string, tag = "6")]
        pub translation_display_abbreviation: ::prost::alloc::string::String,
        #[prost(string, tag = "7")]
        pub translation_internal_abbreviation: ::prost::alloc::string::String,
        #[prost(string, tag = "8")]
        pub book_key: ::prost::alloc::string::String,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SocialMedia {
        #[prost(string, tag = "1")]
        pub term: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "2")]
        pub time: ::core::option::Option<super::Timestamp>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Timeline {
        #[prost(message, repeated, tag = "1")]
        pub cues: ::prost::alloc::vec::Vec<timeline::Cue>,
        #[prost(double, tag = "5")]
        pub duration: f64,
        #[prost(bool, tag = "6")]
        pub r#loop: bool,
        #[prost(message, optional, tag = "8")]
        pub audio_action: ::core::option::Option<super::Action>,
        #[prost(bool, tag = "9")]
        pub timecode_enable: bool,
        #[prost(double, tag = "10")]
        pub timecode_offset: f64,
        #[prost(message, repeated, tag = "11")]
        pub cues_v2: ::prost::alloc::vec::Vec<timeline::Cue>,
    }
    /// Nested message and enum types in `Timeline`.
    pub mod timeline {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Cue {
            #[prost(double, tag = "1")]
            pub trigger_time: f64,
            #[prost(string, tag = "3")]
            pub name: ::prost::alloc::string::String,
            #[prost(oneof = "cue::TriggerInfo", tags = "2, 4")]
            pub trigger_info: ::core::option::Option<cue::TriggerInfo>,
        }
        /// Nested message and enum types in `Cue`.
        pub mod cue {
            #[allow(clippy::derive_partial_eq_without_eq)]
            #[derive(Clone, PartialEq, ::prost::Oneof)]
            pub enum TriggerInfo {
                #[prost(message, tag = "2")]
                CueId(crate::protobuf::RVUuid),
                #[prost(message, tag = "4")]
                Action(super::super::super::Action),
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Arrangement {
        #[prost(message, optional, tag = "1")]
        pub uuid: ::core::option::Option<crate::protobuf::RVUuid>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, repeated, tag = "3")]
        pub group_identifiers: ::prost::alloc::vec::Vec<crate::protobuf::RVUuid>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct CueGroup {
        #[prost(message, optional, tag = "1")]
        pub group: ::core::option::Option<super::Group>,
        #[prost(message, repeated, tag = "2")]
        pub cue_identifiers: ::prost::alloc::vec::Vec<crate::protobuf::RVUuid>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MultiTracksLicensing {
        #[prost(int64, tag = "1")]
        pub song_identifier: i64,
        #[prost(string, tag = "2")]
        pub customer_identifier: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub expiration_date: ::core::option::Option<super::Timestamp>,
        #[prost(message, optional, tag = "4")]
        pub license_expiration: ::core::option::Option<super::Timestamp>,
        #[prost(enumeration = "multi_tracks_licensing::Subscription", tag = "5")]
        pub subscription: i32,
    }
    /// Nested message and enum types in `MultiTracksLicensing`.
    pub mod multi_tracks_licensing {
        #[derive(
            Clone,
            Copy,
            Debug,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            ::prost::Enumeration
        )]
        #[repr(i32)]
        pub enum Subscription {
            ChartPro = 0,
            SlidePro = 1,
        }
        impl Subscription {
            /// String value of the enum field names used in the ProtoBuf definition.
            ///
            /// The values are not transformed in any way and thus are considered stable
            /// (if the ProtoBuf definition does not change) and safe for programmatic use.
            pub fn as_str_name(&self) -> &'static str {
                match self {
                    Subscription::ChartPro => "SUBSCRIPTION_CHART_PRO",
                    Subscription::SlidePro => "SUBSCRIPTION_SLIDE_PRO",
                }
            }
            /// Creates an enum from field names used in the ProtoBuf definition.
            pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
                match value {
                    "SUBSCRIPTION_CHART_PRO" => Some(Self::ChartPro),
                    "SUBSCRIPTION_SLIDE_PRO" => Some(Self::SlidePro),
                    _ => None,
                }
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Music {
        #[prost(string, tag = "1")]
        pub original_music_key: ::prost::alloc::string::String,
        #[prost(string, tag = "2")]
        pub user_music_key: ::prost::alloc::string::String,
        #[prost(message, optional, tag = "3")]
        pub original: ::core::option::Option<super::MusicKeyScale>,
        #[prost(message, optional, tag = "4")]
        pub user: ::core::option::Option<super::MusicKeyScale>,
    }
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum ContentDestination {
        Global = 0,
        Announcements = 1,
    }
    impl ContentDestination {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                ContentDestination::Global => "CONTENT_DESTINATION_GLOBAL",
                ContentDestination::Announcements => "CONTENT_DESTINATION_ANNOUNCEMENTS",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CONTENT_DESTINATION_GLOBAL" => Some(Self::Global),
                "CONTENT_DESTINATION_ANNOUNCEMENTS" => Some(Self::Announcements),
                _ => None,
            }
        }
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum SlideShow {
        #[prost(double, tag = "20")]
        SlideShowDuration(f64),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Template {}
/// Nested message and enum types in `Template`.
pub mod template {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Slide {
        #[prost(message, optional, tag = "1")]
        pub base_slide: ::core::option::Option<super::Slide>,
        #[prost(string, tag = "2")]
        pub name: ::prost::alloc::string::String,
        #[prost(message, repeated, tag = "3")]
        pub actions: ::prost::alloc::vec::Vec<super::Action>,
    }
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Document {
        #[prost(message, optional, tag = "1")]
        pub application_info: ::core::option::Option<super::ApplicationInfo>,
        #[prost(message, repeated, tag = "3")]
        pub slides: ::prost::alloc::vec::Vec<Slide>,
    }
}
