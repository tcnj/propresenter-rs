#!/bin/sh

cd "${BASH_SOURCE[0]%/*}"

if [ ! -d ./ProPresenter7-Proto ]; then
    git clone https://github.com/greyshirtguy/ProPresenter7-Proto
else
    pushd ProPresenter7-Proto
    git pull
    popd
fi

cargo run -p generate-protos --bin generate-protos