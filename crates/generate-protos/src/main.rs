fn main() {
    prost_build::Config::new()
        .out_dir("./generated/")
        .extern_path(".rv.data.UUID", "crate::protobuf::RVUuid")
        .compile_protos(
            &[
                "ProPresenter7-Proto/proto/presentation.proto",
                "ProPresenter7-Proto/proto/template.proto",
            ],
            &["ProPresenter7-Proto/proto/"],
        )
        .expect("compilation failed");
}
